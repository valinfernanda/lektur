import BGGetStarted from './BGGetStarted.jpg';
import BGClassroom from './BGClassroom.jpg';
import Rectangle4 from './Rectangle4.jpg';
import DummyPosters from './DummyPosters.jpg';
import image10 from './image10.jpg';
import DummyLaptop from './DummyLaptop.png';
import PopUpImage from './PopUpImage.png';
import ml from './ml.jpg';
import machineLearning from './machineLearning.webp';
import Love from './Love.jpg';
import ILNullPhoto from './null-photo.png';

// import Pencil from './Pencil.png';

export {
  BGGetStarted,
  BGClassroom,
  Rectangle4,
  DummyPosters,
  DummyLaptop,
  image10,
  PopUpImage,
  ml,
  machineLearning,
  Love,
  ILNullPhoto,

  // Pencil,
};
