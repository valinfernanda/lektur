import Assignment from './Assignment.svg';
import AssignmentActive from './AssignmentActive.svg';
import Course from './Course.svg';
import CourseActive from './CourseActive.svg';
import Home from './Home.svg';
import HomeActive from './HomeActive.svg';
import Profile from './Profile.svg';
import ProfileActive from './ProfileActive.svg';
import ArrowBack from './arrowBack.svg';
import PlaylistActive from './playlisActive.svg';
import Check from './Check.svg';
import ClickWhite from './ClickWhite.svg';
import Dokumen from './Dokumen.svg';
import Vector from './Vector.svg';
import PencilEdit from './PencilEdit.svg';

export {
  Assignment,
  AssignmentActive,
  Course,
  CourseActive,
  Check,
  Home,
  HomeActive,
  Profile,
  ProfileActive,
  ArrowBack,
  PlaylistActive,
  ClickWhite,
  Dokumen,
  Vector,
  PencilEdit,
};
