import {createStackNavigator} from '@react-navigation/stack';
import React from 'react';
import {
  Login,
  Register,
  Home,
  CourseContent,
  CourseDetail,
  CoursePlay,
  Assignment,
  Profile,
  GetStarted,
  LoginTeacher,
  RegisterTeacher,
  InputAssesment,
  CourseThree,
  CourseFour,
  Course,
  Assessment,
  InputEmailStudent,
  TeacherCourse,
  Testingcuy,
  AssignmentStudent,
  AssignmentStudentScore,
  CourseMaterial,
  SearchScreen,
} from '../Features';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {BottomNavigator} from '../Components';
import Logout from '../Features/Logout';
import AddCourse from '../Features/Teacher/AddCourse/index';
import AddCourseEdit from '../Features/Teacher/AddCourseEdit/index';

import InviteStudents from '../Features/Teacher/InviteStudents';
import EditProfile from '../Features/Profile/EditProfile/index';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

const MainApp = () => {
  return (
    <Tab.Navigator
      tabBar={(props) => <BottomNavigator {...props} />}
      initialRouteName="Home">
      <Tab.Screen name="Home" component={Home} />
      <Tab.Screen name="Course" component={TeacherCourse} />
      <Tab.Screen name="Assignment" component={Assignment} />
      <Tab.Screen name="Profile" component={Profile} />
    </Tab.Navigator>
  );
};

const Router = () => {
  return (
    <Stack.Navigator initialRouteName="GetStarted">
      <Stack.Screen
        name="GetStarted"
        component={GetStarted}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Login"
        component={Login}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="CourseThree"
        component={CourseThree}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="CourseFour"
        component={CourseFour}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="LoginTeacher"
        component={LoginTeacher}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Register"
        component={Register}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="RegisterTeacher"
        component={RegisterTeacher}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Home"
        component={Home}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="CourseContent"
        component={CourseContent}
        options={{headerShown: true}}
      />
      <Stack.Screen
        name="CourseDetail"
        component={CourseDetail}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="CoursePlay"
        component={CoursePlay}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="InputAssesment"
        component={InputAssesment}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="CourseMaterial"
        component={CourseMaterial}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="AssignmentStudent"
        component={AssignmentStudent}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="AssignmentStudentScore"
        component={AssignmentStudentScore}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Course"
        component={Course}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Profile"
        component={Profile}
        options={{headerShown: false}}
      />

      <Stack.Screen
        name="Logout"
        component={Logout}
        options={{headerShown: false}}
      />

      <Stack.Screen
        name="TeacherCourse"
        component={TeacherCourse}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Assessment"
        component={Assessment}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="AddCourse"
        component={AddCourse}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="AddCourseEdit"
        component={AddCourseEdit}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="InviteStudents"
        component={InviteStudents}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="InputEmailStudent"
        component={InputEmailStudent}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Assignment"
        component={Assignment}
        options={{headerShown: true}}
      />
      <Stack.Screen
        name="Testingcuy"
        component={Testingcuy}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="MainApp"
        component={MainApp}
        options={{headerShown: false}}
      />

      <Stack.Screen
        name="EditProfile"
        component={EditProfile}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="SearchScreen"
        component={SearchScreen}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  );
};

export default Router;
