import {all, takeLatest, put} from 'redux-saga/effects';
import axios from 'axios';
import {navigate} from '../../../Utils/Nav';

function* SagaRegisterTeacher(payload) {
  try {
    //parameter post : url dan body. Bodynya bentuk object
    const body = {
      name: payload.name,
      email: payload.email,
      password: payload.password,
    };
    console.log(body);
    const Response = yield axios.post(
      'https://laflix-api.herokuapp.com/api/register',
      body,
    );

    yield put({
      type: 'SET_REGISTER_TEACHER',
      email: payload.email,
      password: payload.password,
      fullname: payload.fullname,
      username: payload.username,
    });
    yield put(navigate('SignIn', {}));
    console.log(Response, 'hasil respond register');
  } catch (error) {
    console.log(error);
  }
}

export function* SagasRegisterTeacher() {
  //POST Register INI SESUAIIN SAMA ACTION WEY
  yield takeLatest('POST_REGISTER_TEACHER', SagaRegisterTeacher);
}
