export const ActionRegisterTeacher = (name, email, password) => {
  return {
    type: 'POST_REGISTER_TEACHER',
    name,
    email,
    password,
  };
};
