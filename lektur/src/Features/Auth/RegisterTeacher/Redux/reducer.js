const InitialState = {
  name: '',
  email: '',
};

export const RegisterTeacherReducer = (state = InitialState, action) => {
  switch (action.type) {
    case 'SET_REGISTER_TEACHER':
      return {
        ...state,
        name: action.name,
        email: action.email,
      };
    default:
      return state;
  }
};
