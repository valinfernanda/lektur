import React, {useState} from 'react';
import {View, Text, ImageBackground, StyleSheet} from 'react-native';
import {BGClassroom} from '../../../Assets';
import Link from '../../../Components/atoms/Link';
import Gap from '../../../Components/atoms/Gap';
import Input from '../../../Components/atoms/Input';
import Button from '../../../Components/atoms/Button';
import {Header} from '../../../Components/molecules';
import {ActionRegisterTeacher} from './Redux/action';
import {connect} from 'react-redux';

const RegisterTeacher = (props) => {
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  return (
    <ImageBackground source={BGClassroom} style={styles.page}>
      <Header title="Sign Up" />
      <View style={styles.kotak}>
        <View style={styles.jarakKotak}>
          <Text style={styles.welcome}>Start Teaching!</Text>
          <Gap height={4} />
          <Text style={styles.underLogin}>Create your account</Text>
          <Gap height={40} />
          <Input
            label="Name*"
            onChangeText={(text) => setName(text)}
            value={name}
          />
          <Gap height={20} />
          <Input
            label="Email*"
            onChangeText={(text) => setEmail(text)}
            value={email}
          />
          <Gap height={20} />
          <Input
            label="Password*"
            onChangeText={(text) => setPassword(text)}
            value={password}
          />
          <Gap height={5} />
          <View style={styles.jarak}>
            {/* <Link title="Forgot My Password" size={14} align="right" /> */}
            <Gap height={20} />
            <View style={styles.press}>
              <Button
                title="Sign Up"
                // onPress={() =>
                //   props.ActionRegister(name, email, password)
                // }
              />
            </View>
          </View>
          <Gap height={70} />
          <View style={styles.choose}>
            <Text>Already have an account?</Text>
            <Gap width={4} />
            <Link
              title="Login"
              onPress={() => props.navigation.navigate('LoginTeacher')}
            />
          </View>
        </View>
      </View>
    </ImageBackground>
  );
};

const mapStateToProps = (state) => ({});

const mapDispatchToProps = {
  ActionRegisterTeacher,
};

export default connect(mapStateToProps, mapDispatchToProps)(RegisterTeacher);

const styles = StyleSheet.create({
  page: {
    // padding: 20,
    // justifyContent: 'space-between',
    // backgroundColor: colors.white,
    flex: 1,
    flexDirection: 'column',
  },
  asking: {
    // backgroundColor: 'white',
    alignSelf: 'center',
    fontSize: 20,
    fontWeight: 'bold',
  },
  press: {
    backgroundColor: 'pink',
    flex: 1,
    flexDirection: 'row',
    alignSelf: 'flex-end',
  },
  title: {
    fontSize: 28,
    // fontWeight: "600",
    // color: colors.white,
    marginTop: 20,
    fontFamily: '._Nunito-SemiBold',
    // backgroundColor: 'white',
  },
  kotak: {
    backgroundColor: 'white',
    width: 320,
    paddingVertical: 20,
    alignSelf: 'center',
    marginTop: 52,
  },
  pass: {
    justifyContent: 'flex-end',
  },
  welcome: {
    fontSize: 24,
    fontWeight: 'bold',
  },
  underLogin: {
    fontSize: 18,
  },
  jarakKotak: {
    paddingLeft: 24,
  },
  jarak: {
    paddingRight: 15,
  },
  choose: {
    flexDirection: 'row',
    justifyContent: 'center',
    // color: 'black',
  },
});
