import {all, takeLatest, put} from 'redux-saga/effects';
import axios from 'axios';
// import {navigate} from '../../../Utils/Nav';
import {Store} from '../../../../Store/Store';
// import {navigate} from '../../Utils/Nav';
import {navigate} from '../../../Utils/Nav';
import qs from 'query-string';
import {setLoading} from '../../../../Store/GlobalAction';

function* SagaRegister(payload) {
  try {
    yield put(setLoading(true));
    console.log('ini register');
    const role = Store.getState().RegisterReducer.role;
    //parameter post : url dan body. Bodynya bentuk object
    const body = {
      fullname: payload.fullname,
      email: payload.email,
      password: payload.password,
    };
    console.log(body, 'ini body');
    console.log(role, 'ini role');

    const Response = yield axios.post(
      `https://lekturapp.herokuapp.com/api/users/register?status=${role}`,
      body,
    );

    yield put({
      type: 'SET_REGISTER',
      fullname: payload.fullname,
      email: payload.email,
    });
    yield put(navigate('Login', {}));
    console.log(Response, 'hasil respond register');
  } catch (error) {
    console.log(error);
    console.log(JSON.stringify(error));
  } finally {
    yield put(setLoading(false));
  }
}

export function* SagasRegister() {
  //POST Register INI SESUAIIN SAMA ACTION WEY
  yield takeLatest('POST_REGISTER', SagaRegister);
}
