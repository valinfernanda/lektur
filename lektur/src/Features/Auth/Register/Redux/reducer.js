const InitialState = {
  fullname: '',
  email: '',
  role: 0,
};

export const RegisterReducer = (state = InitialState, action) => {
  switch (action.type) {
    case 'SET_REGISTER':
      return {
        ...state,
        fullname: action.fullname,
        email: action.email,
      };
    case 'ROLE':
      return {
        ...state,
        role: action.role,
      };
    default:
      return state;
  }
};
