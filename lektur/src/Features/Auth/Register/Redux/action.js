export const ActionRegister = (fullname, email, password) => {
  return {
    type: 'POST_REGISTER',
    fullname,
    email,
    password,
  };
};

export const ActionRole = (role) => {
  return {
    type: 'ROLE',
    role,
  };
};
