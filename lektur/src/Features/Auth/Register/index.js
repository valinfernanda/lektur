import React, {useState} from 'react';
import {
  View,
  Text,
  ImageBackground,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import {BGClassroom} from '../../../Assets';
import Link from '../../../Components/atoms/Link';
import Gap from '../../../Components/atoms/Gap';
import Input from '../../../Components/atoms/Input';
import Button from '../../../Components/atoms/Button';
import {Header} from '../../../Components/molecules';
import {ActionRegister} from './Redux/action';
import {connect} from 'react-redux';
import {Size} from '../../../Shared/Global/Config/Size';
import {ScrollView} from 'react-native';
import Loading from '../../../Components/atoms/Loading';
// import {TouchableOpacity} from 'react-native-gesture-handler';

const Register = (props) => {
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  return (
    <>
      {props.isLoading ? (
        <Loading />
      ) : (
        <ImageBackground source={BGClassroom} style={styles.page}>
          <Header title="Sign Up" onPress={() => props.navigation.goBack()} />
          <ScrollView>
            <View style={styles.kotak}>
              <Text style={styles.welcome}>Let's Start!</Text>
              <Gap height={4} />
              <Text style={styles.underLogin}>Create your account</Text>

              <Gap height={40} />
              <Input
                label="Name*"
                onChangeText={(text) => setName(text)}
                value={name}
              />
              <Gap height={20} />
              <Input
                label="Email*"
                onChangeText={(text) => setEmail(text)}
                value={email}
              />
              <Gap height={20} />

              <Input
                label="Password*"
                onChangeText={(text) => setPassword(text)}
                secureTextEntry={true}
                value={password}
              />
              <Gap height={5} />
              <View style={styles.jarak}>
                <View style={styles.press}>
                  <Button
                    title="Sign Up"
                    onPress={() => props.ActionRegister(name, email, password)}
                  />
                </View>
              </View>

              <View style={styles.choose}>
                <Text>Already have an account?</Text>
                <Gap width={4} />
                <Link
                  title="Login"
                  onPress={() => props.navigation.navigate('Login')}
                />
              </View>
            </View>
            <Gap height={90} />
          </ScrollView>
        </ImageBackground>
      )}
    </>
  );
};

const mapStateToProps = (state) => ({
  isLoading: state.GlobalReducer.isLoading,
});

const mapDispatchToProps = {
  ActionRegister,
};

export default connect(mapStateToProps, mapDispatchToProps)(Register);

const styles = StyleSheet.create({
  page: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'blue',
  },
  press: {
    flexDirection: 'row',
    alignSelf: 'flex-end',
    paddingTop: Size.wp3,
  },
  kotak: {
    backgroundColor: 'white',
    width: Size.wp82,
    paddingVertical: Size.wp3,
    alignSelf: 'center',
    paddingLeft: Size.wp7,
    marginTop: Size.wp20,
  },
  welcome: {
    fontSize: Size.ms24,
    fontWeight: 'bold',
  },
  underLogin: {
    fontSize: Size.ms16,
  },
  jarak: {
    paddingRight: Size.wp7,
  },
  choose: {
    flexDirection: 'row',
    justifyContent: 'center',
    paddingTop: Size.wp9,
    paddingBottom: Size.wp5,
  },
});
