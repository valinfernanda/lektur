const InitialState = {
  email: '',
  token: '',
  role: '',
  errormessage: '',
  password: '',
};

export const LoginReducer = (state = InitialState, action) => {
  switch (action.type) {
    case 'SET_LOGIN':
      return {
        ...state,
        email: action.email,
        token: action.token,
        role: action.role,
        password: action.password,
      };
    case 'LOGIN_ERROR':
      return {
        ...state,
        errormessage: action.errormessage,
      };
    default:
      return state;
  }
};
