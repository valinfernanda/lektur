export const ActionLogin = (email, password) => {
  return {
    type: 'POST_LOGIN',
    email,
    password,
  };
};

export const SetLogin = (email, token, role, password) => {
  return {
    type: 'SET_LOGIN',
    email,
    token,
    role,
    password,
  };
};
