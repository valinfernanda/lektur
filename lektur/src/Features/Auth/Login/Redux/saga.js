import {all, takeLatest, put} from 'redux-saga/effects';
import axios from 'axios';
import {navigate} from '../../../Utils/Nav';
import jwt from 'jwt-decode';
import {Alert} from 'react-native';
import {SetLogin} from './action';
import {setLoading} from '../../../../Store/GlobalAction';
import {ActionSendId} from '../../../../Features/Home/Redux/action';

function* SagaLogin(data) {
  try {
    yield put(setLoading(true));
    console.log(data);
    //parameter post : url dan body. Bodynya bentuk object
    const body = {
      email: data.email,
      password: data.password,
    };

    const Response = yield axios.post(
      `https://lekturapp.herokuapp.com/api/users/login`,
      body,
      {validateStatus: (status) => status < 500},
    );
    console.log(Response);

    if (Response.status === 200) {
      const credential = jwt(Response.data.token);
      console.log(credential, 'ini credential men');

      //ini manggil dari action buat di saga nya men
      yield put(
        SetLogin(
          data.email,
          Response.data.token,
          credential.status,
          data.password,
        ),
      );
      yield put({
        type: 'LOGIN_ERROR',
        errormessage: '',
      });
      yield put(ActionSendId(Response.data.token));
      yield put(navigate('MainApp', {}));
    } else {
      // Alert.alert(Response.data.message);
      yield put({
        type: 'LOGIN_ERROR',
        errormessage: Response.data.message,
      });
    }
  } catch (error) {
    console.log(error);
  } finally {
    yield put(setLoading(false));
  }
}

export function* SagasLogin() {
  //POST SIGN INI SESUAIIN SAMA ACTION WEY
  yield takeLatest('POST_LOGIN', SagaLogin);
}
