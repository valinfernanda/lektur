import React, {useState} from 'react';
import {View, Text, ImageBackground, StyleSheet} from 'react-native';
import {BGClassroom} from '../../../Assets';
import Link from '../../../Components/atoms/Link';
import Gap from '../../../Components/atoms/Gap';
import Input from '../../../Components/atoms/Input';
import Button from '../../../Components/atoms/Button';
import {Header} from '../../../Components/molecules';
import {Size} from '../../../Shared/Global/Config/Size';
import {connect} from 'react-redux';
import {ActionLogin} from './Redux/action';
import {ScrollView} from 'react-native';
import Loading from '../../../Components/atoms/Loading';

const Login = (props) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  return (
    <>
      {props.isLoading ? (
        <Loading />
      ) : (
        <ImageBackground source={BGClassroom} style={styles.page}>
          <Header title="Sign Up" onPress={() => props.navigation.goBack()} />
          <ScrollView>
            <View style={styles.kotak}>
              <Text style={styles.welcome}>Welcome Back!</Text>
              <Gap height={4} />
              <Text style={styles.underLogin}>Login to your account</Text>

              <Gap height={40} />
              <Input
                label="Email*"
                onChangeText={(text) => setEmail(text)}
                value={email}
              />
              <Gap height={20} />

              <Input
                label="Password*"
                onChangeText={(text) => setPassword(text)}
                secureTextEntry={true}
                value={password}
              />
              <Gap height={5} />
              <View style={styles.jarak}>
                <Link title="Forgot My Password" size={14} align="right" />
                <Gap height={30} />
                <View style={styles.press}>
                  <Button
                    title="Login"
                    onPress={() => props.ActionLogin(email, password)}
                  />
                </View>
              </View>
              <Text>
                {props.errormessage === '' ? null : props.errormessage}
              </Text>

              <View style={styles.choose}>
                <Text>New User?</Text>
                <Gap width={4} />
                <Link
                  title="Create an account"
                  onPress={() => props.navigation.navigate('Register')}
                />
              </View>
            </View>
          </ScrollView>
        </ImageBackground>
      )}
    </>
  );
};

const mapStateToProps = (state) => ({
  errormessage: state.LoginReducer.errormessage,
  isLoading: state.GlobalReducer.isLoading,
});

const mapDispatchToProps = {
  ActionLogin,
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);

const styles = StyleSheet.create({
  page: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'blue',
  },
  press: {
    flexDirection: 'row',
    alignSelf: 'flex-end',
    paddingTop: Size.wp3,
  },
  kotak: {
    backgroundColor: 'white',
    width: Size.wp82,
    paddingVertical: Size.wp3,
    alignSelf: 'center',
    paddingLeft: Size.wp7,
    marginTop: Size.wp20,
  },
  welcome: {
    fontSize: Size.ms24,
    fontWeight: 'bold',
  },
  underLogin: {
    fontSize: Size.ms16,
  },
  jarak: {
    paddingRight: Size.wp7,
  },
  choose: {
    flexDirection: 'row',
    justifyContent: 'center',
    paddingTop: Size.wp9,
    paddingBottom: Size.wp5,
  },
});
