import React from 'react';
import {View, Text, ImageBackground, StyleSheet} from 'react-native';
import Link from '../../../Components/atoms/Link';
import Gap from '../../../Components/atoms/Gap';
import Input from '../../../Components/atoms/Input';
import Button from '../../../Components/atoms/Button';
import {Header} from '../../../Components/molecules';
import {BGClassroom} from '../../../Assets';

const LoginTeacher = ({navigation}) => {
  return (
    <ImageBackground source={BGClassroom} style={styles.page}>
      <Header title="Login" />
      <View style={styles.kotak}>
        <View style={styles.jarakKotak}>
          <Text style={styles.welcome}>Welcome Back, Teacher!</Text>
          <Gap height={4} />
          <Text style={styles.underLogin}>Login to your account</Text>
          <Gap height={40} />
          <Input label="Email*" />
          <Gap height={20} />
          <Input label="Password*" />
          <Gap height={5} />
          <View style={styles.jarak}>
            <Link title="Forgot My Password" size={14} align="right" />
            <Gap height={30} />
            <View style={styles.press}>
              <Button title="Login" />
            </View>
          </View>
          <Gap height={90} />
          <View style={styles.choose}>
            <Text>New User?</Text>
            <Gap width={4} />
            <Link
              title="Create an account"
              onPress={() => navigation.navigate('Register')}
            />
          </View>
        </View>
      </View>
    </ImageBackground>
  );
};

export default LoginTeacher;

const styles = StyleSheet.create({
  page: {
    // padding: 20,
    // justifyContent: 'space-between',
    // backgroundColor: colors.white,
    flex: 1,
    flexDirection: 'column',
  },
  asking: {
    // backgroundColor: 'white',
    alignSelf: 'center',
    fontSize: 20,
    fontWeight: 'bold',
  },
  press: {
    backgroundColor: 'pink',
    flex: 1,
    flexDirection: 'row',
    alignSelf: 'flex-end',
  },
  title: {
    fontSize: 28,
    // fontWeight: "600",
    // color: colors.white,
    marginTop: 20,
    fontFamily: '._Nunito-SemiBold',
    // backgroundColor: 'white',
  },
  kotak: {
    backgroundColor: 'white',
    width: 320,
    paddingVertical: 20,
    alignSelf: 'center',
    marginTop: 52,
  },
  pass: {
    justifyContent: 'flex-end',
  },
  welcome: {
    fontSize: 24,
    fontWeight: 'bold',
  },
  underLogin: {
    fontSize: 18,
  },
  jarakKotak: {
    paddingLeft: 24,
  },
  jarak: {
    paddingRight: 15,
  },
  choose: {
    flexDirection: 'row',
    justifyContent: 'center',
    // color: 'black',
  },
});
