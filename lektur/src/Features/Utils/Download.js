import RNFetchBlob from 'rn-fetch-blob';
import {PermissionsAndroid, Alert, ToastAndroid} from 'react-native';

const filesDownload = (url, fileName) => {
  let dirs = RNFetchBlob.fs.dirs;
  const locPath = `${
    Platform.OS === 'android' ? dirs.DocumentDir : dirs.DownloadDir
  }`;
  RNFetchBlob.config({
    fileCache: true,
    path: locPath,
    // android only options, these options be a no-op on IOS
    addAndroidDownloads: {
      useDownloadManager: true,
      // Show notification when response data transmitted
      notification: true,
      // Title of download notification
      title: fileName,
      // File description (not notification description)
      description: '',
    },
  })
    .fetch('GET', `${url}`)
    .then((res) => {
      console.log(res, 'RESPONSE_DOWNLOAD');
      console.log(locPath, 'locPath');

      console.log('the File saved to :', res.path());
    })
    .catch((e) => {
      console.log(locPath, dirs, 'locPath');
      console.log(e);
    });
};

export const downloadFile = async (url, fileName) => {
  try {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
    );
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      console.log(url, granted, 'download');
      filesDownload(url, fileName);
      ToastAndroid.show('Download...', ToastAndroid.SHORT);
    } else {
      Alert.alert(
        'Permission Denied!',
        'You need to give storage permission to download the file',
      );
    }
  } catch (err) {
    console.warn(err);
  }
};
