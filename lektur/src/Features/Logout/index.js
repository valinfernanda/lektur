import React from 'react';
import {StyleSheet, ScrollView, Text, TouchableOpacity} from 'react-native';

const Logout = () => {
  return (
    <ScrollView>
      <TouchableOpacity style={styles.menuButton} onPress={() => {}}>
        <Text style={styles.buttonText}>For Teacher</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.menuButton} onPress={() => {}}>
        <Text style={styles.buttonText}>Login</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.menuButton} onPress={() => {}}>
        <Text style={styles.buttonText}>Sign Up</Text>
      </TouchableOpacity>
    </ScrollView>
  );
};

export default Logout;

const styles = StyleSheet.create({
  menuButton: {
    borderColor: 'black',
    borderWidth: 1,
  },

  buttonText: {
    color: '#000000',
    fontWeight: 'bold',
    fontSize: 20,
  },
});
