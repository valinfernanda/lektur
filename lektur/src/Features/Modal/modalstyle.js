import {StyleSheet} from 'react-native';
import {Color} from '../../Shared/Global/Config/Color';
import {Size} from '../../Shared/Global/Config/Size';

export const modalStyles = StyleSheet.create({
  background: {
    height: '100%',
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'black',
    opacity: 0.7,
  },
  mCover: {
    marginTop: Size.hmin97,
    height: '100%',
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 100,
  },
  modalView: {
    height: '50%',
    width: '100%',
    backgroundColor: 'white',
    padding: Size.wp3,
    justifyContent: 'flex-start',
    alignItems: 'flex-end',
  },
  modalMain: {
    flex: 1,
    width: '100%',
    alignItems: 'center',
  },
  modalImage: {
    height: Size.h25,
    width: '100%',
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
    color: 'black',
  },
  modalTitle: {
    fontWeight: 'bold',
    fontSize: Size.ms18,
    color: Color.greenGum,
    marginBottom: Size.h3,
  },
  modalLessonT: {
    paddingHorizontal: Size.wp1,
    paddingTop: Size.h1,
    alignSelf: 'flex-start',
    fontWeight: 'bold',
    fontSize: Size.ms18,
  },
  modalLessonA: {
    paddingHorizontal: Size.wp1,
    alignSelf: 'flex-start',
    fontSize: Size.ms18,
    color: 'silver',
  },
  modalFooter: {
    width: '100%',
    height: Size.h6,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Color.lightSilver,
  },
});
