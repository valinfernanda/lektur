const InitialState = {
  searchResult: [],
};

export const SearchResultReducer = (state = InitialState, action) => {
  switch (action.type) {
    case 'SET_SEARCH_RESULT':
      return {
        ...state,
        searchResult: action.searchResult,
      };
    default:
      return state;
  }
};
