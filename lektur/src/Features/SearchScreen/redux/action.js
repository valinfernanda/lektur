export const SearchCourse = (nameSearch) => {
  return {
    type: 'GET_SEARCH_RESULT',
    nameSearch,
  };
};
