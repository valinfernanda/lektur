import {all, takeLatest, put} from 'redux-saga/effects';
import axios from 'axios';
import {Store} from '../../../Store/Store';

function* SagaSearchResult(payload) {
  try {
    const token = Store.getState().LoginReducer.token;
    // console.log(token, 'ini TOKEN SEARCH RESULT SAGA');
    const Response = yield axios.get(
      `https://lekturapp.herokuapp.com/search?search=${payload.nameSearch}`,
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      },
    );
    console.log(Response, 'INI SEARCH RESULT SAGA RESPONSE');
    const AllSearchResult = Response.data.result;

    yield put({
      type: 'SET_SEARCH_RESULT',
      searchResult: AllSearchResult,
    });
  } catch (error) {
    console.log(error);
  }
}

export function* SagasSearchResult() {
  //INI BUAT NANGKAP ACTION
  yield takeLatest('GET_SEARCH_RESULT', SagaSearchResult);
}
