import React, {useState, useEffect, useCallback} from 'react';
import {View, Text, FlatList, TouchableOpacity, Image} from 'react-native';
import {styles} from '../CourseDetail/style';
//redux
import {connect} from 'react-redux';
import {SearchCourse} from './redux/action';
import {ActionSendId, ActionSendIdToDetail} from '../Home/Redux/action';

const SearchScreen = (props) => {
  console.log(props.route.params.searchValue);
  const nameSearch = props.route.params.searchValue; // ini dipake utk passing data ke API
  useEffect(() => {
    props.SearchCourse(nameSearch);
  }, []);
  const {listSearchResult} = props;

  // bottom read area
  const [textShown, setTextShown] = useState(false); //To show ur remaining Text
  const [lengthMore, setLengthMore] = useState(false); //to show the "Read more & Less Line"
  const toggleNumberOfLines = () => {
    //To toggle the show text or hide it
    setTextShown(!textShown);
  };

  const onTextLayout = useCallback((e) => {
    setLengthMore(e.nativeEvent.lines.length >= 4); //to check the text is more than 4 lines or not
    // console.log(e.nativeEvent);
  }, []);

  const [selectedCategoryID, setSelectedCategoryID] = useState('');
  const filterCourseByCategoryID = (categoryID) => {
    setSelectedCategoryID(categoryID);
  };

  return (
    <View style={styles.searchContainer}>
      <FlatList
        showsVerticalScrollIndicator={false}
        data={listSearchResult.filter((item) => {
          if (selectedCategoryID === '') {
            return !!item.category;
          }
          return !!item.category === selectedCategoryID;
        })}
        ListEmptyComponent={() => {
          return (
            <View>
              <Text>Sorry there's no result for "{nameSearch}"</Text>
            </View>
          );
        }}
        renderItem={({item, index}) => {
          let courseId = item.courseId;
          return (
            <TouchableOpacity
              style={styles.sLessonCover}
              onPress={() => props.ActionSendIdToDetail(courseId)}>
              <Image source={{uri: item.image}} style={styles.apalah}></Image>
              <View style={styles.bottomLsnDesc1}>
                <Text style={styles.btmLsnDscTitle}>{item.title}</Text>
                <Text style={styles.btmLsnDscTeacher}>
                  {/* By {item.fullname[index].fullname} */}
                  {item.fullname.map((value) => (
                    <Text>{value.fullname}</Text>
                  ))}
                </Text>
                <View style={styles.btmLsnDscPlus}>
                  <Text style={styles.btmLsnDscPlusTxt}>
                    {item.totalVideo} Videos
                  </Text>
                  <Text style={styles.btmLsnDscPlusTxt}>
                    {item.totalMaterial} Learning Material
                  </Text>
                </View>
              </View>
              <View style={styles.bottomLsnDesc2}>
                <Text
                  onTextLayout={onTextLayout}
                  numberOfLines={textShown ? undefined : 5}
                  style={styles.courseDetilTxt}>
                  {item.overview}
                </Text>
              </View>
              <View style={styles.bottomLsnDesc3}>
                {item.category.map(
                  (value) =>
                    value && (
                      <Text style={styles.bottomLsnDesc3Txt}>{value}</Text>
                    ),
                )}
              </View>
            </TouchableOpacity>
          );
        }}
      />
    </View>
  );
};

const mapStateToProps = (state) => ({
  listSearchResult: state.SearchResultReducer.searchResult,
});

const mapDispatchToProps = {
  SearchCourse,
  ActionSendId,
  ActionSendIdToDetail,
};

export default connect(mapStateToProps, mapDispatchToProps)(SearchScreen);
