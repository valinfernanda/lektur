const InitialState = {
  detailCourse: [],
  courseContent: [],
};

export const CourseDetailReducer = (state = InitialState, action) => {
  switch (action.type) {
    case 'SET_DETAIL_COURSE':
      return {
        ...state,
        detailCourse: action.detailCourse,
        courseContent: action.courseContent,
      };
    default:
      return state;
  }
};
