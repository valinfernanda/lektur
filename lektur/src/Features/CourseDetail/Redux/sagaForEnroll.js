import {all, takeLatest, put} from 'redux-saga/effects';
import axios from 'axios';
import {Store} from '../../../Store/Store';

function* SagaForEnroll(data) {
  try {
    const token = Store.getState().LoginReducer.token;
    console.log(data, 'ini DATA ENROLL SAGA');
    console.log(token, 'ini TOKEN ENROLL SAGA');
    const Response = yield axios({
      method: 'post',
      url: `https://lekturapp.herokuapp.com/api/student/course/enroll?courseId=${data.enrollCourseId}`,
      headers: {
        Authorization: `Bearer ${token}`,
      },
      validateStatus: (status) => status < 500,
    });
    console.log(Response, 'SUKSES DAPATIN RESPONSE');
    //MAYBE WE NEED TO SEND RESPONSE TO REDUCER BUT WAIT FOR A MOMENT KAY?
  } catch (error) {
    console.log(error);
  }
}

export function* SagaEnroll() {
  //INI BUAT NANGKAP ACTION
  yield takeLatest('POST_ENROLL', SagaForEnroll);
}
