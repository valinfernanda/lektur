export const ActionEnroll = (enrollCourseId) => {
  return {
    type: 'POST_ENROLL',
    enrollCourseId,
  };
};
