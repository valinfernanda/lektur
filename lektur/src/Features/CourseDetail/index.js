import React, {useState, useEffect, useCallback} from 'react';
import {
  ImageBackground,
  View,
  Text,
  ScrollView,
  TouchableHighlight,
  TouchableOpacity,
  Modal,
  FlatList,
} from 'react-native';

//STYLE
import {styles} from './style';
import {modalStyles} from '../Modal/modalstyle';
import {Rectangle4, PopUpImage, machineLearning} from '../../Assets';
//icons
import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';

//COMPONENT
import Read from './Read';
import ReadBottom from './bottomRead';
import {Size} from '../../Shared/Global/Config/Size';
import {SetCourseDetail} from './Redux/action';

import {ActionSendId, ActionSendIdToDetail} from '../Home/Redux/action';
import {connect} from 'react-redux';
import {ActionEnroll} from './Redux/action';

const CourseDetail = (props) => {
  useEffect(() => {
    props.ActionSendId();
    console.log(props.listCourse, 'list Course');
    props.ActionEnroll();
  }, []);
  const {detailCourse} = props;
  const enrollCourseId = detailCourse._id;
  const {courseContent} = props;
  console.log(enrollCourseId, 'PLEASE BACA INI OKAY');

  const [modalVisible, setModalVisible] = useState(false);

  //content area
  const [iconStyle, setIconStyle] = useState(styles.playIcon);
  const [viewBG, setviewBG] = useState(styles.contentCover);
  const [shouldShow, setShouldShow] = useState(true);
  const onPress = () => {
    setIconStyle(styles.playIconBlue);
    setviewBG(styles.contentCoverBlue);
    setShouldShow(!shouldShow);
  };

  // bottom read area
  const [textShown, setTextShown] = useState(false); //To show ur remaining Text
  const [lengthMore, setLengthMore] = useState(false); //to show the "Read more & Less Line"
  const toggleNumberOfLines = () => {
    //To toggle the show text or hide it
    setTextShown(!textShown);
  };

  const [selectedCategoryID, setSelectedCategoryID] = useState('');
  const filterCourseByCategoryID = (categoryID) => {
    setSelectedCategoryID(categoryID);
  };

  const onTextLayout = useCallback((e) => {
    setLengthMore(e.nativeEvent.lines.length >= 4); //to check the text is more than 4 lines or not
    // console.log(e.nativeEvent);
  }, []);
  return (
    <View style={{flex: 1, justifyContent: 'space-between'}}>
      {/* Main Area */}
      <ScrollView
        showsVerticalScrollIndicator={false}
        style={styles.scrollView}>
        <View style={styles.container}>
          {/* TOP CONTAINER AREA */}
          <ImageBackground
            source={{uri: detailCourse.image}}
            style={styles.topContainer}>
            <View style={styles.topContainerTop}>
              <View style={styles.videoDesc}>
                <View style={styles.videoDescTop}>
                  <Text style={styles.videoDescTop2}>{detailCourse.title}</Text>
                  <Text style={styles.btmLsnDscTeacher}>
                    By {detailCourse.teacherId.fullname}
                  </Text>
                  <View style={styles.openButton}>
                    <Text style={styles.textStyle}>
                      {detailCourse.categoryId.categories}
                    </Text>
                  </View>
                </View>
              </View>
            </View>
          </ImageBackground>

          {/* middle area */}
          <View style={styles.topContainerCover}>
            <View style={styles.topContainerBottom}>
              <View style={styles.courseDetil}>
                <View style={styles.courseDetilUp}>
                  <View style={styles.courseDetilUp1}>
                    <Text style={styles.courseDetilNum}>
                      {detailCourse.totalVideo}
                    </Text>
                    <Text style={styles.courseDetilTxt}>Learning Videos</Text>
                  </View>
                  <View style={styles.courseDetilUp1}>
                    <Text style={styles.courseDetilNum}>
                      {detailCourse.totalMaterial}
                    </Text>
                    <Text style={styles.courseDetilTxt}>Study Material</Text>
                  </View>
                </View>
                <View style={styles.courseDetilDown}>
                  {/* course overview on Read component*/}
                  <Read />
                </View>
              </View>
            </View>
          </View>

          {/* bottom container area */}
          <View style={styles.bottomContainer}>
            <Text style={styles.bottomUpText}>Content</Text>
            {/* content list area */}
            <FlatList
              showsVerticalScrollIndicator={false}
              data={courseContent}
              renderItem={({item}) => {
                const contentId = item._id;
                return item.number !== 1 ? (
                  <View style={styles.contentCoverLock}>
                    <MaterialIcons
                      name="lock"
                      size={Size.ms25}
                      style={styles.playIconLock}
                    />
                    <Text style={styles.contentTxt}>
                      Lesson #{item.number}: {item.title}
                    </Text>
                  </View>
                ) : (
                  <TouchableOpacity
                  // onPress={() => {
                  //   props.ActionSendContentId(contentId);
                  // }}
                  >
                    <View style={viewBG}>
                      <AntDesign
                        name="play"
                        size={Size.ms23}
                        style={iconStyle}
                      />
                      <Text style={styles.contentTxtLocked}>
                        Lesson #{item.number}: {item.title}
                      </Text>
                    </View>
                  </TouchableOpacity>
                );
              }}
            />
            {/* end of content list */}

            {/* bottom lesson area */}
            <View style={styles.bottomLessons}>
              <FlatList
                horizontal
                showsHorizontalScrollIndicator={false}
                data={props.listCourse.filter((item) => {
                  if (selectedCategoryID === '') {
                    return !!item.categoryId;
                  }
                  return (
                    !!item.categoryId &&
                    item.categoryId._id === selectedCategoryID
                  );
                })}
                ListEmptyComponent={() => {
                  return (
                    <View style={styles.dataNotFound}>
                      <Text style={styles.dataNotFoundTxt}>
                        SORRY DATA NOT FOUND
                      </Text>
                    </View>
                  );
                }}
                renderItem={({item}) => {
                  let courseId = item._id;
                  return (
                    <TouchableOpacity
                      style={styles.bLessonCover}
                      onPress={() => props.ActionSendIdToDetail(courseId)}>
                      <ImageBackground
                        source={{uri: item.image}}
                        style={styles.apalah}></ImageBackground>
                      <View style={styles.bottomLsnDesc1}>
                        <Text style={styles.btmLsnDscTitle}>{item.title}</Text>
                        <Text style={styles.btmLsnDscTeacher}>
                          By {item.teacherId.fullname}
                        </Text>
                        <View style={styles.btmLsnDscPlus}>
                          <Text style={styles.btmLsnDscPlusTxt}>
                            {item.totalVideo} Videos
                          </Text>
                          <Text style={styles.btmLsnDscPlusTxt}>
                            {item.totalMaterial} Learning Material
                          </Text>
                        </View>
                      </View>
                      <View style={styles.bottomLsnDesc2}>
                        <Text
                          onTextLayout={onTextLayout}
                          numberOfLines={textShown ? undefined : 5}
                          style={styles.courseDetilTxt}>
                          {item.overview}
                        </Text>
                      </View>
                      <View style={styles.bottomLsnDesc3}>
                        {item.categoryId && (
                          <Text style={styles.bottomLsnDesc3Txt}>
                            {item.categoryId.categories}
                          </Text>
                        )}
                      </View>
                    </TouchableOpacity>
                  );
                }}
              />
            </View>
            {/* end of bottom lesson area */}
          </View>
        </View>
      </ScrollView>
      {/* End of Main Area */}

      {/* Bottom Footer Area */}
      <View style={styles.bottomFooter}>
        <View style={styles.bottomFooterCover}>
          <View style={styles.buttomTitleCourse}>
            <Text style={styles.footerTitle}>{detailCourse.title}</Text>
            <Text style={styles.footerTeacher}>
              {detailCourse.teacherId.fullname}
            </Text>
          </View>
          <TouchableOpacity
            style={styles.openButtonFooter}
            onPress={() => {
              props.ActionEnroll(enrollCourseId);
              setModalVisible(true);
            }}>
            <Text style={styles.textStyle}>Enroll Now</Text>
          </TouchableOpacity>
        </View>
      </View>
      {/* End of Bottom Footer */}

      {/* Modal Area */}
      <Modal
        animationType={'slide'}
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          console.log('Modal has been closed.');
        }}>
        {/*All views of Modal*/}
        <View style={modalStyles.background}></View>
        <View style={modalStyles.mCover}>
          <View style={modalStyles.modalView}>
            <TouchableOpacity
              onPress={() => {
                setModalVisible(!modalVisible);
              }}>
              <Ionicons name="close" size={30} style={modalStyles.textStyle} />
            </TouchableOpacity>
            <View style={modalStyles.modalMain}>
              <Text style={modalStyles.modalTitle}>Successfully enrolled!</Text>
              <ImageBackground
                source={{uri: detailCourse.image}}
                style={modalStyles.modalImage}></ImageBackground>
              <Text style={modalStyles.modalLessonT}>{detailCourse.title}</Text>
              <Text style={modalStyles.modalLessonA}>
                By {detailCourse.teacherId.fullname}
              </Text>
            </View>
          </View>
          <View style={modalStyles.modalFooter}>
            <Text>Please wait corresponding teacher approve you!</Text>
          </View>
        </View>
      </Modal>
      {/* End of Modal Area */}
    </View>
  );
};

const mapStateToProps = (state) => ({
  detailCourse: state.CourseDetailReducer.detailCourse,
  listCourse: state.HomeReducer.courses,
  courseContent: state.CourseDetailReducer.courseContent,
});

const mapDispatchToProps = {
  SetCourseDetail,
  ActionSendId,
  ActionEnroll,
  ActionSendIdToDetail,
};

export default connect(mapStateToProps, mapDispatchToProps)(CourseDetail);
