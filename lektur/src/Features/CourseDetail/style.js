//STYLES AREA
import {Size} from '../../Shared/Global/Config/Size';
import {Color} from '../../Shared/Global/Config/Color';
import {StyleSheet, StatusBar} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    // paddingTop: StatusBar.currentHeight,
    justifyContent: 'space-between',
    backgroundColor: 'white',
  },
  topContainer: {
    height: Size.h65,
  },
  topContainerCover: {
    marginHorizontal: Size.wp5,
    marginTop: Size.hmin37,
    resizeMode: 'contain',
  },
  topContainerTop: {
    flex: 0.5,
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingHorizontal: Size.wp5,
  },
  topContainerBottom: {
    flex: 1,
  },
  courseDetil: {
    backgroundColor: 'white',
    flex: 1,
    padding: Size.wp7,
    elevation: 5,
  },
  courseDetilUp: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderBottomWidth: Size.ms1,
    borderBottomColor: 'silver',
    paddingBottom: Size.wp5,
  },
  courseDetilUp1: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  courseDetilDown: {
    flex: 2,
    paddingTop: Size.wp5,
  },
  videoDesc: {
    marginTop: Size.hmin7,
  },
  videoDescTop: {
    flex: 1,
    justifyContent: 'center',
    // padding: Size.wp3,
    // backgroundColor: 'blue',
  },
  videoDescBottom: {
    flex: 1,
    // alignItems: 'flex-end',
    justifyContent: 'center',
    paddingRight: Size.wp5,
  },

  //BOTTOM CONTAINER AREA
  bottomContainer: {
    resizeMode: 'contain',
    marginTop: Size.h1,
    backgroundColor: 'white',
    padding: Size.wp5,
    paddingTop: Size.wp10,
  },
  bottomLessons: {
    marginTop: Size.h8,
  },
  bLessonCover: {
    width: Size.wp72,
    borderWidth: 0.01,
    marginRight: Size.wp6,
    // color: 'red',
    elevation: 5,
  },
  bottomLsnDesc1: {
    flex: 2,
    padding: Size.wp3,
    backgroundColor: 'white',
  },
  bottomLsnDesc2: {
    paddingHorizontal: Size.wp3,
    paddingBottom: Size.wp3,
    backgroundColor: 'white',
    // flex: 3,
    height: Size.h17,
  },
  bottomLsnDesc3: {
    height: Size.h8,
    padding: Size.wp3,
    backgroundColor: Color.pink,
    justifyContent: 'center',
  },
  btmLsnDscPlus: {
    // flex: 1,
    flexDirection: 'row',
    alignContent: 'flex-end',
    paddingTop: Size.wp4,
  },

  //BOTTOM FOOTER
  bottomFooter: {
    marginHorizontal: Size.hmin3,
    backgroundColor: Color.blueSoft,
    // borderWidth: Size.ms1,
    elevation: 5,
    zIndex: 100,
  },
  bottomFooterCover: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignContent: 'center',
    paddingHorizontal: Size.wp13,
    paddingVertical: Size.wp5,
    paddingBottom: Size.wp3,
  },
  buttomTitleCourse: {
    flex: 2,
  },

  //COMPONENT

  openButton: {
    alignSelf: 'flex-start',
    backgroundColor: Color.orange,
    padding: Size.h1,
    borderRadius: Size.ms3,
    opacity: 0.5,
    elevation: 2,
  },
  openButtonFooter: {
    flex: 1,
    alignSelf: 'flex-start',
    backgroundColor: Color.orange,
    padding: Size.h1,
    borderRadius: Size.ms3,
    elevation: 2,
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: Size.ms15,
    textAlign: 'center',
  },
  textStyle2: {
    color: 'black',
    fontSize: Size.ms16,
  },
  courseDetilNum: {
    color: Color.orange,
    fontSize: Size.ms28,
    fontWeight: 'bold',
  },
  courseDetilTxt: {
    fontSize: Size.ms15,
    lineHeight: 25,
  },
  videoDescTop2: {
    fontWeight: 'bold',
    fontSize: Size.ms20,
    color: 'white',
    marginBottom: Size.h1,
  },
  videoDescTop3: {
    fontSize: Size.ms18,
    marginBottom: Size.h2,
    color: '#E5E5E5',
  },
  bottomUpText: {
    fontWeight: 'bold',
    fontSize: Size.ms20,
    marginBottom: Size.wp7,
  },
  bottonUpButton: {
    backgroundColor: 'white',
    padding: Size.wp5,
    borderBottomWidth: Size.ms1,
    borderColor: 'silver',
    borderRadius: Size.ms4,
    marginHorizontal: Size.wp5,
    color: Color.darkblue,
  },
  bottomUpList: {
    marginBottom: Size.wp10,
    borderWidth: Size.ms1,
    borderColor: Color.silver,
  },
  bottomDescTxt: {
    fontSize: Size.ms15,
  },
  btmLsnDscTitle: {
    fontWeight: 'bold',
    fontSize: Size.ms20,
  },
  btmLsnDscTeacher: {
    marginBottom: Size.h2,
    fontSize: Size.ms16,
    color: 'silver',
  },
  btmLsnDscPlusTxt: {
    color: 'silver',
    paddingRight: Size.wp9,
  },
  bottomLsnDesc3Txt: {
    fontSize: Size.ms18,
  },
  courseDetilDownTxt: {
    fontSize: Size.ms15,
  },
  footerTitle: {
    fontWeight: 'bold',
    fontSize: Size.ms16,
    lineHeight: 21,
  },
  footerTeacher: {
    fontSize: Size.ms16,
    color: 'silver',
    lineHeight: 21,
  },
  playIcon: {
    marginRight: Size.wp3,
  },
  contentCover: {
    flexDirection: 'row',
    padding: Size.wp5,
    borderBottomWidth: Size.ms1,
    borderColor: 'silver',
    alignItems: 'center',
  },
  playIconBlue: {
    marginRight: Size.wp3,
    color: Color.bluelight,
  },
  contentCoverBlue: {
    flexDirection: 'row',
    padding: Size.wp5,
    borderBottomWidth: Size.ms1,
    borderColor: 'silver',
    alignItems: 'center',
    backgroundColor: Color.blueSoft,
  },
  contentCoverLock: {
    flexDirection: 'row',
    padding: Size.wp5,
    borderBottomWidth: Size.ms1,
    borderColor: 'silver',
    alignItems: 'center',
    backgroundColor: Color.platinum,
  },
  playIconLock: {
    marginRight: Size.wp3,
    color: Color.darkGrey,
  },
  contentTxt: {
    color: 'black',
  },
  contentTxtLock: {
    color: Color.darkGrey,
  },
  apalah: {
    width: '100%',
    height: Size.h22,
  },
  //SEARCH PAGE STYLE
  searchContainer: {
    backgroundColor: 'white',
    alignItems: 'center',
  },
  sLessonCover: {
    marginTop: Size.h5,
    width: Size.wp72,
    borderWidth: 0.01,
    // marginRight: Size.wp6,
    // color: 'red',
    elevation: 5,
  },
});
