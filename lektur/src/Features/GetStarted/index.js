import React from 'react';
import {View, Text, StyleSheet, ImageBackground} from 'react-native';
import {BGGetStarted} from '../../Assets';
import Button from '../../Components/atoms/Button';
import Gap from '../../Components/atoms/Gap';
import {ActionRole} from '../../Features/Auth/Register/Redux/action';

import {connect} from 'react-redux';
import {Size} from '../../Shared/Global/Config/Size';

const GetStarted = ({navigation, ActionRole}) => {
  return (
    <ImageBackground source={BGGetStarted} style={styles.page}>
      <View>
        {/* <AntDesign name="meho" size={50} color="black" /> */}
        <Text style={styles.title}>Teach and Learn</Text>
        <Text style={styles.title2}>from anywhere...</Text>
      </View>
      <View>
        <Text style={styles.asking}>Who am I? </Text>
        <Gap height={15} />
        <View style={styles.pilihan}>
          <Button
            title="Student"
            onPress={() => {
              navigation.navigate('Register');
              ActionRole(0);
            }}
          />
          <Gap width={16} />
          <Button
            type="secondary"
            title="Teacher"
            onPress={() => {
              navigation.navigate('Register');
              ActionRole(1);
            }}
          />
        </View>
      </View>
    </ImageBackground>
  );
};

const mapStateToProps = (state) => ({});

const mapDispatchToProps = {
  ActionRole,
};

export default connect(mapStateToProps, mapDispatchToProps)(GetStarted);

const styles = StyleSheet.create({
  page: {
    padding: Size.wp10,
    justifyContent: 'space-between',
    // backgroundColor: colors.white,
    flex: 1,
  },
  asking: {
    // backgroundColor: 'white',
    alignSelf: 'center',
    fontSize: Size.ms28,
    fontWeight: 'bold',
  },
  title: {
    fontSize: Size.ms28,
    marginTop: Size.wp14,
    fontWeight: 'bold',
    fontFamily: 'Roboto-Medium.ttf',
  },
  title2: {
    fontSize: Size.ms28,
    marginTop: Size.wp14,
    fontWeight: 'bold',
    fontFamily: 'Roboto-Medium.ttf',
    paddingLeft: Size.wp15,
  },
  pilihan: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginBottom: Size.wp13,
  },
});
