import React, {useState, useEffect, useCallback} from 'react';
import {
  ImageBackground,
  Image,
  View,
  Text,
  ScrollView,
  TouchableHighlight,
  Modal,
  FlatList,
  Animated,
  Easing,
  Dimensions,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import {connect} from 'react-redux';
import {ActionSendId, ActionSendIdToDetail} from './Redux/action';
import Video from 'react-native-video';
import {SearchBar} from 'react-native-elements';

//STYLE
import {styles} from './style';
import {modalStyles} from '../Modal/modalstyle';
import {
  BGClassroom,
  Vector,
  PopUpImage,
  ml,
  Love,
  DummyPosters,
} from '../../Assets';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Fontisto from 'react-native-vector-icons/Fontisto';
import {navigate} from '../Utils/Nav';

const Home = (props) => {
  const [rerender, setRerender] = useState(false);
  useEffect(() => {
    props.ActionSendId();
    setRerender(true);
    console.log(props.listCourse, 'woy ini listCoursenya');
  });

  const {mostPopular} = props;

  const [modalVisible, setModalVisible] = useState(false);

  // bottom read area
  const [textShown, setTextShown] = useState(false); //To show ur remaining Text
  const [lengthMore, setLengthMore] = useState(false); //to show the "Read more & Less Line"
  const toggleNumberOfLines = () => {
    //To toggle the show text or hide it
    setTextShown(!textShown);
  };

  const onTextLayout = useCallback((e) => {
    setLengthMore(e.nativeEvent.lines.length >= 4); //to check the text is more than 4 lines or not
    // console.log(e.nativeEvent);
  }, []);
  // end of bottom read area
  const [selectedCategoryID, setSelectedCategoryID] = useState('');
  const filterCourseByCategoryID = (categoryID) => {
    setSelectedCategoryID(categoryID);
  };
  //SEARCH AREA

  const screenWidth = Dimensions.get('screen').width;
  const translateSearchBar = React.useRef(new Animated.Value(-100)).current;
  const opacitySearchBar = React.useRef(new Animated.Value(0)).current;
  const [searchValue, setSearchValue] = useState('');

  const displaySearchBar = () => {
    Animated.timing(translateSearchBar, {
      toValue: 0,
      easing: Easing.linear(),
      duration: 700,
      useNativeDriver: true,
    }).start();
    Animated.timing(opacitySearchBar, {
      toValue: 1,
      easing: Easing.linear(),
      duration: 700,
      useNativeDriver: true,
    }).start();
  };

  const hideSearchBar = () => {
    Animated.timing(translateSearchBar, {
      toValue: -100,
      easing: Easing.linear(),
      duration: 700,
      useNativeDriver: true,
    }).start();
    Animated.timing(opacitySearchBar, {
      toValue: 0,
      easing: Easing.linear(),
      duration: 300,
      useNativeDriver: true,
    }).start();
  };

  const gotoSearchScreen = () => {
    navigate('SearchScreen', {searchValue});
  };
  //END OF SEARCH AREA
  console.log(mostPopular.video, 'INI DATA VIDEO');
  return (
    <>
      {/* MAIN AREA */}
      <View style={styles.header}>
        <ImageBackground
          source={DummyPosters}
          style={styles.logoLektur}></ImageBackground>

        <TouchableOpacity
          style={{
            alignItems: 'center',
            justifyContent: 'center',
            paddingRight: 10,
          }}
          onPress={displaySearchBar}>
          <Fontisto name="search" size={25} style={modalStyles.textStyle} />
        </TouchableOpacity>
        <Animated.View
          style={{
            flexDirection: 'row',
            position: 'absolute',
            top: 0,
            left: 0,
            right: 0,
            backgroundColor: 'white',
            justifyContent: 'space-between',
            alignItems: 'center',
            height: 74,
            alignContent: 'center',
            transform: [{translateY: translateSearchBar}],
            // opacity: opacitySearchBar,
          }}>
          {/* <SearchBar
            placeholder="Type Here..."
            containerStyle={{
              backgroundColor: 'white',
              borderWidth: 0,
            }}
            inputContainerStyle={{backgroundColor: 'white'}}
            showCancel={true}
          /> */}

          <TextInput
            placeholder={'search'}
            onSubmitEditing={() => gotoSearchScreen()}
            onChangeText={(value) => setSearchValue(value)}></TextInput>
          <TouchableOpacity onPress={hideSearchBar}>
            <Ionicons name="close" size={30} style={modalStyles.textStyle} />
          </TouchableOpacity>
        </Animated.View>
      </View>
      <ScrollView
        showsVerticalScrollIndicator={false}
        style={styles.scrollView}>
        <View style={styles.container}>
          {/* TOP CONTAINER AREA */}
          <ImageBackground source={BGClassroom} style={styles.topContainer}>
            <View style={styles.topContainerTop}>
              <Text style={styles.topContainerTopText}>
                Bring your class at home
              </Text>
            </View>
          </ImageBackground>
          {/* MIDDLE AREA */}

          <View style={styles.topContainerCover}>
            <View style={styles.topContainerBottom}>
              <View style={styles.videoContainer}>
                {mostPopular.mostPopularCourse && (
                  <Video
                    source={{
                      uri: mostPopular.video,
                    }}
                    style={styles.backgroundVideo}
                    resizeMode={'cover'}
                    controls={true}
                    paused
                    poster={mostPopular.mostPopularCourse[0].image}
                    // poster={mostPopular.mostPopularCourse[0].image}
                  />
                )}
              </View>
              <FlatList
                showsHorizontalScrollIndicator={false}
                data={mostPopular.mostPopularCourse}
                renderItem={({item}) => {
                  return (
                    <View style={styles.videoDesc}>
                      <View style={styles.videoDescTop}>
                        <Text style={styles.videoDescTop1}>
                          {item.totalEnrolled} Enrolled
                        </Text>
                        <Text style={styles.videoDescTop2}>{item.title}</Text>
                        <Text style={styles.videoDescTop3}>
                          {item.teacherId.fullname}
                        </Text>
                      </View>
                      <View style={styles.videoDescBottom}>
                        <TouchableHighlight
                          style={styles.openButton}
                          onPress={() => {
                            setModalVisible(true);
                          }}>
                          <Text style={styles.textStyle}>Enroll Now</Text>
                        </TouchableHighlight>
                      </View>
                    </View>
                  );
                }}
              />
            </View>
          </View>

          {/* BOTTOM CONTAINER AREA */}

          <View style={styles.bottomContainer}>
            <View style={styles.bottomUp}>
              <Text style={styles.bottomUpText}>What to learn next</Text>

              <FlatList
                horizontal
                showsHorizontalScrollIndicator={false}
                data={props.listCategory}
                renderItem={({item}) => {
                  return (
                    <View style={styles.bottomUpList}>
                      <TouchableOpacity
                        style={styles.bottonUpButton}
                        onPress={() => {
                          filterCourseByCategoryID(item._id);
                        }}>
                        <Text style={styles.textStyle2}>{item.categories}</Text>
                      </TouchableOpacity>
                    </View>
                  );
                }}
              />
            </View>

            {/* bottom lesson area */}
            <View style={styles.bottomLessons}>
              <FlatList
                horizontal
                showsHorizontalScrollIndicator={false}
                data={props.listCourse.filter((item) => {
                  if (selectedCategoryID === '') {
                    return !!item.categoryId;
                  }
                  return (
                    !!item.categoryId &&
                    item.categoryId._id === selectedCategoryID
                  );
                })}
                ListEmptyComponent={() => {
                  return (
                    <View style={styles.dataNotFound}>
                      <Text style={styles.dataNotFoundTxt}>
                        SORRY DATA NOT FOUND
                      </Text>
                    </View>
                  );
                }}
                renderItem={({item}) => {
                  let courseId = item._id;
                  return (
                    <TouchableOpacity
                      style={styles.bLessonCover}
                      onPress={() => props.ActionSendIdToDetail(courseId)}>
                      <Image
                        source={{uri: item.image}}
                        style={styles.apalah}></Image>
                      <View style={styles.bottomLsnDesc1}>
                        <Text style={styles.btmLsnDscTitle}>{item.title}</Text>
                        <Text style={styles.btmLsnDscTeacher}>
                          By {item.teacherId.fullname}
                        </Text>
                        <View style={styles.btmLsnDscPlus}>
                          <Text style={styles.btmLsnDscPlusTxt}>
                            {item.totalVideo} Videos
                          </Text>
                          <Text style={styles.btmLsnDscPlusTxt}>
                            {item.totalMaterial} Learning Material
                          </Text>
                        </View>
                      </View>
                      <View style={styles.bottomLsnDesc2}>
                        <Text
                          onTextLayout={onTextLayout}
                          numberOfLines={textShown ? undefined : 5}
                          style={styles.courseDetilTxt}>
                          {item.overview}
                        </Text>
                      </View>
                      <View style={styles.bottomLsnDesc3}>
                        {item.categoryId && (
                          <Text style={styles.bottomLsnDesc3Txt}>
                            {item.categoryId.categories}
                          </Text>
                        )}
                      </View>
                    </TouchableOpacity>
                  );
                }}
                // keyExtractor={(item) => item.id.toString()}
              />
            </View>
            {/* end of bottom lesson area */}
          </View>
        </View>
      </ScrollView>
      {/* END OF MAIN AREA */}

      {/* MODAL AREA */}
      <Modal
        animationType={'slide'}
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          console.log('Modal has been closed.');
        }}>
        {/*All views of Modal*/}
        <View style={modalStyles.background}></View>
        <View style={modalStyles.mCover}>
          <View style={modalStyles.modalView}>
            <TouchableHighlight
              onPress={() => {
                setModalVisible(!modalVisible);
              }}>
              <Ionicons name="close" size={30} style={modalStyles.textStyle} />
            </TouchableHighlight>
            <View style={modalStyles.modalMain}>
              <Text style={modalStyles.modalTitle}>Successfully enrolled!</Text>
              <ImageBackground
                source={{
                  uri:
                    mostPopular.mostPopularCourse &&
                    mostPopular.mostPopularCourse[0].image,
                }}
                style={modalStyles.modalImage}></ImageBackground>
              <Text style={modalStyles.modalLessonT}>
                {mostPopular.mostPopularCourse &&
                  mostPopular.mostPopularCourse[0].title}
              </Text>
              <Text style={modalStyles.modalLessonA}>
                By{' '}
                {mostPopular.mostPopularCourse &&
                  mostPopular.mostPopularCourse[0].teacherId.fullname}
              </Text>
            </View>
          </View>
          <View style={modalStyles.modalFooter}>
            <Text>Please wait corresponding teacher approve you!</Text>
          </View>
        </View>
      </Modal>
      {/* END OF MODAL AREA */}
    </>
  );
};

const mapStateToProps = (state) => ({
  listCourse: state.HomeReducer.courses,
  listCategory: state.HomeReducer.categories,
  mostPopular: state.HomeReducer.mostPopulars,
  // searchResult: state.HomeReducer.searchResult,
});

const mapDispatchToProps = {
  ActionSendId,
  ActionSendIdToDetail,
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
