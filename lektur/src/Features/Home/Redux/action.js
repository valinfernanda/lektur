export const ActionSendId = (token) => {
  return {
    type: 'POST_ID',
    token,
  };
};

export const ActionSendIdToDetail = (courseId) => {
  return {
    type: 'POST_ID_TO_DETAIL',
    courseId,
  };
};
// export const SearchCourse = (nameSearch) => {
//   return {
//     type: 'SEARCH_COURSE',
//     nameSearch,
//   };
// };

export const ShowByCategory = (categoryId) => {
  return {
    type: 'SHOW_BY_CATEGORY',
    categoryId,
  };
};
