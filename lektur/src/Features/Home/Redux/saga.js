import {all, takeLatest, put} from 'redux-saga/effects';
import axios from 'axios';
import {Store} from '../../../Store/Store';

function* SagaGetCourses({token}) {
  try {
    const Token = Store.getState().LoginReducer.token;
    console.log(token, 'ini TOKEN HOME SAGA');
    const Response = yield axios.get('https://lekturapp.herokuapp.com/all', {
      headers: {
        Authorization: `Bearer ${token ? token : Token}`,
      },
    });
    console.log(Response, 'INI HOME SAGA RESPONSE');
    const AllCoursesData = Response.data.data.course;
    const AllCoursesCategory = Response.data.data.category;
    const MostPopular = Response.data.data.mostPopular;

    yield put({
      type: 'SET_COURSES',
      courses: AllCoursesData,
      categories: AllCoursesCategory,
      mostPopulars: MostPopular,
    });
  } catch (error) {
    console.log(error);
  }
}

export function* SagaHome() {
  //INI BUAT NANGKAP ACTION
  yield takeLatest('POST_ID', SagaGetCourses);
}
