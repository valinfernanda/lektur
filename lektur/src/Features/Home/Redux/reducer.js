const InitialState = {
  courses: [],
  categories: [],
  mostPopulars: {},
  // searchResult: [],
};

export const HomeReducer = (state = InitialState, action) => {
  switch (action.type) {
    case 'SET_COURSES':
      return {
        ...state,
        courses: action.courses,
        categories: action.categories,
        mostPopulars: action.mostPopulars,
      };
    // case 'SET_SEARCH_RESULT':
    //   return {
    //     ...state,
    //     searchResult: action.searchResult,
    //   };
    default:
      return state;
  }
};
