import {all, takeLatest, put} from 'redux-saga/effects';
import axios from 'axios';
import {Store} from '../../../Store/Store';

function* SagaSearchCourse(payload) {
  try {
    const token = Store.getState().LoginReducer.token;
    console.log(token, 'ini TOKEN HOME SAGA');
    const Response = yield axios.get(
      `https://lekturapp.herokuapp.com/category/course?categoryId=${payload.categoryId}`,
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      },
    );
    console.log(Response, 'INI SEARCH SAGA RESPONSE');
    const SearchResult = Response.data.result;

    yield put({
      type: 'SET_SEARCH_RESULT',
      courses: SearchResult,
    });
  } catch (error) {
    console.log(error);
  }
}

export function* SagasSearchCourse() {
  //INI BUAT NANGKAP ACTION
  yield takeLatest('SEARCH_COURSE', SagaSearchCourse);
}
