import {all, takeLatest, put} from 'redux-saga/effects';
import axios from 'axios';
import {navigate} from '../../Utils/Nav';
import jwt from 'jwt-decode';
import {Alert} from 'react-native';
import {Store} from '../../../Store/Store';
// import {SetCourseDetail} from '../../Course/CourseDetail/Redux/action';

function* SagaToCourseDetail(data) {
  try {
    console.log(data);
    const Response = yield axios.get(
      `https://lekturapp.herokuapp.com/api/courses/detail?courseId=${data.courseId}`,
    );

    if (Response.status === 200) {
      console.log(Response, 'INI sagaToCourseDetail SAGA RESPONSE');
      const CourseDetail = Response.data.result.course;
      const CourseContent = Response.data.result.content;
      yield put({
        type: 'SET_DETAIL_COURSE',
        detailCourse: CourseDetail,
        courseContent: CourseContent,
      });

      yield put(navigate('CourseDetail', {}));
    } else {
      yield put({
        type: 'LOGIN_ERROR',
        errormessage: Response.data.message,
      });
    }
  } catch (error) {
    console.log(error);
  }
}

export function* ToCourseDetail() {
  //POST SIGN INI SESUAIIN SAMA ACTION WEY
  yield takeLatest('POST_ID_TO_DETAIL', SagaToCourseDetail);
}
