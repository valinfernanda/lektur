import React, {useState, useEffect, useCallback} from 'react';
import {View, Text, FlatList} from 'react-native';
import {connect} from 'react-redux';
import {GetCourses} from './Redux/action';
//component
import {styles} from './style';

const ReadBottom = (props) => {
  const [textShown, setTextShown] = useState(false); //To show ur remaining Text
  const [lengthMore, setLengthMore] = useState(false); //to show the "Read more & Less Line"
  const toggleNumberOfLines = () => {
    //To toggle the show text or hide it
    setTextShown(!textShown);
  };

  const onTextLayout = useCallback((e) => {
    setLengthMore(e.nativeEvent.lines.length >= 4); //to check the text is more than 4 lines or not
    // console.log(e.nativeEvent);
  }, []);

  return (
    <View style={{flex: 1}}>
      <Text
        onTextLayout={onTextLayout}
        numberOfLines={textShown ? undefined : 5}
        style={styles.courseDetilTxt}>
        yayayay
      </Text>

      {/* {lengthMore ? (
        <Text
          onPress={toggleNumberOfLines}
          style={{lineHeight: 25, fontWeight: 'bold'}}>
          {textShown ? 'Read less...' : 'Read more...'}
        </Text>
      ) : null} */}
    </View>
  );
};

export default ReadBottom;
