//STYLES AREA
import {Size} from '../../Shared/Global/Config/Size';
import {Color} from '../../Shared/Global/Config/Color';
import {StyleSheet, StatusBar} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    resizeMode: 'contain',
    // paddingTop: StatusBar.currentHeight,
    // justifyContent: 'space-between',
  },
  topContainer: {
    height: Size.h65,
    zIndex: -100,
  },
  topContainerCover: {
    // flex: 1,
    height: Size.h50,
    marginHorizontal: Size.wp5,
    marginTop: Size.hmin33,
  },
  topContainerTop: {
    flex: 0.5,
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingHorizontal: Size.wp5,
  },
  topContainerBottom: {
    // backgroundColor: 'black',
    flex: 3,
  },
  videoContainer: {
    flex: 7,
    elevation: 5,
  },
  videoDesc: {
    backgroundColor: 'white',
    flex: 3,
    elevation: 5,
    // marginTop: Size.h5,
    paddingBottom: Size.h3,
  },
  videoDescTop: {
    flex: 1,
    justifyContent: 'space-between',
    padding: Size.wp3,
    // backgroundColor: 'blue',
  },
  videoDescBottom: {
    flex: 1,
    alignItems: 'flex-end',
    justifyContent: 'center',
    paddingRight: Size.wp5,
    // paddingBottom: Size.h5,
  },

  //BOTTOM CONTAINER AREA
  bottomContainer: {
    resizeMode: 'contain',
    marginHorizontal: Size.wp5,
    marginVertical: Size.wp20,
  },
  bottomUp: {
    flex: 2,
    justifyContent: 'flex-end',
  },
  bottomDown: {
    flex: 4,
    // backgroundColor: 'brown',
  },
  bottomLessons: {
    // backgroundColor: 'blue',
  },
  bLessonCover: {
    width: Size.wp72,
    borderWidth: 0.01,
    marginRight: Size.wp6,
    color: 'red',
    elevation: 5,
  },
  bottomLessonsPict: {
    flex: 2,
    width: '80%',
    backgroundColor: 'black',
  },
  bottomLessonsDesc: {
    flex: 3,
    width: '80%',
  },
  bottomLsnDesc1: {
    flex: 2,
    padding: Size.wp3,
    backgroundColor: 'white',
  },
  bottomLsnDesc2: {
    paddingHorizontal: Size.wp3,
    paddingBottom: Size.wp3,
    backgroundColor: 'white',
    // flex: 3,
    height: Size.h17,
  },
  bottomLsnDesc3: {
    height: Size.h8,
    padding: Size.wp3,
    backgroundColor: Color.pink,
    justifyContent: 'center',
  },
  btmLsnDscCver: {
    flex: 2,
  },
  btmLsnDscPlus: {
    flexDirection: 'row',
    alignContent: 'flex-end',
    paddingTop: Size.wp4,
  },

  //COMPONENT
  topContainerTopText: {
    fontWeight: 'bold',
    fontSize: Size.ms40,
    color: Color.darkblue,
  },
  openButton: {
    backgroundColor: Color.orange,
    padding: Size.h1,
    width: Size.wp30,
    borderRadius: Size.ms3,
    elevation: 2,
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  textStyle2: {
    color: Color.darkblue,
    fontWeight: 'bold',
    fontSize: Size.ms16,
    textAlign: 'center',
  },
  videoDescTop1: {
    fontWeight: 'bold',
    fontSize: Size.ms16,
    color: Color.bluelight,
  },
  videoDescTop2: {
    fontWeight: 'bold',
    fontSize: Size.ms20,
    color: 'black',
  },
  videoDescTop3: {
    fontSize: Size.ms18,
    color: '#E5E5E5',
  },
  bottomUpList: {
    flexDirection: 'row',
  },
  bottomUpText: {
    fontWeight: 'bold',
    fontSize: Size.ms28,
  },
  bottonUpButton: {
    backgroundColor: 'white',
    padding: Size.h1,
    borderWidth: Size.ms1,
    borderColor: Color.darkblue,
    borderRadius: Size.ms4,
    marginTop: Size.wp7,
    marginRight: Size.wp3,
    marginBottom: Size.wp13,
    color: Color.darkblue,
    elevation: 2,
  },
  bottomDescTxt: {
    fontSize: Size.ms15,
  },
  btmLsnDscTitle: {
    fontWeight: 'bold',
    fontSize: Size.ms20,
  },
  btmLsnDscTeacher: {
    fontSize: Size.ms16,
    color: 'silver',
  },
  btmLsnDscPlusTxt: {
    color: 'silver',
    paddingRight: Size.wp9,
  },
  bottomLsnDesc3Txt: {
    fontSize: Size.ms18,
  },
  apalah: {
    width: '100%',
    height: Size.h22,
  },
  backgroundVideo: {
    // position: 'absolute',
    height: '100%',
    width: '100%',
  },
  header: {
    flexDirection: 'row',
    backgroundColor: 'white',
    height: Size.h10,
    width: '100%',
    elevation: 8,
    justifyContent: 'space-between',
    // alignItems: 'flex-end',
  },
  logoLektur: {
    height: '100%',
    width: Size.wp25,
  },
  dataNotFoundTxt: {
    fontSize: Size.ms24,
    fontWeight: 'bold',
    alignSelf: 'center',
    color: Color.red,
    paddingLeft: Size.wp7,
    paddingTop: Size.h2,
  },
  courseDetilTxt: {
    fontSize: Size.ms14,
    lineHeight: 21,
  },
});
