import {all, takeLatest, put} from 'redux-saga/effects';
import axios from 'axios';
import {Store} from '../../../../Store/Store';
import {navigate} from '../../../Utils/Nav';

function* SagaToCourseContent(payload) {
  try {
    const token = Store.getState().LoginReducer.token;
    console.log(token, 'ini TOKEN To CONTENTS');
    console.log(payload, 'ini payload To CONTENTS');
    const Response = yield axios.get(
      `https://lekturapp.herokuapp.com/api/student/pop-up/course/content?courseId=${payload.courseId}`,
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      },
    );
    console.log(Response, 'INI sagaToContent RESPONSE');
    const AllCoursesData = Response.data.result;
    yield put({type: 'SET_COURSE_CONTENT', contents: AllCoursesData}); //TO COURSECONTENT REDUCER
    yield put(navigate('CourseContent', {}));
  } catch (error) {
    console.log(error);
  }
}

export function* SagasToCourseContent() {
  //INI BUAT NANGKAP ACTION
  yield takeLatest('POST_STUDENT_COURSE_ID', SagaToCourseContent);
}
