export const ActionSendStudentCourseId = (courseId) => {
  return {
    type: 'POST_STUDENT_COURSE_ID',
    courseId,
  };
};
