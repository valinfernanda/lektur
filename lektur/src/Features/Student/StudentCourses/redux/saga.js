import {all, takeLatest, put} from 'redux-saga/effects';
import axios from 'axios';
import {Store} from '../../../../Store/Store';

function* SagaGetStudentCourses() {
  try {
    const token = Store.getState().LoginReducer.token;
    console.log(token, 'ini TOKEN STUDENT courses');
    const Response = yield axios.get(
      'https://lekturapp.herokuapp.com/api/student/profile',
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      },
    );
    console.log(Response, 'INI student courses SAGA RESPONSE');
    const AllCoursesData = Response.data.result.course;
    yield put({type: 'SET_STUDENT_COURSES', courses: AllCoursesData});
  } catch (error) {
    console.log(error);
  }
}

export function* SagasGetStudentCourses() {
  //INI BUAT NANGKAP ACTION
  yield takeLatest('POST_STUDENT_COURSE_ID', SagaGetStudentCourses);
}
