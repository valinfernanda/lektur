const InitialState = {
  courses: [],
};

export const StudentCoursesReducer = (state = InitialState, action) => {
  switch (action.type) {
    case 'SET_STUDENT_COURSES':
      return {
        ...state,
        courses: action.courses,
      };
    default:
      return state;
  }
};
