import React, {useEffect} from 'react';
import {View, Text, Image, StyleSheet, FlatList} from 'react-native';
import {DummyPosters} from '../../../Assets';
import {Button, ButtonLesson, Gap, Link} from '../../../Components/atoms';
import * as Progress from 'react-native-progress';
import {Size} from '../../../Shared/Global/Config/Size';
//redux
import {connect} from 'react-redux';
import {ActionSendStudentCourseId} from './redux/action';
import {Color} from '../../../Shared/Global/Config/Color';
import {CourseMaterials} from '../CourseMaterial/redux/action';

const Course = (props) => {
  console.log(props);
  useEffect(() => {
    props.ActionSendStudentCourseId();
    props.CourseMaterials();
  }, []);

  // const {StudentCourses} = props;
  // const progres = (
  //   StudentCourses.totalSeenCourses / StudentCourses.totalCourse
  // ).toFixed(1);
  // console.log(progres, 'INI PROGRESS');
  return (
    <View>
      <View style={styles.container}>
        <Gap height={30} />
        <FlatList
          showsVerticalScrollIndicator={false}
          data={props.StudentCourses}
          renderItem={({item}) => {
            const progres = (item.totalSeenCourses / item.totalCourse).toFixed(
              1,
            );
            console.log(progres, 'INI PROGRESS');
            return item.status === 1 ||
              item.status === 2 ||
              item.status === 3 ? (
              <>
                {item.courseId !== null ? (
                  <View style={styles.courseContainer}>
                    <Image
                      source={{uri: item.courseId.image}}
                      style={styles.gambar}></Image>
                    <Gap height={6} />
                    <View style={styles.innerbox}>
                      <Text style={styles.titleCourse}>
                        {item.courseId.title}
                      </Text>
                      <Text style={styles.penulis}>
                        By {item.courseId.teacherId.fullname}
                      </Text>
                      <Gap height={21} />
                      <Progress.Bar progress={progres} width={320} />
                      <Gap height={10} />
                      <View style={styles.choose}>
                        <Text>
                          {item.totalSeenCourses}/{item.totalCourse}
                        </Text>
                        <Text
                          onPress={() => {
                            const courseAidi = item.courseId._id;
                            props.CourseMaterials(courseAidi);
                          }}>
                          See course materials
                        </Text>
                      </View>
                      <Gap height={30} />
                      <View style={styles.letakkotak}>
                        <Gap width={29} />
                        <ButtonLesson
                          title="Course Contents"
                          onPress={() => {
                            const courseID = item.courseId._id;
                            // props.navigation.navigate('CourseContent');
                            props.ActionSendStudentCourseId(courseID);
                          }}
                        />
                      </View>
                    </View>
                  </View>
                ) : null}
              </>
            ) : (
              <>
                {item.courseId !== null ? (
                  <View style={styles.courseContainer}>
                    <Image
                      source={{uri: item.courseId.image}}
                      style={styles.gambar}></Image>
                    <Gap height={6} />
                    <View style={styles.innerbox}>
                      <Text style={styles.titleCourse}>
                        {item.courseId.title}
                      </Text>
                      <Text style={styles.penulis}>
                        By {item.courseId.teacherId.fullname}
                      </Text>
                      <Gap height={21} />
                      <View style={styles.waitingTxt}>
                        <Text style={styles.waitingText}>Waiting Approval</Text>
                      </View>
                    </View>
                  </View>
                ) : null}
              </>
            );
          }}
        />
      </View>
    </View>
  );
};

const mapStateToProps = (state) => ({
  StudentCourses: state.StudentCoursesReducer.courses,
});

const mapDispatchToProps = {
  ActionSendStudentCourseId,
  CourseMaterials,
};

export default connect(mapStateToProps, mapDispatchToProps)(Course);

const styles = StyleSheet.create({
  courseContainer: {
    marginBottom: Size.wp4,
    borderBottomWidth: Size.ms1,
    borderColor: Color.silver,
    paddingBottom: Size.h5,
  },
  gambar: {
    width: Size.wp92,
    height: Size.h27,
    borderRadius: 2,
    // alignSelf: 'center',
  },
  titleCourse: {
    fontSize: Size.ms18,
    fontWeight: 'bold',
  },
  container: {
    backgroundColor: 'white',
    paddingLeft: Size.wp5,
    paddingRight: Size.wp5,
    paddingBottom: Size.h5,
  },
  choose: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  letakkotak: {
    flexDirection: 'row',
  },
  penulis: {
    fontSize: Size.ms15,
  },
  innerbox: {
    paddingLeft: Size.wp4,
    paddingRight: Size.wp3,
  },
  waitingTxt: {
    marginHorizontal: Size.wp13,
    padding: Size.wp3,
    backgroundColor: Color.orange,
    // justifyContent: 'center',
    alignItems: 'center',
  },
  waitingText: {
    fontWeight: 'bold',
    fontSize: Size.ms14,
    color: 'white',
  },
});
