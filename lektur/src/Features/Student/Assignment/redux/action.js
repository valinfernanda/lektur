export const ActionSendStudentCourseIdtoAssesment = (courseId) => {
  return {
    type: 'POST_COURSE_ID_ASSIGNMENT',
    courseId,
  };
};
export const triggerTheSaga = () => {
  return {
    type: 'TriggerThisSaga',
  };
};
