const InitialState = {
  courses: [],
};

export const StudentAssesmentReducer = (state = InitialState, action) => {
  switch (action.type) {
    case 'SET_STUDENT_ASSESMENT':
      return {
        ...state,
        courses: action.courses,
      };
    default:
      return state;
  }
};
