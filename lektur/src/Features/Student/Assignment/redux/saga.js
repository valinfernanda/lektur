import {all, takeLatest, put} from 'redux-saga/effects';
import axios from 'axios';
import {Store} from '../../../../Store/Store';
import {navigate} from '../../../Utils/Nav';

function* SagaToCourseAssesment(payload) {
  try {
    const token = Store.getState().LoginReducer.token;
    console.log(token, 'ini TOKEN To COURSE ASSIGNMENT');
    console.log(payload, 'ini payload To COURSE ASSIGNMENT');
    const Response = yield axios.get(
      `https://lekturapp.herokuapp.com/api/assessment/?courseId=${payload.courseId}`,
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      },
    );
    console.log(Response, 'INI SagaToCourseAssesment RESPONSE');
    const AllAssignmentData = Response.data.result;
    yield put({type: 'SET_COURSE_ASSIGNMENT', contents: AllAssignmentData}); //TO COURSEASSIGNMENT REDUCER
    yield put(navigate('AssignmentStudentScore', {}));
  } catch (error) {
    console.log(error);
  }
}

export function* SagasToCourseAssesment() {
  //INI BUAT NANGKAP ACTION
  yield takeLatest('POST_COURSE_ID_ASSIGNMENT', SagaToCourseAssesment);
}
