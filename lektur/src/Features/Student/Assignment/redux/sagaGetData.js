import {all, takeLatest, put} from 'redux-saga/effects';
import axios from 'axios';
import {Store} from '../../../../Store/Store';

function* SagaGetStudentAssesment() {
  try {
    const token = Store.getState().LoginReducer.token;
    console.log(token, 'ini TOKEN STUDENT courses');
    const Response = yield axios.get(
      'https://lekturapp.herokuapp.com/api/student/profile',
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      },
    );
    console.log(Response, 'INI student courses SAGA RESPONSE');
    const AllCoursesData = Response.data.result.course;
    yield put({type: 'SET_STUDENT_ASSESMENT', courses: AllCoursesData});
  } catch (error) {
    console.log(error);
  }
}

export function* SagasGetStudentAssesment() {
  //INI BUAT NANGKAP ACTION
  yield takeLatest('TriggerThisSaga', SagaGetStudentAssesment);
}
