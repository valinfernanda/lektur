const InitialState = {
  jawaban: [],
  listUjian: [],
};

export const StudentAssessmentReducer = (state = InitialState, action) => {
  switch (action.type) {
    case 'SET_STUDENT_ASSESMENT':
      return {
        ...state,
        jawaban: action.jawaban,
        listUjian: action.listUjian,
      };

    default:
      return state;
  }
};
