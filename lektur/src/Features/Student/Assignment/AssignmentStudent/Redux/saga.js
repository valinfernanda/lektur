import {takeLatest, put} from 'redux-saga/effects';
import axios from 'axios';

import {Store} from '../../../../../Store/Store';
// import {navigate} from '../../../Utils/Nav';
import {} from '../../../../Utils/Nav';

function* SagaStudentAssesment(payload) {
  try {
    const id = Store.getState().TeacherCourseReducer.listCourse._id;
    const Token = Store.getState().LoginReducer.token;
    console.log('INI ID NYA WOY', id);
    const Response = yield axios.get(
      //   `https://lekturapp.herokuapp.com/api/assessment/?courseId=602b7bd600d91056de8e0392`,
      `https://lekturapp.herokuapp.com/api/student/assessment/?courseId=${id}`,

      {
        //put the token into auth bearer
        headers: {
          Authorization: `Bearer ${Token}`,
        },
      },
    );
    console.log('INI RESPON STUDENT ASSESMENT', Response);

    // yield put({
    //   type: 'SET_STUDENT_ASSESMENT',
    //   options: Response.data.result.options,
    //   listAssesment: Response.data.result,
    //   jawaban: Response.data.result
    //   listUjian:

    //   number: Response.data.result.number,
    //   question: Response.data.result.question,
    //   id: Response.data.result._id,
    //   courseId: Response.data.result.courseId,
    //   answer: Response.data.result.answer,
    //   text: Response.data.result.options.text,
    // });
  } catch (error) {
    console.log(error);
  }
}

export function* SagasStudentAssesment() {
  yield takeLatest('GET_STUDENT_ASSESMENT', SagaStudentAssesment);

  //make sure 1st statement match with action, 2nd match with saga function name
}
