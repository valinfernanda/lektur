import React from 'react';
import {View, Text, StyleSheet, ScrollView} from 'react-native';
import {Gap, Header, Option} from '../../../../Components';
import {Size} from '../../../../Shared/Global/Config/Size';

const AssignmentStudent = ({data}) => {
  return (
    <>
      <View style={styles.bungkus}>
        <Gap height={19} />
        <View style={styles.atasjudul}>
          <Text style={styles.title1}>{data.length} Questions</Text>
          <Text style={styles.title2}>/ Final Assessment</Text>
        </View>
        <View style={styles.atas}>
          <Text style={styles.soal}>Final Assessment </Text>
        </View>
        <Text style={styles.jumlah}>{data.length} Questions</Text>
        <Gap height={20} />
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={styles.container}>
            {data.map((value, index) => {
              return (
                <View key={index.toString()}>
                  <View style={styles.pertanyaan}>
                    <Text>{value.number}.</Text>
                    <Gap width={7} />
                    <View>
                      <Text style={styles.tanya}>{value.question}</Text>
                    </View>
                  </View>
                  <View style={styles.jawaban}>
                    {value.options.map((v, i) => {
                      return <Option title={v.text} />;
                    })}
                  </View>
                </View>
              );
            })}
          </View>
        </ScrollView>
      </View>
    </>
  );
};

export default AssignmentStudent;

const styles = StyleSheet.create({
  soal: {
    fontSize: Size.ms22,
    fontWeight: 'bold',
  },
  atas: {
    flexDirection: 'row',
  },
  //   container: {
  //     backgroundColor: '#E5E5E5',
  //     borderRadius: 5,
  //   },
  jawaban: {
    paddingLeft: Size.wp1,
  },
  pertanyaan: {flexDirection: 'row'},
  bungkus: {
    paddingLeft: Size.wp7,
    paddingRight: Size.wp10,
  },
  tanya: {
    fontSize: Size.ms16,
    paddingRight: Size.wp4,
  },
  jumlah: {
    fontWeight: 'bold',
    paddingTop: Size.wp7,
  },
  atasjudul: {
    flexDirection: 'row',
    paddingBottom: Size.wp7,
  },
  title1: {
    color: 'grey',
    textDecorationLine: 'underline',
  },
  title2: {
    color: '#3e89ae',
    fontWeight: 'bold',
  },
});
