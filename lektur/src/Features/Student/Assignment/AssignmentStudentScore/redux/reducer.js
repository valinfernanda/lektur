const InitialState = {
  contents: [],
};

export const StudentAssignmentReducer = (state = InitialState, action) => {
  switch (action.type) {
    case 'SET_COURSE_ASSIGNMENT':
      return {
        ...state,
        contents: action.contents,
      };
    default:
      return state;
  }
};
//THIS REDUCER FROM STUDENT ASSIGNMENT SAGA
