export const ActionSendScore = (courseID, score) => {
  return {
    type: 'POST_SCORE',
    courseID,
    score,
  };
};
export const ActionTriggerSaga = () => {
  return {
    type: 'POST_TRIGGER',
  };
};
