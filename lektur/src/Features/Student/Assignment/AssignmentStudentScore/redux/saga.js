import {all, takeLatest, put} from 'redux-saga/effects';
import axios from 'axios';
import {Store} from '../../../../../Store/Store';
import {navigate} from '../../../../Utils/Nav';
import {ActionLogin} from '../../../../Auth/Login/Redux/action';

function* SagaGetStudentCourses() {
  try {
    const token = Store.getState().LoginReducer.token;
    console.log(token, 'ini TOKEN STUDENT courses');
    const Response = yield axios.get(
      'https://lekturapp.herokuapp.com/api/student/profile',
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      },
    );
    console.log(Response, 'INI student HASIL ASSESMENT RESPONSE');
    const AllCoursesData = Response.data.result.course;
    yield put({type: 'SET_STUDENT_ASSESMENT', courses: AllCoursesData});
  } catch (error) {
    console.log(error);
  }
}

function* SagaInputScore(payload) {
  try {
    const token = Store.getState().LoginReducer.token;
    console.log(token, 'ini TOKEN di Assignment Student Score');
    const email = Store.getState().LoginReducer.email;
    const password = Store.getState().LoginReducer.password;

    console.log(payload, 'ini payload di Assignment Student Score');
    const body = {
      score: payload.score,
    };
    const Response = yield axios.put(
      `https://lekturapp.herokuapp.com/api/assessment/result?courseId=${payload.courseID}`,
      body,
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      },
    );
    console.log(Response, 'INI SagaInputScore RESPONSE');
    // const AllCoursesData = Response.data.result;
    // yield put({type: 'SET_COURSE_PLAY', contentDetail: AllCoursesData}); //TO COURSE PLAY REDUCER
    yield put(ActionLogin(email, password));
    yield put(navigate('MainApp', {}));
  } catch (error) {
    console.log(error);
  }
}

export function* SagasInputScore() {
  //INI BUAT NANGKAP ACTION
  yield takeLatest('POST_TRIGGER', SagaGetStudentCourses);
  yield takeLatest('POST_SCORE', SagaInputScore);
}
