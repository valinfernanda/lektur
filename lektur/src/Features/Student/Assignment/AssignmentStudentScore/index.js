import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  StyleSheet,
  FlatList,
  TouchableHighlight,
  ScrollView,
} from 'react-native';
import {CheckBox} from 'react-native-elements';
import {Gap, Header, Option} from '../../../../Components';
import {Size} from '../../../../Shared/Global/Config/Size';
import {Color} from '../../../../Shared/Global/Config/Color';
//redux
import {connect} from 'react-redux';
import {ActionSendScore, ActionTriggerSaga} from './redux/action';

const AssignmentStudentScore = (props) => {
  const [check, setcheck] = useState(props.assesmentList);
  const [rerender, setrerender] = useState(false); // utk merender kmbali hasil prubahan
  const [answerList, setAnswerList] = useState([]); // array kumpulan jawaban student
  console.log(answerList, 'ini answerLIST'); //bakal kt pake answerList utk perbandingan dgn answer yg ada
  // console.log(check, 'ini check LOH');
  //submit button area config
  const [shouldShow, setShouldShow] = useState(false);
  const [buttonShow, setButtonHidden] = useState(true);
  const [showCorrect, setShowCorrect] = useState(false);
  const [showRemarks, setShowRemarks] = useState(false);
  const totalCorrect = answerList.length;
  const [pnjangTotalCorrect, setPanjangTotal] = useState([]);
  const [Score, setScore] = useState(0);
  const [totalAnswer, setTotalAnswer] = useState([]);
  const [SubmitScore, setSubmitScore] = useState(false);
  const [sumTrue, setSumTrue] = useState(0);
  const [statusCorrect, setStatusCorrect] = useState(0);
  const answerLength = totalAnswer.length;
  const [correctTotal, setTotalCorrect] = useState(0);

  const courseID = props.assesmentList[0].courseId._id;

  //place all kunci jawaban to an array
  const manipulateData = () => {
    for (let a = 0; a < props.assesmentList.length; a++) {
      setTotalAnswer((prevState) => {
        return [...prevState, props.assesmentList[a].answer];
      });
    }
  };

  console.log(totalAnswer, 'INI TOTAL ANSWER');
  console.log(answerList, 'INI JAWABAN STUDENT');
  console.log(sumTrue, 'INI JUMLAH JAWABAN BENAR');
  console.log(Score, 'INI HASIL AKHIR');

  // find the score result
  const cariScore = () => {
    let totalSumTrue = 0;
    let isItCorrect = [];
    for (let index = 0; index < totalAnswer.length; index++) {
      for (let i = 0; i < answerList.length; i++) {
        if (totalAnswer[index] === answerList[i]) {
          // setSumTrue(sumTrue + 1);
          totalSumTrue = totalSumTrue + 1;
          continue;
        }
      }
    }
    // const totalScore = (sumTrue / totalAnswer.length) * 100;
    const totalScore = (totalSumTrue / totalAnswer.length) * 100;
    setStatusCorrect(isItCorrect);
    setTotalCorrect(totalSumTrue);
    setScore(totalScore);
  };

  useEffect(() => {
    manipulateData();
    props.ActionSendScore();
    props.ActionTriggerSaga();
  }, []);

  return (
    <ScrollView showsVerticalScrollIndicator={false}>
      <View style={styles.bungkus}>
        <Gap height={19} />
        <View style={styles.atasjudul}>
          <Text style={styles.title1}>
            {props.assesmentList[0].courseId.title}
          </Text>
          <Text style={styles.title2}>/ Final Assessment</Text>
        </View>
        <View style={styles.atas}>
          <Text style={styles.soal}>Final Assessment </Text>
        </View>

        <View style={styles.containerScore}>
          {/* {shouldShow ? <Text>TESTING</Text> : null} */}
          {shouldShow ? (
            <>
              <Text style={styles.scoreStyle}>{Score}%</Text>
              <Text style={styles.courseComplete}>
                {correctTotal}/{props.assesmentList.length} Question Correct
              </Text>
            </>
          ) : null}
        </View>

        <Text style={styles.jumlah}>
          {props.assesmentList.length} Questions
        </Text>
        <Gap height={20} />
        {/* FLATLIST AREA */}
        <FlatList
          data={check}
          renderItem={({item, index}) => {
            return (
              <View style={styles.container}>
                <View style={styles.pertanyaan}>
                  <Text style={styles.numberStyle}>{item.number}. </Text>
                  <Gap width={7} />
                  <View>
                    <Text style={styles.tanya}>{item.question}</Text>
                    <View style={styles.bungkusAnswer}>
                      <Text style={styles.answerTxt}>Answer</Text>
                      <View>
                        {showCorrect ? (
                          answerList[index] === item.answer ? (
                            <Text style={styles.correctText}>Correct</Text>
                          ) : (
                            <Text style={styles.wrongText}>Wrong</Text>
                          )
                        ) : null}
                      </View>
                    </View>
                  </View>
                </View>
                <View style={styles.jawaban}>
                  {item.options.map((value, i) =>
                    value.value === item.answer ? (
                      <CheckBox
                        uncheckedIcon="circle-o"
                        checkedIcon={SubmitScore ? 'dot-circle-o' : 'circle-o'}
                        title={value.text}
                        checked={!SubmitScore ? value.checked : SubmitScore}
                        // checked={SubmitScore}
                        style={styles.checkStyle}
                        onPress={() => {
                          if (SubmitScore) {
                            return;
                          }
                          // console.log(item.options[i]);
                          console.log(index);
                          console.log(check[index]);
                          console.log(
                            check[index].options[i]._id,
                            'INI YANG KT CARI',
                          );
                          setcheck((prevState) => {
                            prevState[index].options.forEach((content) => {
                              content.checked = false;
                            });
                            return prevState;
                          });
                          setcheck((prevState) => {
                            prevState[index].options[i].checked = true;
                            return prevState;
                          });
                          setrerender(!rerender);
                          setAnswerList((prevState) => {
                            prevState[index] = check[index].options[i].value;
                            return prevState;
                          });
                        }}
                      />
                    ) : (
                      <CheckBox
                        uncheckedIcon="circle-o"
                        checkedIcon="circle-o"
                        title={value.text}
                        checked={value.checked}
                        style={styles.checkStyle}
                        // containerStyle={styles.checkStyle}
                        onPress={() => {
                          if (SubmitScore) {
                            return;
                          }
                          // console.log(item.options[i]);
                          console.log(index);
                          console.log(check[index]);
                          console.log(
                            check[index].options[i]._id,
                            'INI YANG KT CARI',
                          );
                          setcheck((prevState) => {
                            prevState[index].options.forEach((content) => {
                              content.checked = false;
                            });
                            return prevState;
                          });
                          setcheck((prevState) => {
                            prevState[index].options[i].checked = true;
                            return prevState;
                          });
                          setrerender(!rerender);
                          setAnswerList((prevState) => {
                            prevState[index] = check[index].options[i].value;
                            return prevState;
                          });
                        }}
                      />
                    ),
                  )}
                </View>

                <View style={styles.remarksArea}>
                  {showRemarks ? (
                    answerList[index] !== item.answer ? (
                      <>
                        <Text style={styles.remarksTxt}>Remark</Text>
                        <Text style={styles.isiRemarks}>{item.remarks}</Text>
                      </>
                    ) : null
                  ) : null}
                </View>
              </View>
            );
          }}
        />
        {/* END OF FLATLIST AREA */}

        {buttonShow ? (
          <TouchableHighlight
            style={styles.buttonSubmit}
            onPress={() => {
              setSubmitScore(true);
              cariScore();
              setShouldShow(true);
              setButtonHidden(false);
              setShowCorrect(true);
              setShowRemarks(true);
            }}>
            <Text style={styles.textStyleBold}>Submit Assesment</Text>
          </TouchableHighlight>
        ) : (
          <TouchableHighlight
            style={styles.buttonSubmitBack}
            onPress={() => {
              props.ActionSendScore(courseID, Score);
              props.ActionTriggerSaga();
            }}>
            <Text style={styles.textStyleBold}>Back To Main Assesment</Text>
          </TouchableHighlight>
        )}
      </View>
    </ScrollView>
  );
};

const mapStateToProps = (state) => ({
  assesmentList: state.StudentAssignmentReducer.contents,
});

const mapDispatchToProps = {
  ActionSendScore,
  ActionTriggerSaga,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(AssignmentStudentScore);

const styles = StyleSheet.create({
  soal: {
    fontSize: Size.ms22,
    fontWeight: 'bold',
  },
  atas: {
    flexDirection: 'row',
  },
  container: {
    paddingTop: Size.wp5,
    paddingBottom: Size.wp9,
    borderTopWidth: Size.wp1,
    borderColor: Color.lightSilver,
  },
  jawaban: {
    paddingLeft: Size.wp1,
  },
  pertanyaan: {flexDirection: 'row'},
  bungkus: {
    backgroundColor: 'white',
    paddingLeft: Size.wp7,
    paddingRight: Size.wp10,
  },
  tanya: {
    fontSize: Size.ms16,
    paddingRight: Size.wp4,
  },
  jumlah: {
    fontWeight: 'bold',
    paddingTop: Size.wp7,
    fontSize: Size.ms16,
  },
  atasjudul: {
    flexDirection: 'row',
    paddingBottom: Size.wp7,
  },
  title1: {
    color: 'grey',
    textDecorationLine: 'underline',
  },
  title2: {
    color: '#3e89ae',
    fontWeight: 'bold',
  },
  checkStyle: {
    backgroundColor: 'transparent',
    borderWidth: 0,
    marginLeft: 0,
    marginRight: 0,
    // margin: 0,
    padding: 0,
  },
  buttonSubmit: {
    marginVertical: Size.h5,
    backgroundColor: Color.bluelight,
    padding: Size.wp5,
    borderRadius: Size.ms4,
    color: Color.darkblue,
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonSubmitBack: {
    marginVertical: Size.h5,
    backgroundColor: Color.greenGum,
    padding: Size.wp5,
    borderRadius: Size.ms4,
    color: Color.darkblue,
    alignItems: 'center',
    justifyContent: 'center',
  },
  textStyleBold: {
    color: 'white',
    fontSize: Size.ms16,
    fontWeight: 'bold',
    marginLeft: Size.wp5,
  },
  numberStyle: {
    fontSize: Size.ms16,
  },
  bungkusAnswer: {
    marginVertical: Size.wp3,
    width: Size.wp77,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  answerTxt: {
    color: Color.darkGrey,
    fontWeight: 'bold',
    fontSize: Size.ms16,
  },
  correctText: {
    color: Color.greenGum,
    fontWeight: 'bold',
    fontSize: Size.ms16,
  },
  wrongText: {
    color: Color.redPink,
    fontWeight: 'bold',
    fontSize: Size.ms16,
  },
  scoreStyle: {
    color: Color.orange,
    fontSize: Size.ms40,
  },
  courseComplete: {
    color: Color.darkGrey,
  },
  containerScore: {
    marginTop: Size.wp3,
    justifyContent: 'center',
    alignItems: 'center',
  },
  remarksArea: {
    paddingTop: Size.wp6,
    paddingLeft: Size.wp6,
  },
  remarksTxt: {
    paddingBottom: Size.wp3,
    color: Color.darkGrey,
    fontWeight: 'bold',
    fontSize: Size.ms16,
  },
  isiRemarks: {
    fontSize: Size.ms16,
  },
});
