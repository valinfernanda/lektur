import React, {useEffect} from 'react';
import {
  StyleSheet,
  View,
  Text,
  ScrollView,
  TouchableOpacity,
  TouchableHighlight,
  Image,
  FlatList,
} from 'react-native';
import {SearchBar} from 'react-native-elements';
import {Size} from '../../../Shared/Global/Config/Size';
import {Color} from '../../../Shared/Global/Config/Color';
import {Link} from '../../../Components/atoms';
//redux
import {connect} from 'react-redux';
import {
  ActionSendStudentCourseIdtoAssesment,
  triggerTheSaga,
} from './redux/action';
import {ImageBackground} from 'react-native';
import {BGClassroom, Books} from '../../../Assets';

const Assignment = (props) => {
  useEffect(() => {
    props.ActionSendStudentCourseIdtoAssesment();
    props.triggerTheSaga();
  }, []);

  if (!props.teacher) {
    return (
      <View style={styles.container}>
        <FlatList
          showsVerticalScrollIndicator={false}
          data={props.StudentCourses}
          renderItem={({item}) => {
            return item.status === 1 && item.courseId !== null ? (
              <View style={styles.topContainerCover}>
                <View style={styles.topContainerBottom}>
                  <View style={styles.courseDesc}>
                    <View style={styles.courseDescTop}>
                      <Text style={styles.courseDescTop1}>
                        {item.courseId.title}
                      </Text>
                      <Text style={styles.courseDescTop2}>
                        {item.courseId.teacherId.fullname}
                      </Text>
                      <Text style={styles.courseDescTop3}>
                        Completed at: (No Result Yet)
                      </Text>
                    </View>
                    <View style={styles.coursebuttonContainer}>
                      <TouchableHighlight
                        style={styles.editButton}
                        // onPress={() => props.navigation.navigate('Ass')}
                        onPress={() => {
                          const courseId = item.courseId._id;
                          props.ActionSendStudentCourseIdtoAssesment(courseId);
                        }}>
                        <Text style={styles.quizTextStyle}>TAKE QUIZ</Text>
                      </TouchableHighlight>
                    </View>
                  </View>
                </View>
              </View>
            ) : item.status === 2 ? (
              <View style={styles.topContainerCover}>
                <View style={styles.topContainerBottom}>
                  <View style={styles.courseDesc}>
                    <View style={styles.courseDescTop}>
                      <Text style={styles.courseDescTop1}>
                        {item.courseId.title}
                      </Text>
                      <Text style={styles.courseDescTop2}>
                        {item.courseId.teacherId.fullname}
                      </Text>
                      <Text style={styles.courseDescTop3}>
                        Completed at: {item.completionDate}
                      </Text>
                    </View>
                    <View style={styles.resultContainer}>
                      <Text style={styles.resultText}>{item.score}%</Text>
                      <Text>{}</Text>
                    </View>
                  </View>
                </View>
              </View>
            ) : null;
          }}
        />
      </View>
    );
  } else {
    return (
      <ImageBackground source={BGClassroom} style={styles.page}>
        <View style={styles.assignemtteacher}>
          <Text style={styles.notes}>
            Dear Amazing Teacher, this page can only be accessed by student.
            Thank you and keep sharing!
          </Text>
        </View>
      </ImageBackground>
    );
  }
};

const mapStateToProps = (state) => ({
  StudentCourses: state.StudentAssesmentReducer.courses,
  teacher: state.LoginReducer.role,
});

const mapDispatchToProps = {
  ActionSendStudentCourseIdtoAssesment,
  triggerTheSaga,
};

export default connect(mapStateToProps, mapDispatchToProps)(Assignment);

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-evenly',
    flexDirection: 'column',
    padding: 20,
  },
  page: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'blue',
  },
  assignemtteacher: {
    fontSize: Size.ms50,
    justifyContent: 'center',
    flex: 1,
    paddingHorizontal: Size.wp25,
  },
  notes: {
    fontWeight: 'bold',
    justifyContent: 'center',
  },

  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },

  topContainerCover: {
    flex: 2,
    height: Size.h25,
    width: Size.wp92,
    marginBottom: 20,
  },

  topContainerBottom: {
    flex: 3,
  },

  courseDesc: {
    backgroundColor: 'white',
    flex: 3,
    elevation: 5,
  },
  courseDescTop: {
    flex: 1,
    justifyContent: 'space-between',
    padding: Size.wp3,
    // backgroundColor: 'blue',
  },

  courseDescTop1: {
    fontWeight: 'bold',
    fontSize: Size.ms20,
    color: Color.black,
  },
  courseDescTop2: {
    fontWeight: 'normal',
    fontSize: Size.ms14,
    color: Color.bluelight,
  },
  courseDescTop3: {
    fontSize: Size.ms12,
    color: Color.paleGray,
  },

  coursebuttonContainer: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-evenly',
  },

  editButton: {
    backgroundColor: Color.white,
    padding: Size.h1,
    width: Size.wp30,
    borderRadius: Size.ms3,
    borderColor: Color.orange,
    elevation: 2,
    borderWidth: 2,
  },

  quizTextStyle: {
    color: Color.orange,
    fontWeight: 'bold',
    textAlign: 'center',
  },

  resultText: {
    fontSize: Size.ms40,
    // fontWeight: 'bold',
    color: Color.orange,
  },

  resultContainer: {
    paddingLeft: Size.wp4,
    paddingRight: Size.wp3,
    flexDirection: 'row',
  },
});
