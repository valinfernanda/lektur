import {all, takeLatest, put} from 'redux-saga/effects';
import axios from 'axios';
import {Store} from '../../../../Store/Store';
import {navigate} from '../../../Utils/Nav';

function* SagaCourseMaterial(payload) {
  try {
    const token = Store.getState().LoginReducer.token;
    // console.log(token, 'ini TOKEN SEARCH RESULT SAGA');
    const Response = yield axios.get(
      `https://lekturapp.herokuapp.com/api/student/pop-up/course/materials?courseId=${payload.courseAidi}`,
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      },
    );
    console.log(Response, 'INI COURSE MATERIAL SAGA RESPONSE');
    const AllMaterials = Response.data.result;

    yield put({
      type: 'SET_MATERIAL_COURSE',
      materialCourse: AllMaterials,
    });
    yield put(navigate('CourseMaterial', {}));
  } catch (error) {
    console.log(error);
  }
}

export function* SagasCourseMaterial() {
  //INI BUAT NANGKAP ACTION
  yield takeLatest('GET_MATERIALS', SagaCourseMaterial);
}
