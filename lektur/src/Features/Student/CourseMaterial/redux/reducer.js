const InitialState = {
  materialCourse: [],
};

export const CourseMaterialReducer = (state = InitialState, action) => {
  switch (action.type) {
    case 'SET_MATERIAL_COURSE':
      return {
        ...state,
        materialCourse: action.materialCourse,
      };
    default:
      return state;
  }
};
