import React, {useState, useEffect} from 'react';
import {View, Text, StyleSheet, FlatList, Linking} from 'react-native';
import {CheckBox} from 'react-native-elements';
import {Color} from '../../../Shared/Global/Config/Color';
import {Size} from '../../../Shared/Global/Config/Size';
import {downloadFile} from '../../Utils/Download';
//redux
import {connect} from 'react-redux';
// import {CourseMaterials} from './redux/action';

const CourseMaterial = (props) => {
  // const [check, setcheck] = useState(false);
  // useEffect(() => {
  //   props.CourseMaterials();
  // }, []);
  const {courseMaterial} = props;

  const download = (url, fileName) => {
    downloadFile(url, 'dicoba download');
  };

  return (
    <View style={{marginTop: 70}}>
      <Text style={styles.judul}>Lesson #1: {props.judul}</Text>
      <FlatList
        showsVerticalScrollIndicator={false}
        data={courseMaterial}
        ListEmptyComponent={() => {
          return (
            <View>
              <Text>Sorry there's no result</Text>
            </View>
          );
        }}
        renderItem={({item, index}) => {
          // const courseID = item.courseId;
          return (
            <View>
              <View style={styles.top}>
                <View style={styles.barispdf}>
                  <CheckBox uncheckedIcon="circle-o" />

                  <Text
                    style={styles.pdf}
                    onPress={() => download(item.material)}>
                    {item.contentId.title} {index + 1}
                  </Text>
                </View>
              </View>
            </View>

            // <Text onPress={() => Linking.openURL(item.material)}>
            //   {item.contentId.title} {item.contentId.number}
            // </Text>
          );
        }}
      />
    </View>
  );
};
const mapStateToProps = (state) => ({
  courseMaterial: state.CourseMaterialReducer.materialCourse,
  judul: state.CourseMaterialReducer.materialCourse[0].contentId.title,
});

const mapDispatchToProps = {
  // CourseMaterials,
};

export default connect(mapStateToProps, mapDispatchToProps)(CourseMaterial);

export const styles = StyleSheet.create({
  checboxTxt: {
    color: Color.bluelight,

    textDecorationLine: 'underline',
  },
  pdf: {
    fontSize: Size.ms15,
    paddingTop: Size.h3,
    textDecorationLine: 'underline',
    color: '#3E89AE',
  },
  judul: {
    paddingLeft: Size.wp13,
    fontWeight: 'bold',
    fontSize: Size.ms20,
  },
  barispdf: {
    flexDirection: 'row',
    paddingLeft: Size.wp10,
  },

  top: {
    paddingTop: Size.h3,
  },
  checkboxStyle: {
    backgroundColor: 'transparent',
    borderWidth: 0,
    marginLeft: 0,
    marginRight: 0,
    // margin: 0,
    padding: 0,
  },
  soal: {
    fontSize: Size.ms18,
    fontWeight: 'bold',
  },
});
