import React, {useState, useCallback, useRef} from 'react';
import {
  ImageBackground,
  View,
  Text,
  ScrollView,
  TouchableHighlight,
  TouchableOpacity,
  FlatList,
  SafeAreaView,
} from 'react-native';
import {CheckBox} from 'react-native-elements';
import Video from 'react-native-video';
//STYLE
import {styles} from './style';
import {image10, PlaylistActive, PopUpImage} from '../../../Assets';
//icon
import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
//COMPONENT
import Read from './readTop';
import ReadBottom from './bottomRead';
import {Size} from '../../../Shared/Global/Config/Size';
import {downloadFile} from '../../Utils/Download';
//redux
import {connect} from 'react-redux';
import {ActionSendContentId} from '../CourseContent/redux/action';
import {ActionSendId, ActionSendIdToDetail} from '../../Home/Redux/action';

const CoursePlay = (props) => {
  const {contentDetails} = props;
  const listContents = contentDetails.listContent;
  const index = contentDetails.content.contentId.number; // krn array dimulai 0 dan number dimulai dari 1 jd utk dpatkan next content mka ckup kt ambil numbernya sj

  const [check, setcheck] = useState(false);
  const [iconStyle, setIconStyle] = useState(styles.playIcon);
  const [viewBG, setviewBG] = useState(styles.contentCover);
  const [shouldShow, setShouldShow] = useState(true);
  const onPress = () => {
    setIconStyle(styles.playIconBlue);
    setviewBG(styles.contentCoverBlue);
    setShouldShow(!shouldShow);
  };

  // bottom read area
  const [textShown, setTextShown] = useState(false); //To show ur remaining Text
  const [lengthMore, setLengthMore] = useState(false); //to show the "Read more & Less Line"
  const toggleNumberOfLines = () => {
    //To toggle the show text or hide it
    setTextShown(!textShown);
  };

  const onTextLayout = useCallback((e) => {
    setLengthMore(e.nativeEvent.lines.length >= 4); //to check the text is more than 4 lines or not
    // console.log(e.nativeEvent);
  }, []);

  //related course area setup
  const [selectedCategoryID, setSelectedCategoryID] = useState('');
  const filterCourseByCategoryID = (categoryID) => {
    setSelectedCategoryID(categoryID);
  };

  //download pdf set up
  const download = (url, fileName) => {
    downloadFile(url, 'dicoba download');
  };

  return (
    <View style={{flex: 1, justifyContent: 'space-between'}}>
      <ScrollView
        showsVerticalScrollIndicator={false}
        style={styles.scrollView}>
        <View style={styles.container}>
          {/* TOP CONTAINER AREA */}
          <View style={styles.topContainerTop}>
            <View style={styles.lessonPart}>
              <Text style={styles.lessonPart1}>
                {contentDetails.content.courseId.title} /{' '}
              </Text>
              <Text style={styles.lessonPart2}>
                Lesson #{contentDetails.content.contentId.number} :{' '}
                {contentDetails.content.contentId.title}
              </Text>
            </View>
            <Text style={styles.lessonTitle}>
              Lesson #{contentDetails.content.contentId.number} :{' '}
              {contentDetails.content.contentId.title}
            </Text>
          </View>
          {/* VIDEO SET AREA */}
          <View style={styles.topContainer}>
            <Video
              source={{
                uri: contentDetails.content.contentId.video,
              }}
              style={styles.backgroundVideo}
              resizeMode={'cover'}
              controls={true}
              paused
              poster={contentDetails.content.contentId.thumbnail}
              // ref={(ref) => {
              //   this.player = ref;
              // }}
            />
          </View>
          {/* VIDEO SET AREA */}
          {/* MIDDLE AREA */}
          <View style={styles.topContainerCover}>
            <View style={styles.courseDetil}>
              <Read />
            </View>
          </View>
          {/* BOTTOM CONTAINER AREA */}
          <View style={styles.bottomContainer}>
            <View style={styles.bottomUp}>
              <Text style={styles.bottomUpText}>What's Next?</Text>
              {/* <CheckBox
                title="React and Open Source.pdf"
                checked={check}
                textStyle={styles.checboxTxt}
                uncheckedIcon="circle-o"
                containerStyle={styles.checkboxStyle}
                onPress={() => {
                  setcheck(!check);
                }}
              /> */}
              <FlatList
                showsVerticalScrollIndicator={false}
                data={props.contentDetails.material}
                renderItem={({item, index}) => {
                  return (
                    <View style={styles.pdfContainer}>
                      {/* <Text style={styles.pdfNum}>{index + 1}</Text>
                      <Text style={styles.pdfName}>{item.material}</Text> */}
                      <Text
                        style={styles.pdfStyle}
                        onPress={() => download(item.material)}>
                        {props.contentDetails.content.contentId.title}.pdf
                      </Text>
                    </View>
                  );
                }}
              />
              {index < listContents.length ? (
                <TouchableOpacity
                  style={styles.bottonButtonBlue}
                  onPress={() => {
                    const ContentID = listContents[index].contentId._id;
                    props.ActionSendContentId(ContentID);
                  }}>
                  <View style={styles.bottomUpButtonCover2}>
                    <AntDesign
                      name="play"
                      size={Size.ms23}
                      style={styles.nextLessonIcon}
                    />
                    <Text style={styles.textStyleBold}>
                      Next Lesson: {listContents[index].contentId.title}
                    </Text>
                  </View>
                </TouchableOpacity>
              ) : (
                <TouchableOpacity style={styles.bottonButtonGreen}>
                  <View style={styles.bottomUpButtonCover2}>
                    <Text style={styles.textStyleBold}>
                      All lesson has completed
                    </Text>
                  </View>
                </TouchableOpacity>
              )}

              {/* content area */}
              <Text style={styles.bottomUpText}>Content</Text>
              {/* content list area */}
              <View style={styles.bottomUpList}>
                <FlatList
                  showsVerticalScrollIndicator={false}
                  data={props.contentDetails.listContent}
                  renderItem={({item}) => {
                    const contentId = item.contentId._id;
                    return item.contentId.number !== 1 &&
                      item.contentStatus === 0 ? (
                      <View style={styles.contentCoverLock}>
                        <MaterialIcons
                          name="lock"
                          size={Size.ms25}
                          style={styles.playIconLock}
                        />
                        <Text style={styles.contentTxtLock}>
                          Lesson #{item.contentId.number}:{' '}
                          {item.contentId.title}
                        </Text>
                      </View>
                    ) : (
                      <TouchableHighlight
                        onPress={() => {
                          props.ActionSendContentId(contentId);
                        }}>
                        <View style={styles.contentCover}>
                          <AntDesign
                            name="play"
                            size={Size.ms23}
                            style={styles.playIcon}
                          />
                          <Text style={styles.contentTxt}>
                            Lesson #{item.contentId.number}:{' '}
                            {item.contentId.title}
                          </Text>
                        </View>
                      </TouchableHighlight>
                    );
                  }}
                />
              </View>
              {/* end of content list */}
            </View>
            {/* bottom lesson area */}
            <View style={styles.bottomLessons}>
              <FlatList
                horizontal
                data={props.contentDetails.relatedCourse.filter((item) => {
                  if (selectedCategoryID === '') {
                    return !!item.categoryId;
                  }
                  return (
                    !!item.categoryId &&
                    item.categoryId._id === selectedCategoryID
                  );
                })}
                ListEmptyComponent={() => {
                  return (
                    <View style={styles.dataNotFound}>
                      <Text style={styles.dataNotFoundTxt}>
                        THERE'RE NO RELATED COURSES
                      </Text>
                    </View>
                  );
                }}
                renderItem={({item}) => {
                  let courseId = item._id;
                  return (
                    <TouchableHighlight
                      style={styles.bLessonCover}
                      onPress={() => {
                        props.ActionSendIdToDetail(courseId);
                      }}>
                      <>
                        <ImageBackground
                          source={{uri: item.image}}
                          style={styles.apalah}></ImageBackground>
                        <View style={styles.bottomLsnDesc1}>
                          <Text style={styles.btmLsnDscTitle}>
                            {item.title}
                          </Text>
                          <Text style={styles.btmLsnDscTeacher}>
                            By {item.teacherId.fullname}
                          </Text>
                          <View style={styles.btmLsnDscPlus}>
                            <Text style={styles.btmLsnDscPlusTxt}>
                              {item.totalVideo} Videos
                            </Text>
                            <Text style={styles.btmLsnDscPlusTxt}>
                              {item.totalMaterial} Learning Material
                            </Text>
                          </View>
                        </View>
                        <View style={styles.bottomLsnDesc2}>
                          <Text
                            onTextLayout={onTextLayout}
                            numberOfLines={textShown ? undefined : 5}
                            style={styles.courseDetilTxt}>
                            {item.overview}
                          </Text>
                        </View>
                        <View style={styles.bottomLsnDesc3}>
                          {item.categoryId && (
                            <Text style={styles.bottomLsnDesc3Txt}>
                              {item.categoryId.categories}
                            </Text>
                          )}
                        </View>
                      </>
                    </TouchableHighlight>
                  );
                }}
              />
            </View>
            {/* end of bottom lesson area */}
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

const mapStateToProps = (state) => ({
  contentDetails: state.CoursePlayReducer.contentDetail,
  // courseContents: state.StudentContentReducer.contents,
});

const mapDispatchToProps = {
  ActionSendContentId,
  ActionSendId,
  ActionSendIdToDetail,
};

export default connect(mapStateToProps, mapDispatchToProps)(CoursePlay);
