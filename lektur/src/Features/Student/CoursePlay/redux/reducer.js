const InitialState = {
  contentDetail: {},
};

export const CoursePlayReducer = (state = InitialState, action) => {
  switch (action.type) {
    case 'SET_COURSE_PLAY':
      return {
        ...state,
        contentDetail: action.contentDetail,
      };
    default:
      return state;
  }
};
