import {StyleSheet} from 'react-native';
//STYLES AREA
import {Size} from '../../../Shared/Global/Config/Size';
import {Color} from '../../../Shared/Global/Config/Color';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    // paddingTop: StatusBar.currentHeight,
    justifyContent: 'space-between',
  },
  topContainer: {
    flex: 1,
    height: Size.h27,
    paddingHorizontal: Size.wp5,
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: 'silver',
  },
  topContainerCover: {
    padding: Size.wp5,
    resizeMode: 'contain',
  },
  topContainerTop: {
    height: Size.h14,
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingHorizontal: Size.wp5,
  },
  courseDetil: {
    backgroundColor: 'white',
    flex: 1,
    padding: Size.wp7,
    elevation: 5,
  },
  courseDetilUp1: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  videoDescBottom: {
    flex: 1,
    // alignItems: 'flex-end',
    justifyContent: 'center',
    paddingRight: Size.wp5,
  },

  //BOTTOM CONTAINER AREA
  bottomContainer: {
    resizeMode: 'contain',
    marginTop: Size.h1,
    backgroundColor: 'white',
    padding: Size.wp5,
    paddingTop: Size.wp10,
  },
  bottomUp: {
    flex: 2,
    justifyContent: 'center',
  },
  //   bottomDown: {
  //     flex: 4,
  //     // backgroundColor: 'brown',
  //   },
  bottomLessons: {
    // resizeMode: 'contain',
  },
  bLessonCover: {
    width: Size.wp72,
    borderWidth: 0.01,
    marginRight: Size.wp6,
    // color: 'red',
    elevation: 5,
  },
  bottomLessonsPict: {
    flex: 2,
    width: '80%',
    backgroundColor: 'black',
  },
  bottomLessonsDesc: {
    flex: 3,
    width: '80%',
  },
  bottomLsnDesc1: {
    flex: 2,
    padding: Size.wp3,
    backgroundColor: 'white',
  },
  bottomLsnDesc2: {
    paddingHorizontal: Size.wp3,
    paddingBottom: Size.wp3,
    backgroundColor: 'white',
    // flex: 3,
    height: Size.h17,
  },
  bottomLsnDesc3: {
    height: Size.h8,
    padding: Size.wp3,
    backgroundColor: Color.pink,
    justifyContent: 'center',
  },
  //   btmLsnDscCver: {
  //     flex: 2,
  //   },
  btmLsnDscPlus: {
    // flex: 1,
    flexDirection: 'row',
    alignContent: 'flex-end',
    paddingTop: Size.wp4,
  },

  //BOTTOM FOOTER
  bottomFooter: {
    elevation: 5,
    marginHorizontal: Size.hmin3,
  },
  bottomFooterCover: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignContent: 'center',
    paddingHorizontal: Size.wp13,
    paddingVertical: Size.wp7,
    paddingBottom: Size.wp3,
  },

  //COMPONENT

  openButton: {
    alignSelf: 'flex-start',
    backgroundColor: Color.orange,
    padding: Size.h1,
    borderRadius: Size.ms3,
    opacity: 0.5,
    elevation: 2,
  },
  openButtonFooter: {
    alignSelf: 'flex-start',
    backgroundColor: Color.orange,
    padding: Size.h1,
    borderRadius: Size.ms3,
    elevation: 2,
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: Size.ms15,
    textAlign: 'center',
  },
  textStyle2: {
    color: 'black',
    fontSize: Size.ms16,
  },
  textStyleBold: {
    color: 'white',
    fontSize: Size.ms16,
    fontWeight: 'bold',
    marginLeft: Size.wp5,
  },
  courseDetilNum: {
    color: Color.orange,
    fontSize: Size.ms28,
    fontWeight: 'bold',
  },
  courseDetilTxt: {
    fontSize: Size.ms15,
    lineHeight: 25,
  },
  lessonTitle: {
    fontWeight: 'bold',
    fontSize: Size.ms14,
    color: 'black',
  },
  lessonPart: {
    flexDirection: 'row',
  },
  lessonPart1: {
    fontSize: Size.ms11,
    color: 'silver',
  },
  lessonPart2: {
    fontSize: Size.ms11,
    color: Color.bluelight,
  },
  bottomUpList: {
    marginBottom: Size.wp10,
  },
  bottomUpText: {
    fontWeight: 'bold',
    fontSize: Size.ms20,
    marginTop: Size.wp10,
    marginBottom: Size.wp3,
  },
  checkboxStyle: {
    backgroundColor: 'transparent',
    borderWidth: 0,
    marginLeft: 0,
    marginRight: 0,
    // margin: 0,
    padding: 0,
  },
  checboxTxt: {
    color: Color.bluelight,
    textDecorationLine: 'underline',
  },
  contentCoverLock: {
    flexDirection: 'row',
    padding: Size.wp5,
    borderBottomWidth: 0.5,
    borderColor: Color.silver,
    alignItems: 'center',
    backgroundColor: Color.platinum,
  },
  playIconLock: {
    marginRight: Size.wp3,
    color: Color.darkGrey,
  },
  bottonUpButton: {
    backgroundColor: 'white',
    padding: Size.wp5,
    borderBottomWidth: Size.ms1,
    borderColor: 'silver',
    color: Color.darkblue,
  },
  bottonButtonBlue: {
    marginTop: Size.h5,
    backgroundColor: Color.bluelight,
    padding: Size.wp5,
    borderRadius: Size.ms4,
    color: Color.darkblue,
  },
  bottonButtonGreen: {
    marginTop: Size.h5,
    backgroundColor: Color.greenGum,
    padding: Size.wp5,
    borderRadius: Size.ms4,
    color: Color.darkblue,
  },
  bottomUpButtonCover: {
    alignItems: 'center',
    flexDirection: 'row',
  },
  bottomUpButtonCover2: {
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  bottomDescTxt: {
    fontSize: Size.ms15,
  },
  btmLsnDscTitle: {
    fontWeight: 'bold',
    fontSize: Size.ms20,
  },
  btmLsnDscTeacher: {
    fontSize: Size.ms16,
    color: 'silver',
  },
  btmLsnDscPlusTxt: {
    color: 'silver',
    paddingRight: Size.wp9,
  },
  bottomLsnDesc3Txt: {
    fontSize: Size.ms18,
  },
  courseDetilDownTxt: {
    fontSize: Size.ms15,
  },
  footerTitle: {
    fontWeight: 'bold',
    fontSize: Size.ms16,
    lineHeight: 21,
  },
  footerTeacher: {
    fontSize: Size.ms16,
    color: 'silver',
    lineHeight: 21,
  },
  playIcon: {
    marginRight: Size.wp3,
  },
  nextLessonIcon: {
    color: 'white',
  },
  contentCover: {
    flexDirection: 'row',
    padding: Size.wp5,
    borderBottomWidth: 0.5,
    borderColor: Color.silver,
    alignItems: 'center',
  },
  playIconBlue: {
    marginRight: Size.wp3,
    color: Color.bluelight,
  },
  contentCoverBlue: {
    flexDirection: 'row',
    padding: Size.wp5,
    borderBottomWidth: Size.ms1,
    borderColor: 'silver',
    alignItems: 'center',
    backgroundColor: Color.blueSoft,
  },
  contentTxt: {
    color: 'black',
  },
  contentTxtLock: {
    color: Color.darkGrey,
  },
  apalah: {
    width: '100%',
    height: Size.h22,
  },
  backgroundVideo: {
    // aspectRatio: 0.1,
    position: 'absolute',
    height: '100%',
    width: '100%',
  },
  dataNotFoundTxt: {
    fontSize: Size.ms20,
    fontWeight: 'bold',
    alignSelf: 'center',
    color: Color.red,
    paddingLeft: Size.wp3,
    paddingTop: Size.h2,
  },
  pdfStyle: {
    color: Color.bluelight,
    textDecorationLine: 'underline',
  },
});
