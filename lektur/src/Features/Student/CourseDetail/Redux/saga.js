import {all, takeLatest, put} from 'redux-saga/effects';
import axios from 'axios';
import {Store} from '../../../../Store/Store';

function* SagaGetCourses() {
  try {
    const token = Store.getState().LoginReducer.token;
    console.log(token, 'ini TOKEN HOME SAGA');
    const Response = yield axios.get(
      'https://lekturapp.herokuapp.com/api/courses/all',
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      },
    );
    console.log(Response, 'INI COURSE DETAIL SAGA RESPONSE');
    const AllCoursesData = Response.data.result.result;
    yield put({type: 'SET_DETAIL_COURSES', courses: AllCoursesData});
  } catch (error) {
    console.log(error);
  }
}

export function* SagaCourseDetail() {
  //INI BUAT NANGKAP ACTION
  yield takeLatest('SET_COURSE_DETAIL', SagaGetCourses);
}