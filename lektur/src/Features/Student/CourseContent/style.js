import {StyleSheet} from 'react-native';
import {Color} from '../../../Shared/Global/Config/Color';
import {Size} from '../../../Shared/Global/Config/Size';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: Size.wp5,
    backgroundColor: 'white',
  },
  contentCover: {
    flexDirection: 'row',
    padding: Size.wp5,
    borderBottomWidth: Size.ms1,
    borderColor: 'silver',
    alignItems: 'center',
  },
  contentCoverBlue: {
    flexDirection: 'row',
    padding: Size.wp5,
    borderBottomWidth: Size.ms1,
    borderColor: 'silver',
    alignItems: 'center',
    backgroundColor: Color.blueSoft,
  },
  contentCoverLock: {
    flexDirection: 'row',
    padding: Size.wp5,
    borderBottomWidth: Size.ms1,
    borderColor: 'silver',
    alignItems: 'center',
    backgroundColor: Color.platinum,
  },
  playIcon: {
    marginRight: Size.wp3,
  },
  playIconBlue: {
    marginRight: Size.wp3,
    color: Color.bluelight,
  },
  playIconLock: {
    marginRight: Size.wp3,
    color: Color.darkGrey,
  },
  contentTxtLocked: {
    fontSize: Size.ms17,
  },
  contentTxt: {
    fontSize: Size.ms17,
    color: Color.darkGrey,
  },
});
