const InitialState = {
  contents: [],
};

export const StudentContentReducer = (state = InitialState, action) => {
  switch (action.type) {
    case 'SET_COURSE_CONTENT':
      return {
        ...state,
        contents: action.contents,
      };
    default:
      return state;
  }
};
