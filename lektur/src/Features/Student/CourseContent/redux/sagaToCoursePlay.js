import {all, takeLatest, put} from 'redux-saga/effects';
import axios from 'axios';
import {Store} from '../../../../Store/Store';
import {navigate} from '../../../Utils/Nav';

function* SagaToCoursePlay(payload) {
  try {
    const token = Store.getState().LoginReducer.token;
    console.log(token, 'ini TOKEN To CONTENTS');
    console.log(payload, 'ini payload To CONTENTS');
    const Response = yield axios.get(
      `https://lekturapp.herokuapp.com/api/student/course/content?contentId=${payload.contentId}`,
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      },
    );
    console.log(Response, 'INI sagaToCoursePlay/ContentDetail RESPONSE');
    const AllCoursesData = Response.data.result;
    yield put({type: 'SET_COURSE_PLAY', contentDetail: AllCoursesData}); //TO COURSE PLAY REDUCER
    yield put(navigate('CoursePlay', {}));
  } catch (error) {
    console.log(error);
  }
}

export function* SagasToCoursePlay() {
  //INI BUAT NANGKAP ACTION
  yield takeLatest('POST_CONTENT_ID', SagaToCoursePlay);
}
