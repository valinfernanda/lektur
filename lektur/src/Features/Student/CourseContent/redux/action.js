export const ActionSendContentId = (contentId) => {
  return {
    type: 'POST_CONTENT_ID',
    contentId,
  };
};
