import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  ScrollView,
  TouchableHighlight,
  FlatList,
} from 'react-native';
import {Size} from '../../../Shared/Global/Config/Size';
import {styles} from './style';
//icons
import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
//redux
import {connect} from 'react-redux';
import {ActionSendContentId} from './redux/action';

const CourseContent = (props) => {
  console.log(props);
  useEffect(() => {
    props.ActionSendContentId();
  }, []);

  const {courseContents} = props;
  console.log(courseContents, 'ini course CONTENT');

  const [iconStyle, setIconStyle] = useState(styles.playIcon);
  const [viewBG, setviewBG] = useState(styles.contentCover);
  const [shouldShow, setShouldShow] = useState(true);
  const onPress = () => {
    setIconStyle(styles.playIconBlue);
    setviewBG(styles.contentCoverBlue);
    setShouldShow(!shouldShow);
  };

  return (
    <View style={styles.container}>
      {/* <TouchableHighlight onPress={onPress}>
          <View style={viewBG}>
            <AntDesign name="play" size={Size.ms23} style={iconStyle} />
            <Text style={styles.contentTxt}>Lesson #1: What is React?</Text>
          </View>
        </TouchableHighlight> */}
      {/* <TouchableHighlight
        onPress={() => props.navigation.navigate('CourseMaterial')}>
        <View style={viewBG}>
          <AntDesign name="play" size={Size.ms23} style={iconStyle} />
          <Text style={styles.contentTxt}>Lesson #1: What is React?</Text>
        </View>
      </TouchableHighlight>
      {shouldShow ? (
        <View style={styles.contentCoverLock}>
          <MaterialIcons
            name="lock"
            size={Size.ms25}
            style={styles.playIconLock}
          />
          <Text style={styles.contentTxt}>Lesson #1: What is React?</Text>
        </View>
      ) : (
        <View style={styles.contentCover}>
          <AntDesign name="play" size={Size.ms23} style={styles.playIcon} />
          <Text style={styles.contentTxt}>Lesson Unlock </Text>
        </View>
      )}
      <View style={styles.contentCoverLock}>
        <MaterialIcons
          name="lock"
          size={Size.ms25}
          style={styles.playIconLock}
        />
        <Text style={styles.contentTxt}>Lesson #1: What is React?</Text>
      </View>
      <View style={styles.contentCoverLock}>
        <MaterialIcons
          name="lock"
          size={Size.ms25}
          style={styles.playIconLock}
        />
        <Text style={styles.contentTxt}>Lesson #1: What is React?</Text>
      </View> */}

      <FlatList
        showsVerticalScrollIndicator={false}
        data={props.courseContents}
        renderItem={({item}) => {
          const contentId = item.contentId;
          return item.number !== 1 && item.contentStatus === 0 ? (
            <View style={styles.contentCoverLock}>
              <MaterialIcons
                name="lock"
                size={Size.ms25}
                style={styles.playIconLock}
              />
              <Text style={styles.contentTxt}>
                Lesson #{item.number}: {item.title}
              </Text>
            </View>
          ) : (
            <TouchableHighlight
              onPress={() => {
                props.ActionSendContentId(contentId);
              }}>
              <View style={viewBG}>
                <AntDesign name="play" size={Size.ms23} style={iconStyle} />
                <Text style={styles.contentTxtLocked}>
                  Lesson #{item.number}: {item.title}
                </Text>
              </View>
            </TouchableHighlight>
          );
        }}
      />
    </View>
  );
};

const mapStateToProps = (state) => ({
  courseContents: state.StudentContentReducer.contents,
});

const mapDispatchToProps = {
  ActionSendContentId,
};

export default connect(mapStateToProps, mapDispatchToProps)(CourseContent);
