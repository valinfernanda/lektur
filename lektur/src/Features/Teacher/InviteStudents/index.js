import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  View,
  Text,
  ScrollView,
  TouchableOpacity,
  TouchableHighlight,
  TextInput,
  Dimensions,
  Button,
  Alert,
  FlatList,
} from 'react-native';
import {SearchBar} from 'react-native-elements';
import {Size} from '../../../Shared/Global/Config/Size';
import {Color} from '../../../Shared/Global/Config/Color';
import Gap from '../../../Components/atoms/Gap/index';
import * as Progress from 'react-native-progress';
import {Link} from '../../../Components/atoms';
//component
import TeacherHeader from '../teacherHeader';
import {Header} from '../../../Components';
//redux
import {connect} from 'react-redux';
import {GetIdForInvtStudent} from './redux/action';
//icon
import FontAwesome from 'react-native-vector-icons/FontAwesome';

state = {
  progress: 20,
  progressWithOnComplete: 0,
};

increase = (key, value) => {
  this.setState({
    [key]: this.state[key] + value,
  });
};

const InviteStudents = (props) => {
  useEffect(() => {
    props.GetIdForInvtStudent();
  }, []);
  // const {dataEnrolledStudent} = props;

  const [buttonShow, setButtonHidden] = useState(true);

  return (
    <>
      <Header title="New Course" onPress={() => props.navigation.goBack()} />
      <TeacherHeader navigate={props.navigation.navigate} />
      <View style={styles.container}>
        <View style={styles.topContainer}>
          <View style={styles.inviteStudentsContainer}>
            <TouchableHighlight
              style={styles.inviteButton}
              onPress={() => props.navigation.navigate('InputEmailStudent')}>
              <Text style={styles.inviteTextStyle}>INVITE</Text>
            </TouchableHighlight>
          </View>
          <Gap height={20} />
          <View style={styles.searchBarContainer}>
            <SearchBar
              inputStyle={{backgroundColor: 'white'}}
              containerStyle={{
                backgroundColor: 'white',
                borderWidth: 1,
                borderRadius: 5,
                width: Size.wp92,
              }}
            />
          </View>
        </View>

        <View style={styles.bottomContainer}>
          <FlatList
            showsVerticalScrollIndicator={false}
            data={props.dataEnrolledStudent}
            renderItem={({item}) => {
              let StudentId = item.studentId._id;
              return item.status === 0 ? (
                <View style={styles.flatContainer}>
                  <View style={styles.topPending}>
                    <Text style={styles.studentName}>
                      {item.studentId.fullname}
                    </Text>
                    <View style={styles.iconContainer}>
                      <FontAwesome
                        name="circle"
                        size={Size.ms20}
                        style={styles.iconPending}
                      />
                      <Text style={styles.iconTxt}>Pending</Text>
                    </View>
                  </View>
                  <View style={styles.bottomPending}>
                    <Text
                      style={styles.buttonAcc}
                      onPress={() => props.GetIdForInvtStudent(StudentId)}>
                      Accept
                    </Text>
                  </View>
                </View>
              ) : item.status === 1 ? (
                <View style={styles.flatContainer}>
                  <View style={styles.topPending}>
                    <Text style={styles.studentName}>
                      {item.studentId.fullname}
                    </Text>
                    <View style={styles.iconContainer}>
                      <FontAwesome
                        name="check-circle"
                        size={Size.ms20}
                        style={styles.iconActive}
                      />
                      <Text style={styles.iconTxtActive}>Active</Text>
                    </View>
                  </View>

                  <Gap height={10} />
                  <Progress.Bar
                    progress={0.6}
                    width={300}
                    style={styles.progressBar}
                  />

                  <Gap height={10} />
                  <View style={styles.choose}>
                    <Text>
                      {}/{}
                    </Text>
                  </View>
                </View>
              ) : item.status === 2 ? (
                <View style={styles.flatContainer}>
                  <View style={styles.topPending}>
                    <Text style={styles.studentName}>
                      {item.studentId.fullname}
                    </Text>
                    <View style={styles.iconContainer}>
                      <FontAwesome
                        name="check-circle"
                        size={Size.ms20}
                        style={styles.iconComplete}
                      />
                      <Text style={styles.iconTxtComplete}>Completed</Text>
                    </View>
                  </View>
                  <Text style={styles.percentage}>{}%</Text>

                  <View style={styles.choose}>
                    <Link title="Assesment Score" />
                  </View>
                </View>
              ) : null;
            }}
          />
        </View>
      </View>
    </>
  );
};

const mapStateToProps = (state) => ({
  dataEnrolledStudent: state.SetStudentDataById.dataEnrolledStudent,
});

const mapDispatchToProps = {
  GetIdForInvtStudent,
};

export default connect(mapStateToProps, mapDispatchToProps)(InviteStudents);

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-evenly',
    flexDirection: 'column',
    backgroundColor: 'white',
  },
  topContainer: {
    flex: 1,
    alignItems: 'center',
    width: '100%',
    paddingTop: Size.wp9,
    marginBottom: Size.h3,
    backgroundColor: Color.lightSilver,
  },
  bottomContainer: {
    flex: 2,
    marginTop: Size.hmin10,
    padding: Size.wp4,
    paddingTop: Size.wp9,
    width: '100%',
    backgroundColor: 'white',
  },
  progressBar: {
    color: Color.orange,
  },
  flatContainer: {
    paddingBottom: Size.h2,
    borderBottomWidth: Size.ms1,
    borderColor: 'silver',
  },
  inviteButton: {
    backgroundColor: Color.oxfordBlue,
    padding: Size.h1,
    width: Size.wp92,
    borderRadius: Size.ms3,
    elevation: 2,
  },

  inviteTextStyle: {
    textAlign: 'center',
    color: Color.white,
  },

  searchBarContainer: {
    flex: 1,
    alignItems: 'center',
  },

  // containerStudent: {
  //   width: Size.wp92,
  //   height: Size.h14,
  //   borderRadius: 2,
  //   backgroundColor: Color.white,

  //   paddingLeft: Size.wp4,
  //   paddingRight: Size.wp3,
  // },
  percentage: {
    fontSize: Size.ms20,
    fontWeight: 'bold',
    color: Color.orange,
  },
  topPending: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  studentName: {
    fontWeight: 'bold',
    fontSize: Size.ms20,
  },
  iconContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  iconPending: {
    color: Color.darkGrey,
    marginRight: Size.wp2,
  },
  iconAccepted: {
    color: Color.orange,
    marginRight: Size.wp2,
  },
  iconActive: {
    color: Color.greenGum,
    marginRight: Size.wp2,
  },
  iconComplete: {
    color: Color.bluelight,
    marginRight: Size.wp2,
  },
  iconTxt: {
    color: Color.darkGrey,
    fontWeight: 'bold',
    fontSize: Size.ms16,
  },
  iconTxtAcc: {
    color: Color.orange,
    fontWeight: 'bold',
    fontSize: Size.ms16,
  },
  iconTxtActive: {
    color: Color.greenGum,
    fontWeight: 'bold',
    fontSize: Size.ms16,
  },
  iconTxtComplete: {
    color: Color.bluelight,
    fontWeight: 'bold',
    fontSize: Size.ms16,
  },
  bottomPending: {
    backgroundColor: Color.orange,
    alignSelf: 'flex-end',
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: Size.wp9,
    paddingVertical: Size.wp2,
    borderRadius: Size.ms4,
    marginTop: Size.h2,
  },
  buttonAcc: {
    fontWeight: 'bold',
    fontSize: Size.ms17,
    color: 'white',
  },
});
