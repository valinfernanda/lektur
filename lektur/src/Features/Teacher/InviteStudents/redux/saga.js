import {all, takeLatest, put} from 'redux-saga/effects';
import axios from 'axios';
import {Store} from '../../../../Store/Store';
// import {navigate} from '../../../Utils/Nav';

function* SagaApproveStudent(payload) {
  try {
    const Token = Store.getState().LoginReducer.token;
    const CourseId = Store.getState().SetStudentDataById.CourseID;
    console.log(payload, 'ini payload invt student');
    //parameter post : url dan body. Bodynya bentuk object
    console.log(Token, 'ini token DI INVITE STUDENT');
    console.log(CourseId, 'ini CourseId DI INVITE STUDENT');
    // const Response = yield axios.put(
    //   `https://lekturapp.herokuapp.com/api/teacher/courses/student/approve?courseId=${payload.idGetStudent}&studentId=${payload.StudentId}`,
    //   {
    //     headers: {
    //       Authorization: `Bearer ${Token}`,
    //     },
    //   },
    //   {validateStatus: (status) => status < 500},
    // );
    const Response = yield axios({
      method: 'put',
      url: `https://lekturapp.herokuapp.com/api/teacher/courses/student/approve?courseId=${CourseId}&studentId=${payload.StudentId}`,
      headers: {
        Authorization: `Bearer ${Token}`,
      },
      validateStatus: (status) => status < 500,
    });

    console.log(Response, 'ini get dari APPROVE STUDENT');
    //yield put(SetStudentDataById(Response.data.result));
    // yield put({
    //     //WE'LL PUT TO STUDENT PROFILE REDUCER
    // //   type: 'SET_STUDENT_ID',
    // //   dataEnrolledStudent: Response.data.result,
    // });

    // yield put(navigate('InviteStudents', {}));
  } catch (error) {
    console.log(error);
  }
}

export function* SagasApproveStudent() {
  yield takeLatest('APPROVE_STUDENT', SagaApproveStudent);
}
