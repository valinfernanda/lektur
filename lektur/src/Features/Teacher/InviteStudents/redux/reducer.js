const InitialState = {
  dataEnrolledStudent: [],
  CourseID: '',
};

export const SetStudentDataById = (state = InitialState, action) => {
  switch (action.type) {
    case 'SET_STUDENT_ID':
      return {
        ...state,
        dataEnrolledStudent: action.dataEnrolledStudent,
      };
    case 'SET_COURSE_ID':
      return {
        ...state,
        CourseID: action.CourseID,
      };
    default:
      return state;
  }
};
