const InitialState = {
  editContent: [],
};

export const TeacherEdit = (state = InitialState, action) => {
  switch (action.type) {
    case 'SET_EDIT_CONTENT':
      return {
        ...state,
        editContent: action.editContent,
      };
    case 'SET_EDIT_COURSE':
      return {
        ...state,
        editCourse: action.editCourse,
      };
    default:
      return state;
  }
};
