export const ActionPutContent = () => {
  return {
    type: 'PUT_TEACHER_CONTENT',
  };
};

export const ActionPutCourse = (judul, penjelasan) => {
  return {
    type: 'PUT_TEACHER_COURSE',
    judul,
    penjelasan,
  };
};

export const ActionGetContentbyCourseId = (id) => {
  return {
    type: 'GET_CONTENT_BY_COURSE_ID',
    id,
  };
};
