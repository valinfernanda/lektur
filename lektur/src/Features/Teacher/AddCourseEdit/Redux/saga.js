import {all, takeLatest, put} from 'redux-saga/effects';
import axios from 'axios';
import {Store} from '../../../../Store/Store';
import {ActionPostCourse} from '../../AddCourse/Redux/action';

function* SagaGetContent(payload) {
  try {
    // const id = Store.getState().TeacherAddCourseReducer.id;
    const id = Store.getState().TeacherCourseReducer.courseDetail._id;

    const Token = Store.getState().LoginReducer.token;
    // console.log('INI ID NYA WOY', id);
    const Response = yield axios.get(
      `https://lekturapp.herokuapp.com/api/content/all?courseId=${id}`,
      {
        //put the token into auth bearer
        headers: {
          Authorization: `Bearer ${Token}`,
        },
      },
    );
    console.log('INI RESPON DARI EDIT CONTENT TEACHER ', Response);

    yield put({
      type: 'SET_EDIT_CONTENT',
      editContent: Response.data.result,
    });
  } catch (error) {
    console.log(error, 'ini error dari edit content');
  }
}

function* SagaEditCourse(payload) {
  try {
    const id = Store.getState().TeacherCourseReducer.courseDetail._id;
    const token = Store.getState().LoginReducer.token;
    //parameter post : url dan body. Bodynya bentuk object
    console.log(payload);
    const body = {
      judul: payload.judul,
      penjelasan: payload.penjelasan,
    };
    console.log(token, 'ini token'), console.log(body, 'ini body');
    const Response = yield axios({
      method: 'put',
      url: `https://lekturapp.herokuapp.com/api/courses/update?courseId=${id}`,
      data: body,
      headers: {
        Authorization: `Bearer ${token}`,
      },
      validateStatus: (status) => status < 500,
    });
    console.log(Response, 'ini response dari dari edit course');
    // yield put(gantiPhoto(payload.imagePicker));
    // yield put(ActionPostCourse(Response.data.result.email, password));

    console.log('ini response', Response);
  } catch (error) {
    console.log(error);
  }
}

export function* SagasEditTeacher() {
  yield takeLatest('GET_CONTENT_BY_COURSE_ID', SagaGetContent);
  yield takeLatest('PUT_TEACHER_COURSE', SagaEditCourse);
}
