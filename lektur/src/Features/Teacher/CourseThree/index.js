import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  View,
  Text,
  ScrollView,
  TouchableOpacity,
  TouchableHighlight,
  TextInput,
} from 'react-native';
import {connect} from 'react-redux';
import {Button, Link, TabItem} from '../../../Components';
// import {ActionGetCourse} from '../../Teacher/Course/Redux/action';
import {ActionPostCourseTwo} from '../CourseThree/Redux/action';

import Gap from '../../../Components/atoms/Gap/index';
import {Color} from '../../../Shared/Global/Config/Color';
import {Size} from '../../../Shared/Global/Config/Size';

const CourseThree = (props) => {
  const [titlecontent, setTitleContent] = useState('');
  const [desccontent, setDescContent] = useState('');
  // useEffect(() => {
  //   // props.GetCourseAddedbyTeacher();
  //   // console.log(props.ActionGetCourse, 'ini buat nge get teacher course ');
  // }, []);
  // // console.log(props.listCourse);

  return (
    <ScrollView>
      <View style={styles.container}>
        <Text style={styles.judul}>{props.courseDetail.title}</Text>
        <Gap height={15} />
        <Text>
          {/* Nascetur consequat quam tellus sed convallis amet, nunc. Venenatis,
          eget faucibus iaculis facilisi pellentesque eleifend mattis vel. Nunc
          euismod morbi lectus aliquam pretium, pharetra, tellus orci. Lobortis
          at nulla dictum risus amet... See more */}
          {props.courseDetail.overview}
        </Text>
        <Text style={styles.konten}>Content*</Text>
        <View style={styles.kotak}>
          <Text style={styles.lesson}>Lesson #1 </Text>
          <TextInput placeholder="Title*" style={styles.inputan} />
          <TextInput
            multiline
            numberOfLines={8}
            placeholder="Description*"
            style={styles.desc}
          />
          <TouchableHighlight
            style={styles.uploadVideoButton}
            onPress={() => {}}>
            <Text style={styles.uploadVideoText}>Upload Video</Text>
          </TouchableHighlight>
          <Gap height={8} />
          <Text style={styles.req}>
            Required. Max. size 200 MB. Supported format .mp4
          </Text>
          <Gap height={15} />
          <TouchableHighlight style={styles.addLessonButton} onPress={() => {}}>
            <Text style={styles.addLessonText}>Add Lesson Material</Text>
          </TouchableHighlight>
          <Text style={styles.req}>Max. size 20 MB. Supported format .pdf</Text>

          <View style={styles.coursebuttonContainer}>
            <Gap height={40} />
            <TouchableHighlight
              style={styles.saveButton}
              onPress={() => {
                props.navigation.navigate('CourseFour');
              }}>
              <Text style={styles.textStyleSave}>SAVE</Text>
            </TouchableHighlight>
          </View>
        </View>
        <TouchableOpacity style={styles.newLessontButton} onPress={() => {}}>
          <Text style={styles.newLessonText}>Add New Lesson</Text>
        </TouchableOpacity>
        <View style={styles.publishCoursebuttonContainer}>
          <Gap height={40} />
          <TouchableHighlight style={styles.publishButton} onPress={() => {}}>
            <Text style={styles.publishTextStyle}>PUBLISH COURSE!</Text>
          </TouchableHighlight>
          <Gap height={10} />
          <TouchableOpacity style={styles.newLessontButton} onPress={() => {}}>
            <Text style={styles.deleteCourseText}>Delete Course</Text>
          </TouchableOpacity>
        </View>
      </View>
    </ScrollView>
  );
};

const mapStateToProps = (state) => ({
  // listCourse: state.TeacherCourseReducer.listCourse,
  courseDetail: state.TeacherCourseReducer.courseDetail,
});

const mapDispatchToProps = {
  ActionPostCourseTwo,
};

export default connect(mapStateToProps, mapDispatchToProps)(CourseThree);

export const styles = StyleSheet.create({
  judul: {
    fontSize: Size.ms24,
    fontWeight: 'bold',
    marginTop: Size.h6,
  },
  publishButton: {
    backgroundColor: Color.oxfordBlue,
    padding: Size.h1,
    width: Size.wp46,
    borderRadius: Size.ms3,
    elevation: 2,
  },
  deleteCourseText: {
    fontSize: 15,
    fontStyle: 'normal',
    textDecorationLine: 'underline',
    color: Color.red,
    marginBottom: Size.h19,
  },
  publishTextStyle: {
    textAlign: 'center',
    color: Color.white,
  },
  publishCoursebuttonContainer: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
  },
  saveButton: {
    backgroundColor: Color.oxfordBlue,
    padding: Size.h1,
    width: Size.wp30,
    borderRadius: Size.ms3,
    elevation: 2,
  },
  addLesson: {
    marginLeft: Size.wp92,
  },
  textStyleSave: {
    textAlign: 'center',
    color: Color.white,
  },
  newLessonText: {
    fontSize: Size.ms14,
    fontWeight: 'bold',
    fontStyle: 'normal',
    textDecorationLine: 'underline',
    color: Color.bluelight,
    marginLeft: Size.wp3,
    paddingTop: Size.h5,
  },
  addLesson: {
    marginBottom: Size.h60,
  },
  coursebuttonContainer: {
    flex: 1,
    alignItems: 'flex-end',
    flexDirection: 'column',
    justifyContent: 'space-evenly',
  },
  addLessonButton: {
    backgroundColor: Color.white,
    padding: Size.h1,
    width: Size.wp46,
    borderRadius: Size.ms3,
    borderColor: Color.orange,
    borderWidth: 1,
    marginBottom: 8,

    // paddingTop: Size.ms14,
  },
  uploadVideoText: {
    paddingLeft: Size.ms12,
  },
  container: {
    marginHorizontal: 24,
  },
  req: {
    fontStyle: 'italic',
    fontSize: Size.ms11,
    color: 'grey',
  },
  addLessonText: {
    alignSelf: 'center',
    color: Color.orange,
    fontWeight: 'bold',
  },

  konten: {
    fontSize: Size.ms16,
    fontWeight: 'bold',
    marginTop: Size.h10,
    marginLeft: Size.wp3,
  },
  kotak: {
    // backgroundColor: 'pink',
    width: Size.wp82,
    // height: Size.h50,
    paddingHorizontal: Size.ms17,
    paddingTop: Size.ms11,
    borderWidth: 0.5,
    alignSelf: 'center',
    paddingBottom: Size.ms20,
    elevation: 1,
  },
  inputan: {
    height: Size.h6,
    width: Size.wp70,
    alignSelf: 'center',
    marginTop: Size.wp3,
    borderBottomWidth: 1,
  },
  desc: {
    width: Size.wp70,
    alignSelf: 'center',
    // backgroundColor: 'blue',
    borderBottomWidth: 1,
  },
  lesson: {
    fontSize: Size.ms14,
    fontWeight: 'bold',
  },
  uploadVideoButton: {
    backgroundColor: Color.orange,
    padding: Size.h1,
    width: Size.wp30,
    borderRadius: Size.ms3,
    marginTop: Size.ms11,
  },
});
