export const GetCourseAddedbyTeacher = (id) => {
  return {
    type: 'GET_TEACHER_COURSE_DUA',
    id,
  };
};

export const ActionPostCourseTwo = (titlecontent, desccontent) => {
  return {
    type: 'POST_TEACHER_COURSE_TWO',

    titlecontent,
    desccontent,
  };
};

// export const ActionGetCourse = (isiJudul, isiPenjelasan) => {
//   return {
//     type: 'GET_TEACHER_COURSE',
//     isiJudul,
//     isiPenjelasan,
//   };
// };
