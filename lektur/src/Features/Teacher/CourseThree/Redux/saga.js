import {all, takeLatest, put} from 'redux-saga/effects';
import axios from 'axios';
import {navigate} from '../../../Utils/Nav';
import jwt from 'jwt-decode';
import {Alert} from 'react-native';
import {SetLogin} from './action';
import {ActionPostCourse} from './action';
import {Store} from '../../../../Store/Store';

function* SagaGetCourse(data) {
  try {
    const Token = Store.getState().LoginReducer.token;
    console.log(data);
    //parameter post : url dan body. Bodynya bentuk object
    const body = {
      //si variabel ini sesuain sama nama di APInya wey, dan valuenya sesuaiin sama yang di action
      title: data.titlecontent,
      description: data.desccontent,
    };
    const Response = yield axios({
      method: 'GET',
      // url: `https://lekturapp.herokuapp.com/api/courses/filled?courseId=${data.id}`,
      // url: `https://lekturapp.herokuapp.com/api/content/create?courseId=${data.id}`,
      url: `https://lekturapp.herokuapp.com/api/courses/detail?courseId=${data.id}`,

      data: body,
      headers: {Authorization: `Bearer ${Token}`},
      validateStatus: (status) => status < 500,
    });
    console.log(Response, 'ini fetch all content ');
    // yield put({
    //     type:
    // })
  } catch (error) {
    console.log(error);
  }
}

export function* SagasGetCourse() {
  yield takeLatest('GET_TEACHER_COURSE_DUA', SagaGetCourse);
}
