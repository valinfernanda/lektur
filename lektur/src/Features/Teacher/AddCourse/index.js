import React, {useState} from 'react';
import {
  StyleSheet,
  View,
  Text,
  ScrollView,
  TouchableOpacity,
  TouchableHighlight,
  TextInput,
  Image,
  ImageBackground,
} from 'react-native';
import {Size} from '../../../Shared/Global/Config/Size';
// import ReadMore from '@fawazahmed/react-native-read-more';
import {Color} from '../../../Shared/Global/Config/Color';
import Gap from '../../../Components/atoms/Gap/index';
import {Header, Link} from '../../../Components';
import {connect} from 'react-redux';
import {
  ActionPostCourse,
  ActionPostContent,
  ActionPostPdf,
  ActionPostVideo,
} from './Redux/action';
import TeacherHeader from '../teacherHeader';
import DocumentPicker from 'react-native-document-picker';
import {
  BGClassroom,
  BGGetStarted,
  Dokumen,
  DummyLaptop,
  PencilEdit,
} from '../../../Assets';
import Read from './Read';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';

const AddCourse = (props) => {
  const [judul, setJudul] = useState('');
  const [penjelasan, setPenjelasan] = useState('');
  const [addNewCourse, setAddNewCourse] = useState([]);
  const [number, setNumber] = useState(1);
  const [titlecontent, setTitleContent] = useState('');
  const [desccontent, setDescContent] = useState('');
  const [savedTitle, setSavedTitle] = useState(false);
  const [newtitle, setNewTitle] = useState('');
  const [imagePicker, setImagePicker] = useState({});
  const [videoPicker, setVideoPicker] = useState({});
  const [pdfFile, setPdfFile] = useState([]);

  const getImage = () => {
    launchImageLibrary(
      {
        mediaType: 'photo',
        title: 'Select Image',
        // customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
        storageOptions: {
          skipBackup: true,
          path: 'images',
        },
        allowsEditing: true,
      },
      async (response) => {
        setImagePicker(response);
        console.log(response, 'response: ');
      },
    );
  };
  console.log(imagePicker, 'set photo wey');

  const getVideo = () => {
    launchImageLibrary(
      {
        mediaType: 'video',
        title: 'Select Video',
        videoQuality: 'low',
        // customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
        storageOptions: {
          skipBackup: true,
          // path: 'images',
        },
        // allowsEditing: true,
      },
      async (response) => {
        setVideoPicker(response);
        console.log(response, 'response: ');
      },
    );
  };
  console.log(videoPicker, 'set video wey');

  const getDocs = async () => {
    try {
      const res = await DocumentPicker.pick({
        type: [DocumentPicker.types.pdf],
      });
      setPdfFile(res);
      console.log(res, 'ini get docs');
    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        // User cancelled the picker, exit any dialogs or menus and move on
      } else {
        throw err;
      }
    }
  };

  const tambahContent = () => {
    setNumber(number + 1);
    setTitleContent('');
    setDescContent('');

    const dataContent = {
      number: number,
      title: '',
      description: '',
      pdf: '',
      saved: false,
    };
    setAddNewCourse((prevState) => [...prevState, dataContent]);
  };

  const saveContent = (number) => {
    const dataContent = {
      id: props.id,
      number: number,
      title: titlecontent,
      description: desccontent,
      pdf: pdfFile,
      video: videoPicker,
      saved: true,
    };

    setAddNewCourse((prevState) => {
      prevState[number - 1] = dataContent;
      return [...prevState];
    });
    console.log(addNewCourse, 'from saved');
    props.ActionPostContent(
      props.id,
      number,
      titlecontent,
      desccontent,
      pdfFile,
      videoPicker,
    );
    // props.ActionPostPdf(pdfFile);
  };
  console.log(addNewCourse, 'ini data content');

  const editContent = (data) => {
    setTitleContent(data.title);
    setDescContent(data.description);
    const dataContent = {
      number: data.number,
      title: data.title,
      description: data.description,
      saved: false,
    };
    setAddNewCourse((prevState) => {
      prevState[data.number - 1] = dataContent;
      return [...prevState];
    });
    console.log(addNewCourse);
  };

  const setCourseHeader = () => {
    setSavedTitle(true);
    props.ActionPostCourse(judul, penjelasan, imagePicker);
  };

  const editHeaderCourse = () => {
    setSavedTitle(false);
  };

  // const createTitle = (index, text) => {
  //   setTitleContent((prevState) => {
  //     prevState[index].title = text;
  //     return [...prevState];
  //   });
  // };
  const selectFile = async () => {
    // Opening Document Picker to select one file
    try {
      const res = await DocumentPicker.pick({
        // Provide which type of file you want user to pick
        type: [DocumentPicker.types.allFiles],
        // There can me more options as well
        // DocumentPicker.types.allFiles
        // DocumentPicker.types.images
        // DocumentPicker.types.plainText
        // DocumentPicker.types.audio
        // DocumentPicker.types.pdf
      });
      // Printing the log realted to the file
      console.log('res : ' + JSON.stringify(res));
      // Setting the state to show single file attributes
      setSingleFile(res);
    } catch (err) {
      setSingleFile(null);
      // Handling any exception (If any)
      if (DocumentPicker.isCancel(err)) {
        // If user canceled the document selection
        alert('Canceled');
      } else {
        // For Unknown Error
        alert('Unknown Error: ' + JSON.stringify(err));
        throw err;
      }
    }
  };

  return (
    <>
      <Header title="New Course" onPress={() => props.navigation.goBack()} />
      <TeacherHeader navigate={props.navigation.navigate} />
      <ScrollView showsVerticalScrollIndicator={true}>
        <View style={styles.container}>
          <View style={styles.topContainerCover}>
            <View style={styles.topContainerBottom}>
              <View style={styles.courseDesc}>
                <View style={styles.courseDescTop}>
                  {savedTitle ? (
                    <>
                      <View style={styles.kotakpalingluar}>
                        <View style={styles.kotakluar}>
                          <View style={styles.judulatas}>
                            <Text style={styles.tema}>{props.title}</Text>
                            <Gap width={20} />
                            <TouchableOpacity onPress={editHeaderCourse}>
                              <PencilEdit />
                            </TouchableOpacity>
                          </View>
                          <Gap height={30} />
                          <View style={styles.under}>
                            <Read text={props.overview} />
                          </View>
                        </View>
                      </View>
                    </>
                  ) : (
                    <View style={styles.courseDesc}>
                      <View style={styles.courseDescTop}>
                        {/* <Read /> */}
                        <TextInput
                          placeholder="Title*"
                          onChangeText={(text) => setJudul(text)}
                          style={styles.titleInput}
                          value={judul}
                        />

                        <Gap height={8} />
                        <TextInput
                          multiline
                          numberOfLines={5}
                          onChangeText={(text) => setPenjelasan(text)}
                          placeholder="OverView*"
                          style={styles.overViewInput}
                          value={penjelasan}
                        />
                        <Gap height={20} />
                        <Image
                          source={{uri: imagePicker.uri}}
                          style={{width: '100%'}}
                          resizeMode={'cover'}
                        />
                        <TouchableHighlight
                          style={styles.AddHeaderButton}
                          onPress={getImage}>
                          <Text style={styles.textStyleAddHeader}>
                            Add Header Image
                          </Text>
                        </TouchableHighlight>
                        <Text style={styles.warningMax}>
                          Max. size 5 MB. Supported format .png/jpg/jpeg
                        </Text>
                      </View>

                      <View style={styles.coursebuttonContainer}>
                        <Gap height={20} />
                        <TouchableHighlight
                          style={styles.saveButton}
                          onPress={() => {
                            setCourseHeader();
                          }}>
                          {/* onPress={setCourseHeader} */}
                          <Text style={styles.textStyleSave}>SAVE</Text>
                        </TouchableHighlight>
                      </View>
                    </View>
                  )}
                </View>
              </View>
            </View>
          </View>

          {addNewCourse.length > 0
            ? addNewCourse.map((value, index) => {
                return (
                  <View style={styles.kotak}>
                    {/* <Text style={styles.lesson}>Lesson #{value.number} </Text>
                     */}
                    <View style={styles.judulatas}>
                      <Text>
                        {' '}
                        Lesson #{value.number}{' '}
                        {value.title === '' ? null : `: ${value.title}`}
                      </Text>
                      <Gap width={5} />

                      {value.title === '' || value.saved === false ? null : (
                        <TouchableOpacity onPress={() => editContent(value)}>
                          <PencilEdit />
                        </TouchableOpacity>
                      )}
                    </View>
                    {value.title === '' || value.saved === false ? (
                      <TextInput
                        onChangeText={(text) => setTitleContent(text)}
                        // onChangeText={(text) => {
                        //   createTitle(index, text);
                        // }}
                        placeholder="Title*"
                        style={styles.inputan}
                        value={titlecontent}
                      />
                    ) : (
                      <>
                        <Gap height={20} />
                        <View style={styles.letakGambar}>
                          <Image source={DummyLaptop} style={styles.gambar} />
                        </View>

                        <Gap height={15} />
                        <View style={styles.link}>
                          <Dokumen />

                          <Gap width={14} />
                          <Link title="jhdajabdjhbas" />
                        </View>
                        <Gap height={15} />
                        <View style={styles.link}>
                          <Gap height={15} />
                          <Dokumen />
                          <Gap width={14} />
                          <Link title="jhdajabdjhbas" />

                          <Gap height={60} />
                        </View>
                      </>
                    )}

                    {value.description === '' || value.saved === false ? (
                      <TextInput
                        multiline
                        numberOfLines={8}
                        onChangeText={(text) => setDescContent(text)}
                        placeholder="Description*"
                        style={styles.desc}
                        value={desccontent}
                      />
                    ) : (
                      <Text>{value.description}</Text>
                    )}

                    {(value.title === '' && value.description === '') ||
                    !value.saved ? (
                      <>
                        <Gap height={20} />
                        <TouchableHighlight
                          style={styles.uploadVideoButton1}
                          onPress={getVideo}>
                          <Text style={styles.uploadVideoText}>
                            Upload Video
                          </Text>
                        </TouchableHighlight>
                        <Gap height={8} />
                        <Text style={styles.req}>
                          Required. Max. size 200 MB. Supported format .mp4
                        </Text>
                        <Gap height={15} />
                        <TouchableHighlight
                          style={styles.addLessonButton}
                          onPress={getDocs}>
                          <Text style={styles.addLessonText}>
                            Add Lesson Material
                          </Text>
                        </TouchableHighlight>
                        <Text style={styles.req}>
                          Max. size 20 MB. Supported format .pdf
                        </Text>

                        <View style={styles.coursebuttonContainer}>
                          <Gap height={40} />
                          <TouchableHighlight
                            style={styles.saveButton}
                            onPress={() => {
                              saveContent(value.number);
                            }}>
                            <Text style={styles.textStyleSave}>SAVE</Text>
                          </TouchableHighlight>
                        </View>
                      </>
                    ) : null}
                  </View>
                );
              })
            : null}
          <TouchableOpacity
            style={styles.newLessontButton}
            onPress={tambahContent}>
            <Text style={styles.newLessonText}>Add New Lesson</Text>
          </TouchableOpacity>
          <Gap height={30} />

          <View style={styles.publishCoursebuttonContainer}>
            <Gap height={40} />
            <TouchableHighlight style={styles.publishButton} onPress={() => {}}>
              <Text style={styles.publishTextStyle}>PUBLISH COURSE!</Text>
            </TouchableHighlight>
            <Gap height={10} />
            <TouchableOpacity
              style={styles.newLessontButton}
              onPress={() => {}}>
              <Text style={styles.deleteCourseText}>Delete Course</Text>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    </>
  );
};
const mapStateToProps = (state) => ({
  id: state.TeacherAddCourseReducer.id,
  title: state.TeacherAddCourseReducer.judul,
  overview: state.TeacherAddCourseReducer.penjelasan,
});

const mapDispatchToProps = {
  ActionPostCourse,
  ActionPostContent,
  ActionPostPdf,
  ActionPostVideo,
};

export default connect(mapStateToProps, mapDispatchToProps)(AddCourse);

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-evenly',
    flexDirection: 'column',
    padding: 20,
  },
  tema: {
    fontWeight: 'bold',
    fontSize: Size.ms24,
  },
  judulatas: {
    flexDirection: 'row',
    alignItems: 'center',
  },

  titleInput: {
    // backgroundColor: Color.paleGray,
    backgroundColor: 'white',
  },

  overViewInput: {
    // backgroundColor: Color.paleGray,
    backgroundColor: 'white',
    height: Size.h19,
  },

  AddHeaderButton: {
    backgroundColor: Color.white,
    padding: Size.h1,
    width: Size.wp46,
    borderRadius: Size.ms3,
    borderColor: Color.orange,
    borderWidth: 1,
    marginBottom: 20,
  },

  textStyleAddHeader: {
    color: Color.orange,
    fontWeight: 'bold',
    textAlign: 'center',
  },

  warningMax: {
    fontStyle: 'normal',
    fontSize: Size.ms12,
    // color: Color.paleGray,
    color: 'grey',
  },

  topContainerCover: {
    flex: 2,
    height: Size.h60,
    width: Size.wp92,
    marginBottom: 20,
  },
  // under: {
  //   paddingTop: Size.wp20,
  //   backgroundColor: 'blue',
  // },

  contentTopContainerCover: {
    flex: 2,
    height: Size.h75,
    width: Size.wp92,
    marginBottom: 20,
  },
  topContainerBottom: {
    flex: 3,
  },
  judultengah: {
    backgroundColor: 'white',
  },

  courseDesc: {
    backgroundColor: 'white',
    flex: 3,
    elevation: 2,
  },
  courseDescTop: {
    flex: 1,
    justifyContent: 'space-between',
    padding: Size.wp3,
    // backgroundColor: 'blue',
  },

  coursebuttonContainer: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-evenly',
  },
  uploadVideoButton1: {
    backgroundColor: Color.orange,
    padding: Size.h1,
    width: Size.wp30,
    borderRadius: Size.ms3,
    marginTop: Size.ms11,
  },

  saveButton: {
    backgroundColor: Color.oxfordBlue,
    padding: Size.h1,
    width: Size.wp30,
    borderRadius: Size.ms3,
    elevation: 2,
    // paddingRight: 30,
  },

  textStyleSave: {
    textAlign: 'center',
    color: Color.white,
  },

  content: {
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: 20,
  },
  lesson: {
    fontSize: Size.ms14,
    fontWeight: 'bold',
  },
  addLessonButton: {
    backgroundColor: Color.white,
    padding: Size.h1,
    width: Size.wp46,
    borderRadius: Size.ms3,
    borderColor: Color.orange,
    borderWidth: 1,
    marginBottom: 8,

    // paddingTop: Size.ms14,
  },
  req: {
    fontStyle: 'italic',
    fontSize: Size.ms11,
    color: 'grey',
  },
  coursebuttonContainer: {
    flex: 1,
    alignItems: 'flex-end',
    flexDirection: 'column',
    justifyContent: 'space-evenly',
  },
  desc: {
    width: Size.wp70,
    alignSelf: 'center',
    // backgroundColor: 'blue',
    borderBottomWidth: 1,
  },
  inputan: {
    height: Size.h6,
    width: Size.wp70,
    alignSelf: 'center',
    marginTop: Size.wp3,
    borderBottomWidth: 1,
  },
  kotak: {
    // backgroundColor: 'pink',
    width: Size.wp92,
    // height: Size.h50,
    paddingHorizontal: Size.ms17,
    paddingTop: Size.ms11,
    // borderWidth: 0.5,
    alignSelf: 'center',
    paddingBottom: Size.ms20,
    elevation: 2,
    backgroundColor: 'white',
  },
  kotakpalingluar: {
    alignItems: 'center',
    paddingTop: Size.h10,
  },
  kotakluar: {
    // borderWidth: 1,
    // backgroundColor: 'blue',
    width: Size.wp82,
    // alignItems: 'center',
  },

  uploadVideoButton: {
    backgroundColor: Color.orange,
    padding: Size.h1,
    width: Size.wp46,
    borderRadius: Size.ms3,
    marginBottom: 20,
  },
  uploadVideoText: {
    paddingLeft: Size.ms12,
  },

  uploadVideoText: {
    color: Color.white,
    fontWeight: 'bold',
    textAlign: 'center',
  },

  addLessonButton: {
    backgroundColor: Color.white,
    padding: Size.h1,
    width: Size.wp46,
    borderRadius: Size.ms3,
    borderColor: Color.orange,
    borderWidth: 1,
    marginBottom: 20,
  },

  addLessonText: {
    color: Color.orange,
    fontWeight: 'bold',
    textAlign: 'center',
  },

  newLessonButton: {
    backgroundColor: Color.white,
  },

  newLessonText: {
    fontSize: 20,
    fontStyle: 'normal',
    textDecorationLine: 'underline',
    color: Color.bluelight,
  },

  publishCoursebuttonContainer: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-evenly',
  },

  publishButton: {
    backgroundColor: Color.oxfordBlue,
    padding: Size.h1,
    width: Size.wp46,
    borderRadius: Size.ms3,
    elevation: 2,
  },

  publishTextStyle: {
    textAlign: 'center',
    color: Color.white,
  },

  deleteCourseText: {
    fontSize: 15,
    fontStyle: 'normal',
    textDecorationLine: 'underline',
    color: Color.red,
  },
  bungkus: {
    width: 324,
    backgroundColor: 'white',
    elevation: 5,
  },
  link: {
    paddingLeft: 17,
    flexDirection: 'row',
  },
  judul: {
    fontWeight: 'bold',
    paddingTop: 24,
    paddingLeft: 17,
    fontSize: 18,
  },
  gambar: {
    borderRadius: 2,
    width: 296,
    height: 202,
  },
  letakGambar: {
    alignSelf: 'center',
  },
});
