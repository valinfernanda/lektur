import React, {useState, useCallback} from 'react';
import {View, Text} from 'react-native';
//component
import {styles} from './style';

const Read = (props) => {
  const [textShown, setTextShown] = useState(false); //To show ur remaining Text
  const [lengthMore, setLengthMore] = useState(false); //to show the "Read more & Less Line"
  const toggleNumberOfLines = () => {
    //To toggle the show text or hide it
    setTextShown(!textShown);
  };

  const onTextLayout = useCallback((e) => {
    setLengthMore(e.nativeEvent.lines.length >= 2); //to check the text is more than 4 lines or not
    console.log(e.nativeEvent.lines.length);
  }, []);

  return (
    <View style={{flex: 1, backgroundColor: 'blue'}}>
      <Text
        onTextLayout={onTextLayout}
        numberOfLines={textShown ? undefined : 3}
        style={styles.courseDetilTxt}>
        {props.text}
      </Text>

      {lengthMore ? (
        <Text
          onPress={toggleNumberOfLines}
          style={{
            lineHeight: 25,
            fontWeight: 'bold',
          }}>
          {textShown ? 'Read less...' : 'Read more...'}
        </Text>
      ) : null}
    </View>
  );
};

export default Read;
