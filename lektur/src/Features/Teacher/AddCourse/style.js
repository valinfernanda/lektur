//STYLES AREA
import {Size} from '../../../Shared/Global/Config/Size';
import {Color} from '../../../Shared/Global/Config/Color';
import {StyleSheet, StatusBar} from 'react-native';
import {color} from 'react-native-reanimated';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    // paddingTop: StatusBar.currentHeight,
    justifyContent: 'space-between',
  },
  topContainer: {
    height: Size.h45,
    // backgroundColor: 'silver',
  },
  topContainerCover: {
    marginHorizontal: Size.wp5,
    marginTop: Size.hmin37,
    resizeMode: 'contain',
  },
  contain: {
    alignSelf: 'center',
  },
  judul: {
    fontSize: Size.ms23,
    fontWeight: 'bold',
    paddingLeft: Size.wp10,
    paddingTop: Size.wp9,
  },
  topContainerTop: {
    flex: 0.5,
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingHorizontal: Size.wp5,
  },
  topContainerBottom: {
    flex: 1,
    paddingTop: 60,
  },
  courseDetil: {
    backgroundColor: 'white',
    flex: 1,
    padding: Size.wp7,
    elevation: 5,
  },
  courseDetilUp: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderBottomWidth: Size.ms1,
    borderBottomColor: 'silver',
    paddingBottom: Size.wp5,
  },
  courseDetilUp1: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  courseDetilDown: {
    flex: 2,
    paddingTop: Size.wp5,
  },
  videoDesc: {
    marginTop: Size.hmin7,
  },
  videoDescTop: {
    flex: 1,
    justifyContent: 'center',
  },
  videoDescBottom: {
    flex: 1,
    // alignItems: 'flex-end',
    justifyContent: 'center',
    paddingRight: Size.wp5,
  },

  //BOTTOM CONTAINER AREA
  bottomContainer: {
    height: Size.h100,
  },
  bottomUp: {
    flex: 2,
    justifyContent: 'center',
  },
  bottomDown: {
    flex: 4,
    // backgroundColor: 'brown',
  },
  bottomLessons: {
    // backgroundColor: 'blue',
    height: Size.h60,
    paddingLeft: Size.wp5,
  },
  bottomLessonsPict: {
    flex: 2,
    width: '80%',
    backgroundColor: 'black',
  },
  bottomLessonsDesc: {
    flex: 3,
    width: '80%',
  },
  bottomLsnDesc1: {
    flex: 2,
    padding: Size.wp3,
    backgroundColor: 'white',
  },
  bottomLsnDesc2: {
    flex: 3,
    paddingHorizontal: Size.wp3,
    paddingBottom: Size.wp3,
    backgroundColor: 'white',
  },
  bottomLsnDesc3: {
    flex: 1,
    padding: Size.wp3,
    backgroundColor: Color.pink,
    justifyContent: 'center',
  },
  btmLsnDscCver: {
    flex: 2,
  },
  btmLsnDscPlus: {
    flex: 1,
    flexDirection: 'row',
    alignContent: 'flex-end',
    paddingTop: Size.wp4,
  },

  //BOTTOM FOOTER
  bottomFooter: {
    elevation: 5,
    marginHorizontal: Size.hmin3,
  },
  bottomFooterCover: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignContent: 'center',
    paddingHorizontal: Size.wp13,
    paddingVertical: Size.wp7,
    paddingBottom: Size.wp3,
  },

  //COMPONENT

  openButton: {
    alignSelf: 'flex-start',
    backgroundColor: Color.orange,
    padding: Size.h1,
    borderRadius: Size.ms3,
    opacity: 0.5,
    elevation: 2,
  },
  openButtonFooter: {
    alignSelf: 'flex-start',
    backgroundColor: Color.orange,
    padding: Size.h1,
    borderRadius: Size.ms3,
    elevation: 2,
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: Size.ms15,
    textAlign: 'center',
  },
  textStyle2: {
    color: 'black',
    fontSize: Size.ms16,
  },
  courseDetilNum: {
    color: Color.orange,
    fontSize: Size.ms28,
    fontWeight: 'bold',
  },
  courseDetilTxt: {
    fontSize: Size.ms15,
    lineHeight: 25,
    backgroundColor: '#F6F6F6',
    elevation: 1,
    borderWidth: 1,
    padding: Size.ms9,
  },
  videoDescTop2: {
    fontWeight: 'bold',
    fontSize: 35,
    color: 'white',
    marginBottom: Size.h1,
  },
  videoDescTop3: {
    fontSize: Size.ms18,
    marginBottom: Size.h2,
    color: '#E5E5E5',
  },
  bottomUpText: {
    fontWeight: 'bold',
    fontSize: Size.ms20,
    marginLeft: Size.wp5,
    marginBottom: Size.wp10,
  },
  bawah: {
    alignSelf: 'center',
  },
  bottonUpButton: {
    backgroundColor: 'white',
    padding: Size.wp5,
    borderBottomWidth: Size.ms1,
    borderColor: 'silver',
    borderRadius: Size.ms4,
    marginHorizontal: Size.wp5,
    color: Color.darkblue,
  },
  bottomDescTxt: {
    fontSize: Size.ms15,
  },
  btmLsnDscTitle: {
    fontWeight: 'bold',
    fontSize: Size.ms20,
  },
  btmLsnDscTeacher: {
    fontSize: Size.ms16,
    color: 'silver',
  },
  btmLsnDscPlusTxt: {
    color: 'silver',
    paddingRight: Size.wp9,
  },
  bottomLsnDesc3Txt: {
    fontSize: Size.ms18,
  },
  courseDetilDownTxt: {
    fontSize: Size.ms15,
  },
  footerTitle: {
    fontWeight: 'bold',
    fontSize: Size.ms16,
    lineHeight: 21,
  },
  footerTeacher: {
    fontSize: Size.ms16,
    color: 'silver',
    lineHeight: 21,
  },
});
