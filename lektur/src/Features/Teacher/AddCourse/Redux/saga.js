import {all, takeLatest, put} from 'redux-saga/effects';
import axios from 'axios';
import {navigate} from '../../../Utils/Nav';
import jwt from 'jwt-decode';
import {Alert} from 'react-native';
import {SetLogin} from './action';
import {ActionPostCourse} from './action';
import {Store} from '../../../../Store/Store';
import {ActionInputAssesment} from '../../InputAssesment/Redux/action';
import {ActionLogin} from '../../../Auth/Login/Redux/action';

function* SagaAddCourse(data) {
  try {
    const Token = Store.getState().LoginReducer.token;
    // const email = Store.getState().LoginReducer.email;
    // const password = Store.getState().LoginReducer.password;
    console.log(data);
    //parameter post : url dan body. Bodynya bentuk object
    const body = {
      //si variabel ini sesuain sama nama di APInya wey, dan valuenya sesuaiin sama yang di action
      title: data.judul,
      overview: data.penjelasan,
      image: data.imagePicker,
    };

    const formData = new FormData();
    formData.append('title', data.judul);
    formData.append('overview', data.penjelasan);
    formData.append('file', {
      uri: data.imagePicker.uri,
      name: data.imagePicker.fileName,
      type: data.imagePicker.type,
    });

    const Response = yield axios({
      method: 'POST',
      url: `https://lekturapp.herokuapp.com/api/courses/create`,
      data: formData,
      headers: {
        Authorization: `Bearer ${Token}`,
        'Content-Type': 'multipart/form-data',
      },
      validateStatus: (status) => status < 500,
    });

    console.log(Response, 'ini hasil dari teacher add course');
    // yield put(ActionInputAssesment(Response.data.result._id));
    // yield put(ActionLogin(email, password));
    yield put({
      type: 'SET_ADD_COURSE',
      judul: Response.data.result.title,
      penjelasan: Response.data.result.overview,
      imagePicker: Response.data.result.image,
      id: Response.data.result._id,
    });

    // yield put(navigate('InputAssesment', {}));
  } catch (error) {
    console.log(error);
  }
}

function* SagaAddContent(data) {
  try {
    const Token = Store.getState().LoginReducer.token;
    console.log(data, 'ini data saga add content');
    //parameter post : url dan body. Bodynya bentuk object
    const body = {
      //si variabel ini sesuain sama nama di APInya wey, dan valuenya sesuaiin sama yang di action

      number: data.number,
      title: data.titlecontent,
      description: data.desccontent,
    };

    const Response = yield axios({
      method: 'POST',
      url: `https://lekturapp.herokuapp.com/api/content/create?courseId=${data.id}`,
      data: body,
      headers: {Authorization: `Bearer ${Token}`},
      validateStatus: (status) => status < 500,
    });
    console.log(Response, 'YUHU');

    const formData = new FormData();
    formData.append('file', {
      uri: data.pdf.uri,
      name: data.pdf.name,
      type: data.pdf.type,
    });

    console.log(formData, 'formdata add dokumen');

    const ResponseDoc = yield axios({
      method: 'POST',
      url: `https://lekturapp.herokuapp.com/api/content/upload/file?contentId=${Response.data.result._id}`,
      data: formData,
      headers: {
        Authorization: `Bearer ${Token}`,
        'Content-Type': 'multipart/form-data',
        Accept: 'application/json',
      },
      validateStatus: (status) => status < 500,
    });

    console.log(ResponseDoc, 'ini hasil dari upload document');

    const formDataVideo = new FormData();
    formDataVideo.append('video', {
      uri: data.video.uri,
      name: data.video.fileName,
      type: 'video/mp4',
    });

    const ResponseVideo = yield axios({
      method: 'PUT',
      url: `https://lekturapp.herokuapp.com/api/content/upload/video?contentId=${Response.data.result._id}`,
      data: formDataVideo,
      headers: {
        Authorization: `Bearer ${Token}`,
        'Content-Type': 'multipart/form-data',
      },
      validateStatus: (status) => status < 500,
    });

    console.log(ResponseVideo, 'ini hasil dari upload video');
    // yield put({
    //   type: 'SET_ADD_COURSE_TWO',
    //   titlecontent: Response.data.result.overview,
    //   desccontent: Response.data.result.image,
    // });
  } catch (error) {
    console.log(JSON.stringify(error), 'ini error dari content');
  }
}

function* SagaGetContent(payload) {
  try {
    const id = Store.getState().TeacherAddCourseReducer.id;
    const Token = Store.getState().LoginReducer.token;
    console.log('INI ID NYA WOY', id);
    const Response = yield axios.get(
      `https://lekturapp.herokuapp.com/api/content/all?courseId=${id}`,
      {
        //put the token into auth bearer
        headers: {
          Authorization: `Bearer ${Token}`,
        },
      },
    );
    console.log('INI RESPON DARI GET CONTENT', Response);

    // yield put({
    //   type: 'SET_TEACHER_ASSESMENT',

    //   listAssesment: Response.data.result,

    //   hasil: Response.data.result,
    // });
  } catch (error) {
    console.log(error);
  }
}

export function* SagasAddCourse() {
  yield takeLatest('POST_TEACHER_COURSE', SagaAddCourse);
  yield takeLatest('POST_TEACHER_CONTENT', SagaAddContent);
  yield takeLatest('GET_CONTENT_TEACHER', SagaGetContent);
  // yield takeLatest('POST_PDF', SagaAddPdf);
  // yield takeLatest('PUT_VIDEO', SagaAddVideo);
}
