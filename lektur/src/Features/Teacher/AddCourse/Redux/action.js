export const ActionPostCourse = (judul, penjelasan, imagePicker) => {
  return {
    type: 'POST_TEACHER_COURSE',
    judul,
    penjelasan,
    imagePicker,
  };
};

export const ActionPostContent = (
  id,
  number,
  titlecontent,
  desccontent,
  pdf,
  video,
) => {
  return {
    type: 'POST_TEACHER_CONTENT',
    id,
    number,
    titlecontent,
    desccontent,
    pdf,
    video,
  };
};

// export const ActionPostPdf = (pdf) => {
//   return {
//     type: 'POST_PDF',
//     pdf,
//   };
// };

// export const ActionPostVideo = (video) => {
//   return {
//     type: 'PUT_VIDEO',
//     video,
//   };
// };

export const ActionGetPdf = () => {
  return {
    type: 'GET_CONTENT_TEACHER',
  };
};
