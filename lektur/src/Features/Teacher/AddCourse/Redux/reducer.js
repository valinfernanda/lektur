const InitialState = {
  judul: '',
  penjelasan: '',
  imagePicker: '',
  errormessage: '',
  titlecontent: '',
  desccontent: '',
  number: '',
  id: '',
  dokumen: '',
  video: '',

  // isiJudul: '',
  // isiPenjelasan: '',
};

export const TeacherAddCourseReducer = (state = InitialState, action) => {
  switch (action.type) {
    case 'SET_ADD_COURSE':
      return {
        ...state,
        id: action.id,
        judul: action.judul,
        penjelasan: action.penjelasan,
        imagePicker: action.imagePicker,
      };
    case 'SET_ADD_COURSE_TWO':
      return {
        ...state,
        number: action.number,
        titlecontent: action.titlecontent,
        desccontent: action.desccontent,
      };
    case 'ADD_COURSE_ERROR':
      return {
        ...state,
        errormessage: action.errormessage,
      };
    case 'SET_ADD_PDF':
      return {
        ...state,
        dokumen: action.dokumen,
      };
    case 'SET_ADD_VIDEO':
      return {
        ...state,
        video: action.video,
      };
    case 'SET_GET_CONTENT':
      return {
        ...state,
        ambilcontent: action.ambilcontent,
      };
    // case 'SET_TEACHER_COURSE':
    //   return {
    //     ...state,
    //     isiJudul: action.isiJudul,
    //     isiPenjelasan: action.isiPenjelasan,
    //   };
    default:
      return state;
  }
};
