import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  ScrollView,
  TextInput,
  TouchableHighlight,
  TouchableOpacity,
} from 'react-native';
//import component
import {styles} from './style';
import {Check} from '../../../Assets';
import CheckBox from '@react-native-community/checkbox';
import TeacherHeader from '../teacherHeader';
import {ActionInputAssesment, actGetAssesment} from './Redux/action';

import {connect} from 'react-redux';
// import {AssignmentStudent} from '../..';
import Assessment from '../../Teacher/Assessment';
import {neq} from 'react-native-reanimated';

const InputAssesment = (props) => {
  // useEffect(() => {
  //   props.ActionInputAssesment();
  // }, []);

  const [question, setQuestions] = useState('');
  const [answer, setAnswer] = useState(0);
  const [indexchecked, setIndexChecked] = useState(0);
  const [text, setText] = useState('');
  const [option, setOption] = useState([]);
  const [remark, setRemark] = useState([]);

  const [number, setNumber] = useState(1);
  const [savedQuestion, setSavedQuestion] = useState(false);
  const [newQuestion, setNewQuestion] = useState([]);
  const [questionTitle, setQuestionTitle] = useState('');
  const [optionList, setOptionList] = useState([
    {text: 'Option', value: '', checked: false},
  ]);
  const [clicked, setClicked] = useState(false);
  const [newRemark, setNewRemark] = useState([]);
  const [toggleCheckBox, setToggleCheckBox] = useState(false);
  const [nomor, setNomor] = useState(1);

  console.log(newQuestion);

  const createAnswer = (value, index, i) => {
    console.log(text, index, i);
    setNewQuestion((prevState) => {
      prevState[index].options[i].text = value;
      return [...prevState];
    });
  };

  const createQuestionTitle = (index, text) => {
    setNewQuestion((prevState) => {
      prevState[index].question = text;
      return [...prevState];
    });
  };
  console.log(newQuestion, 'INI FINAL PLEASE');

  const createRemark = (index, text) => {
    setNewQuestion((prevState) => {
      prevState[index].remark = text;
      return [...prevState];
    });
  };

  const addOption = (index) => {
    const daftarOption = {
      text: '',
      placeholder: 'Option',
      value: nomor + 1,
      checked: false,
    };

    console.log(daftarOption, 'cobain');

    setNewQuestion((prevState) => {
      prevState[index].options = [...prevState[index].options, daftarOption];

      return prevState;
    });
    setNomor(nomor + 1);
  };
  console.log(newQuestion, 'ini new question aye');

  const tambahContent = () => {
    setNomor(1);
    setNumber(number + 1);

    setQuestions('');
    setOption('');
    setRemark('');
    setOptionList([
      {
        text: 'Option',
        value: '',
      },
    ]);

    const soalUjian = {
      number: number,
      question: '',
      options: [
        {
          text: '',
          value: 1,
          checked: false,
          placeholder: 'Option',
        },
      ],
      remark: '',
      saved: false,
    };
    setNewQuestion((prevState) => [...prevState, soalUjian]);
  };

  const saveQuestion = (number) => {
    const soalUjian = {
      number: number,
      question: question,
      options: option,
      remark: remark,

      saved: true,
    };
    setNewQuestion((prevState) => {
      prevState[number - 1] = soalUjian;
      return [...prevState];
    });
    console.log(newQuestion, 'from saved');
  };

  const editContent = (data) => {
    // setTitleContent(data.title);
    setQuestions(data.question);
    setRemark(data.remark);
    // setDescContent(data.description);
    setOption(data.options);
    const soalUjian = {
      number: data.number,
      question: data.question,
      options: data.options,
      remark: data.remark,
      saved: false,
    };
    setNewQuestion((prevState) => {
      prevState[data.number - 1] = soalUjian;
      return [...prevState];
    });
    console.log(newQuestion);
  };

  const createQuestion = () => {
    setSavedQuestion();
  };

  console.log(optionList, 'ini option list');

  console.log(newQuestion, 'ini new quest');

  if (savedQuestion) {
    return <Assessment data={newQuestion} />;
  } else {
    return (
      <>
        <TeacherHeader navigate={props.navigation.navigate} />
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={styles.container}>
            <Text style={styles.titlePage}>Question</Text>

            {newQuestion.length > 0
              ? newQuestion.map((value, index) => {
                  console.log(value, 'Ini value wey');

                  return (
                    <View style={styles.inputContainer}>
                      <View style={styles.question}>
                        {/* <Text style={styles.qNumber}>{numberOfQuestion} </Text> */}

                        {value.question === '' || value.saved === false ? (
                          <TextInput
                            multiline
                            numberOfLines={5}
                            style={styles.qTitle}
                            placeholder="Question"
                            onChangeText={(text) => {
                              // let valueBaru = [...newQuestion];
                              // valueBaru[index].question = text;
                              // setNewQuestion(valueBaru);
                              // createQuestionTitle(index, text);
                              const qName = createQuestionTitle(index, text);
                              qName;
                              // 2nd PARAMETER TO ACTION
                            }}
                            // value={value.question}
                            // value={questionTitle}
                          />
                        ) : null}
                      </View>
                      {value.options === '' || value.saved === false ? (
                        <>
                          <Text style={styles.answerTitle}>Answer</Text>
                          <View style={styles.answer}>
                            {(clicked ? optionList : value.options).map(
                              (v, i) => {
                                return (
                                  <View
                                    key={i.toString()}
                                    style={styles.option}>
                                    <CheckBox
                                      disabled={false}
                                      value={v.checked}
                                      onValueChange={(newValue) => {
                                        // let newArray = [];
                                        value.options[i].checked = !v.checked;
                                        // newQuestion[index].answer = i + 1;
                                        // setIndexChecked(i + 1);
                                        setNewQuestion((prevState) => {
                                          prevState[index].answer = i + 1;
                                          return [...prevState];
                                        });

                                        // if (!value.options[i].checked) {
                                        //   value.options[i].checked = true;
                                        // } else {
                                        //   value.options[i].checked = false;
                                        // }
                                      }}
                                    />

                                    <TextInput
                                      multiline
                                      numberOfLines={1}
                                      style={styles.qAnswer}
                                      placeholder={v.placeholder}
                                      onChangeText={(text) => {
                                        let qAnswernya = createAnswer(
                                          text,
                                          index,
                                          i,
                                        );
                                        qAnswernya;
                                        // 3rd PARAMETER
                                      }}
                                      // value={option}
                                      // onBlur={addOption}
                                    />
                                  </View>
                                );
                              },
                            )}
                          </View>
                        </>
                      ) : (
                        <Text>{value.options}</Text>
                      )}
                      {(value.question === '' && value.options === '') ||
                      !value.saved ? (
                        <>
                          <TouchableHighlight
                            style={styles.addOption}
                            onPress={() => {
                              addOption(index);
                            }}>
                            <Text style={styles.textStyle}>
                              Add More Option
                            </Text>
                          </TouchableHighlight>
                        </>
                      ) : null}

                      {value.remark === '' || value.saved === false ? (
                        <>
                          <Text style={styles.remark}>Remark</Text>
                          <TextInput
                            multiline
                            numberOfLines={7}
                            style={styles.remarkInput}
                            placeholder="Explain here..."
                            onChangeText={(text) => {
                              createRemark(index, text);
                            }}
                            // value={remark}
                          />
                        </>
                      ) : null}
                    </View>
                  );
                })
              : null}
            <View>
              <TouchableOpacity style={styles.anQuest} onPress={tambahContent}>
                <Text>Add new question</Text>
              </TouchableOpacity>
            </View>
          </View>

          {/* {(value.question === '' && value.options === '') || !value.saved ? (
          <> */}
          <View style={styles.bottomContainer}>
            <TouchableHighlight
              style={styles.bottomButton}
              onPress={async () => {
                setSavedQuestion(true);
                newQuestion.forEach((value, index) => {
                  props.ActionInputAssesment(
                    value.number,
                    value.question,
                    parseInt(value.answer),
                    value.options,
                    value.remark,
                  );
                });
                // for (let i = 0; i < newQuestion.length; i++) {
                //   await props.ActionInputAssesment(
                //     newQuestion[i].number,
                //     newQuestion[i].question,
                //     parseInt(newQuestion[i].answer),
                //     newQuestion[i].options,
                //     newQuestion[i].remark,
                //   );
                // }
              }}>
              <Text style={styles.saveTxt}>Save Exam</Text>
            </TouchableHighlight>
          </View>
          {/* </>
        ) : null} */}
        </ScrollView>
      </>
    );
  }
};

const mapStateToProps = (state) => ({
  number: state.InputAssesmentReducer.number,
  question: state.InputAssesmentReducer.question,
  answer: state.InputAssesmentReducer.answer,
  //options ???
  // value: state.InputAssesmentReducer.options.value,
  // text: state.InputAssesmentReducer.options.text,
  remark: state.InputAssesmentReducer.options.remark,
  // assessmentlist : state.InputAssesmentReducer.
});

const mapDispatchToProps = {
  ActionInputAssesment,
  actGetAssesment,
};

export default connect(mapStateToProps, mapDispatchToProps)(InputAssesment);
