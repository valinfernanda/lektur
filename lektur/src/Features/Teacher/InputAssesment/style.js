import {StyleSheet} from 'react-native';
//IMPORT STYLES
import {Color} from '../../../Shared/Global/Config/Color';
import {Size} from '../../../Shared/Global/Config/Size';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: Size.wp5,
    backgroundColor: 'white',
  },
  inputContainer: {
    resizeMode: 'contain',
    marginTop: Size.h2,
    borderRadius: Size.ms4,
    padding: Size.wp3,
    elevation: 2,
    // backgroundColor: 'orange',
  },
  question: {
    flexDirection: 'row',
    marginBottom: Size.h5,
  },
  option: {
    flexDirection: 'row',
    marginBottom: Size.h2,
  },
  bottomContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: Size.h5,
    paddingBottom: Size.h19,
  },
  tanya: {
    fontSize: Size.ms16,
    paddingRight: Size.wp4,
    backgroundColor: 'pink',
  },

  //COMPONENT
  titlePage: {
    fontWeight: 'bold',
    fontSize: Size.ms18,
  },
  answerTitle: {
    fontWeight: 'bold',
    fontSize: Size.ms18,
    marginBottom: Size.h2,
  },
  remark: {
    fontWeight: 'bold',
    fontSize: Size.ms18,
    marginTop: Size.h5,
    marginBottom: Size.h2,
  },
  qNumber: {
    fontWeight: 'bold',
    fontSize: Size.ms18,
    marginTop: Size.h1,
    // backgroundColor: 'silver',
    alignSelf: 'flex-start',
  },
  qTitle: {
    flex: 1,
    height: Size.h15,
    backgroundColor: Color.lightSilver,
    padding: Size.wp3,
    marginLeft: Size.wp1,
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    textAlignVertical: 'top',
    borderBottomWidth: Size.ms1,
    borderColor: 'silver',
  },
  remarkInput: {
    flex: 1,
    height: Size.h15,
    backgroundColor: Color.lightSilver,
    padding: Size.wp3,
    marginLeft: Size.wp1,
    marginBottom: Size.h5,
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    textAlignVertical: 'top',
    borderBottomWidth: Size.ms1,
    borderColor: 'silver',
  },
  qAnswer: {
    flex: 1,
    backgroundColor: Color.lightSilver,
    padding: Size.wp3,
    marginLeft: Size.wp2,
    alignItems: 'center',
    justifyContent: 'flex-start',
    textAlignVertical: 'top',
    borderBottomWidth: Size.ms1,
    borderColor: 'silver',
  },
  cStyle: {
    marginTop: Size.h2,
  },
  addOption: {
    // paddingHorizontal: Size.wp7,
    padding: Size.wp3,
    width: Size.wp50,
    borderWidth: Size.ms2,
    borderRadius: Size.ms4,
    borderColor: Color.orange,
    marginTop: Size.h2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textStyle: {
    color: Color.orange,
    fontWeight: 'bold',
    fontSize: Size.ms18,
  },
  anQuest: {
    color: Color.bluelight,
    fontWeight: 'bold',
    fontSize: Size.ms18,
    textDecorationLine: 'underline',
    marginTop: Size.h5,
  },
  bottomButton: {
    padding: Size.wp3,
    width: Size.wp50,
    borderRadius: Size.ms4,
    marginTop: Size.h2,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Color.darkblue,
  },
  saveTxt: {
    fontSize: Size.ms18,
    fontWeight: 'bold',
    color: 'white',
  },
});
