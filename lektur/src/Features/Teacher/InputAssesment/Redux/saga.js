import {all, takeLatest, put} from 'redux-saga/effects';
import axios from 'axios';
// import {navigate} from '../../../Utils/Nav';
import {Store} from '../../../../Store/Store';
// import {navigate} from '../../Utils/Nav';
import {navigate} from '../../../Utils/Nav';

import qs from 'query-string';

function* SagaInputAssesment(payload) {
  try {
    console.log('ini input assesment');
    const id = Store.getState().TeacherAddCourseReducer.id; //maybe need use map?
    const Token = Store.getState().LoginReducer.token;
    //parameter post : url dan body. Bodynya bentuk object
    const body = {
      number: payload.number,
      question: payload.question,
      answer: payload.answer,
      options: payload.options,
    };
    console.log(body, 'ini body');
    console.log(id, 'ini id');
    console.log(Token, 'ini token');

    const Response = yield axios.post(
      `https://lekturapp.herokuapp.com/api/assessment/create?courseId=${id}`,
      body,
      {headers: {Authorization: `Bearer ${Token}`}},
    );

    console.log(Response, 'ini response dari input assessment yuhu');
    yield put({
      type: 'SET_ASSESMENT',
      // number: Response.data.result.number,
      // question: Response.data.result.question,
      // answer: Response.data.result.answer,
      // questionId: Response.data.result._id,
      result: Response.data.result,
    });
    // yield put({
    //   type: 'SET_OPTIONS',
    //   options: [
    //     {
    //       value: payload.value,
    //       text: payload.text,
    //     },
    //   ],
    // });
    // yield put(navigate('Assessment', {}));
    console.log(Response, 'hasil respond Input Assesment');
  } catch (error) {
    console.log(error.response);
  }
}

export function* SagasInputAssesment() {
  //POST Register INI SESUAIIN SAMA ACTION WEY
  yield takeLatest('POST_INPUT_ASSESMENT', SagaInputAssesment);
}
