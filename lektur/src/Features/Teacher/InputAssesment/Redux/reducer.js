const InitialState = {
  number: 0,
  question: '',
  answer: 0,
  options: [{value: 0, text: ''}],
  questionId: '',
  result: [],
};

export const InputAssesmentReducer = (state = InitialState, action) => {
  switch (action.type) {
    case 'SET_ASSESMENT':
      return {
        ...state,
        // number: action.number,
        // question: action.question,
        // answer: action.answer,
        // questionId: action.questionId,
        result: action.result,
      };
    case 'SET_OPTIONS':
      return {
        ...state,
        options: [
          ...state.options,
          {
            // value: nextOptionsValue(state.options),
            // text: action.text,
            value: action.value,
            text: action.text,
          },
        ],
      };
    default:
      return state;
  }
};
