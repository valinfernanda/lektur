import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {Size} from '../../Shared/Global/Config/Size';
import {Color} from '../../Shared/Global/Config/Color';

const TeacherHeader = (props) => {
  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={() => props.navigate('CourseFour')}>
        <Text style={styles.courseTxt}>Course</Text>
      </TouchableOpacity>
      <TouchableOpacity onPress={() => props.navigate('InputAssesment')}>
        {/* Assessment, InputAssesment */}
        <Text style={styles.courseTxt}>Assesment</Text>
      </TouchableOpacity>
      <TouchableOpacity onPress={() => props.navigate('InviteStudents')}>
        <Text style={styles.courseTxt}>Students</Text>
      </TouchableOpacity>
    </View>
  );
};

export default TeacherHeader;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    height: Size.h6,
    backgroundColor: Color.pink,
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  courseTxt: {},
});
