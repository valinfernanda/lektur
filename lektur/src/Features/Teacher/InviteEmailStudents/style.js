import {StyleSheet} from 'react-native';
//IMPORT STYLES
import {Color} from '../../../Shared/Global/Config/Color';
import {Size} from '../../../Shared/Global/Config/Size';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: Size.wp5,
    backgroundColor: Color.lightSilver,
  },
  inputContainer: {
    resizeMode: 'contain',
    borderRadius: Size.ms4,
    padding: Size.wp3,
    paddingVertical: Size.wp4,
    margin: Size.wp5,
    backgroundColor: 'white',
    elevation: 2,
  },
  inputEmail: {
    flexDirection: 'row',
  },
  middleContainer: {
    borderTopWidth: Size.ms1,
    borderColor: 'silver',
    marginTop: Size.h3,
  },
  option: {
    flexDirection: 'row',
    padding: Size.wp5,
    backgroundColor: 'white',
    borderBottomWidth: Size.ms1,
    borderColor: 'silver',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  bottomContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: Size.h5,
    paddingBottom: Size.h5,
  },

  //COMPONENT
  addStudent: {
    fontSize: Size.ms28,
    marginTop: Size.h1,
  },
  inputStudent: {
    flex: 1,
    backgroundColor: Color.lightSilver,
    padding: Size.wp3,
    marginLeft: Size.wp1,
    marginRight: Size.wp7,
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    textAlignVertical: 'top',
    borderBottomWidth: Size.ms1,
    borderColor: 'silver',
  },
  emailList: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start',
    textAlignVertical: 'top',
  },
  xStyle: {
    // marginTop: Size.h3,
    fontSize: Size.ms15,
  },
  textStyle: {
    color: Color.orange,
    fontWeight: 'bold',
    fontSize: Size.ms18,
  },
  bottomButton: {
    padding: Size.wp3,
    width: '90%',
    borderRadius: Size.ms4,
    marginTop: Size.h2,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Color.darkblue,
  },
  saveTxt: {
    fontSize: Size.ms18,
    fontWeight: 'bold',
    color: 'white',
  },
});
