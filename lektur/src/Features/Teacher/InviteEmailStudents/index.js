import React, {useState} from 'react';
import {
  View,
  Text,
  ScrollView,
  TextInput,
  TouchableHighlight,
} from 'react-native';
//import component
import {Vector} from '../../../Assets';
import {Header} from '../../../Components';
import {styles} from './style';
import {ActionInviteEmail} from './Redux/action';
import {connect} from 'react-redux';
import {TouchableOpacity} from 'react-native';

const InputEmailStudent = (props) => {
  const [text, setText] = useState('');
  const [emailStudent, setEmailStudent] = useState([]);

  const addEmail = (email) => {
    setEmailStudent((prevState) => {
      return [...prevState, email];
    });
  };

  const removeEmail = (index) => {
    setEmailStudent((prevState) => {
      prevState.splice(index, 1);
      return [...prevState];
    });
  };

  const inviteStudent = () => {
    props.ActionInviteEmail(emailStudent);
  };

  const renderListEmailInvitation = () => {
    return emailStudent.map((item, index) => {
      return (
        <View key={index.toString()} style={styles.option}>
          <Text
            multiline
            numberOfLines={1}
            style={styles.emailList}
            placeholder="Option"
            onChangeText={(text) => setText(text)}
            defaultValue={text}>
            {item}
          </Text>
          <TouchableOpacity
            onPress={() => {
              removeEmail(index);
            }}>
            <Vector style={styles.xStyle} />
          </TouchableOpacity>
        </View>
      );
    });
  };

  // setTheArray(oldArray => [...oldArray, newElement]);

  return (
    <>
      <Header
        title="Invite Student"
        onPress={() => props.navigation.goBack()}
      />

      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.container}>
          {/* email input area */}
          <View style={styles.inputContainer}>
            <View style={styles.inputEmail}>
              <TextInput
                multiline
                numberOfLines={1}
                style={styles.inputStudent}
                placeholder="Type student email"
                onChangeText={(text) => setText(text)}
                defaultValue={text}
              />
              <TouchableOpacity
                onPress={() => {
                  addEmail(text);
                }}>
                <Text style={styles.addStudent}>+</Text>
              </TouchableOpacity>
            </View>
          </View>
          {/* end of email input */}
          <View style={styles.middleContainer}>
            {renderListEmailInvitation()}
          </View>
        </View>
        <View style={styles.bottomContainer}>
          <TouchableHighlight
            style={styles.bottomButton}
            onPress={() => {
              inviteStudent();
            }}>
            <Text style={styles.saveTxt}>Invite</Text>
          </TouchableHighlight>
        </View>
      </ScrollView>
    </>
  );
};

const mapStateToProps = (state) => ({});

const mapDispatchToProps = {
  ActionInviteEmail,
};

export default connect(mapStateToProps, mapDispatchToProps)(InputEmailStudent);
