export const ActionInviteEmail = (email) => {
  return {
    type: 'POST_EMAIL_STUDENT',
    email,
  };
};
