import {all, takeLatest, put} from 'redux-saga/effects';
import axios from 'axios';
// import {navigate} from '../../../Utils/Nav';
import {Store} from '../../../../Store/Store';
// import {navigate} from '../../Utils/Nav';
import {navigate} from '../../../Utils/Nav';

import qs from 'query-string';

function* SagaInviteEmail(payload) {
  try {
    console.log('ini input assesment');
    const id = Store.getState().TeacherAddCourseReducer.id;
    const Token = Store.getState().LoginReducer.token;
    const body = {
      email: payload.email,
    };
    console.log(payload, 'ini isi payload');
    console.log(Token, 'ini isi token');

    const Response = yield axios.post(
      `https://lekturapp.herokuapp.com/api/teacher/courses/invite?courseId=${id}`,
      body,
      {headers: {Authorization: `Bearer ${Token}`}},
    );

    console.log(Response, 'ini response dari invite email student');
    yield put({
      type: 'SET_INVITE_EMAIL',
      email: Response.data.result,
    });
    yield put(navigate('InviteStudents', {}));
  } catch (error) {
    console.log(error.response);
  }
}

export function* SagasInviteEmail() {
  //POST Register INI SESUAIIN SAMA ACTION WEY
  yield takeLatest('POST_EMAIL_STUDENT', SagaInviteEmail);
}
