const InitialState = {
  email: '',
};

export const InviteEmailReducer = (state = InitialState, action) => {
  switch (action.type) {
    case 'SET_INVITE_EMAIL':
      return {
        ...state,
        email: action.email,
      };

    default:
      return state;
  }
};
