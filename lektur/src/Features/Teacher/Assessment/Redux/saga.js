import {takeLatest, put} from 'redux-saga/effects';
import axios from 'axios';
import {Store} from '../../../../Store/Store';
import {navigate} from '../../../Utils/Nav';

function* SagaTeacherAssesment(payload) {
  try {
    const id = Store.getState().TeacherAddCourseReducer.id;
    const Token = Store.getState().LoginReducer.token;
    console.log('INI ID NYA WOY', id);
    const Response = yield axios.get(
      `https://lekturapp.herokuapp.com/api/assessment/?courseId=${id}`,
      {
        //put the token into auth bearer
        headers: {
          Authorization: `Bearer ${Token}`,
        },
      },
    );
    console.log('INI RESPON TEACHER ASSESMENT', Response);

    yield put({
      type: 'SET_TEACHER_ASSESMENT',
      // options: Response.data.result.options,
      // listAssesment: Response.data.result,
      listAssesment: Response.data.result,
      //   number: Response.data.result.number,
      //   question: Response.data.result.question,
      //   id: Response.data.result._id,
      //   courseId: Response.data.result.courseId,
      //   answer: Response.data.result.answer,
      // text: Response.data.result.options.text,
      // hasil: Response.data.result,
    });
  } catch (error) {
    console.log(error);
  }
}

export function* SagasTeacherAssesment() {
  yield takeLatest('GET_TEACHER_ASSESMENT', SagaTeacherAssesment);

  //make sure 1st statement match with action, 2nd match with saga function name
}
