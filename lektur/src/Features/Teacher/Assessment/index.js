import React, {useEffect} from 'react';
import {ScrollView} from 'react-native-gesture-handler';
import {View, Text, StyleSheet, FlatList} from 'react-native';
import {CheckBox} from 'react-native-elements';
import {Gap, Header, Option} from '../../../Components';
import TeacherHeader from '../teacherHeader';
import {Size} from '../../../Shared/Global/Config/Size';
import {ActionTeacherAssesment} from './Redux/action';
import {connect} from 'react-redux';

const Assessment = (props) => {
  useEffect(() => {
    props.ActionTeacherAssesment();
    console.log(props.listAssesment, 'woy ini hasil get assessmentnya ');
  }, []);
  // props.listAssesment.options.map((value) => console.log(value));
  console.log(props.ActionTeacherAssesment, 'ini list teacher assessment');
  return (
    <>
      <Header title="New Course" onPress={() => props.navigation.goBack()} />
      <TeacherHeader />
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.bungkus}>
          <Gap height={19} />

          <View style={styles.atas}>
            <Text style={styles.soal}>Questions </Text>
            {/* <PencilEdit style={styles.pensil} /> */}
            {/* <Pencil /> */}
          </View>
          <Gap height={8} />
          <FlatList
            data={props.listAssesment}
            inverted={false}
            renderItem={({item}) => {
              return (
                <View style={styles.container}>
                  <View style={styles.pertanyaan}>
                    <Text>{item.number}</Text>
                    <Gap width={7} />
                    <View>
                      <Text style={styles.tanya}>{item.question}</Text>
                    </View>
                  </View>
                  <View style={styles.jawaban}>
                    {item.options.map((value) =>
                      value.value === item.answer ? (
                        <CheckBox checked={true} title={value.text} />
                      ) : (
                        <CheckBox title={value.text} />
                      ),
                    )}

                    {/* {item.options.map((value) => {
                    // for (let i = 0; i <= value.length; i++) {
                    //   if (i === item.answer) {
                    //     <CheckBox checked={true} title={value.text} />
                    //   } else {
                    //     <CheckBox title={value.text} />
                    //   }
                    // }
                    if (value.value === item.answer) {
                      <CheckBox checked={true} title={value.text} />;
                    } else {
                      <CheckBox title={value.text} />;
                    }
                  })} */}
                  </View>
                  <Gap height={20} />
                </View>
              );
            }}
          />
        </View>
      </ScrollView>
    </>
  );
};

const mapStateToProps = (state) => ({
  listAssesment: state.TeacherAssessmentReducer.listAssesment,
});

const mapDispatchToProps = {
  ActionTeacherAssesment,
};

export default connect(mapStateToProps, mapDispatchToProps)(Assessment);

// STYLE AREA
const styles = StyleSheet.create({
  soal: {
    fontSize: Size.ms20,
    fontWeight: 'bold',
  },
  atas: {
    flexDirection: 'row',
  },

  jawaban: {
    paddingLeft: Size.ms11,
  },
  // pensil: {
  //   width: 500,
  //   height: 50,
  // },
  pertanyaan: {flexDirection: 'row'},
  bungkus: {
    paddingLeft: Size.wp7,
    paddingRight: Size.wp7,
  },
  tanya: {
    fontSize: Size.ms14,
    paddingRight: Size.wp7,
  },
});
