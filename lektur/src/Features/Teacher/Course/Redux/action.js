export const GetCourseById = (id) => {
  return {
    type: 'GET_COURSE',
    id,
  };
};

export const GetIdForInvtStudent = (idGetStudent) => {
  return {
    type: 'GET_STUDENT',
    idGetStudent,
  };
};

export const ActionGetCourse = () => {
  return {
    type: 'GET_TEACHER_COURSE',
  };
};

export const SetCoursesById = (data) => {
  return {
    type: 'SET_COURSE_BY_ID',
    data,
  };
};
