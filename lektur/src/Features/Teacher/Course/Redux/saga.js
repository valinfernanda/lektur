import {all, takeLatest, put} from 'redux-saga/effects';
import axios from 'axios';
// import {navigate} from '../../../Utils/Nav';
import {Store} from '../../../../Store/Store';
// import {navigate} from '../../Utils/Nav';
import {navigate} from '../../../Utils/Nav';
import {ActionGetCourse, SetCoursesById} from './action';
import {GetIdForInvtStudent} from '../../InviteStudents/redux/action';

// import qs from 'query-string';

function* SagaTeacherCourse(payload) {
  try {
    const Token = Store.getState().LoginReducer.token;
    console.log(payload);
    //parameter post : url dan body. Bodynya bentuk object
    console.log(Token, 'ini token');
    const Response = yield axios.get(
      `https://lekturapp.herokuapp.com/api/teacher/profile`,
      {
        headers: {
          Authorization: `Bearer ${Token}`,
        },
      },
    );

    console.log('ini response yang teacher course', Response);

    yield put({
      type: 'SET_TEACHER_COURSE',
      // title: Response.data.result.result.title,
      // overview: Response.data.result.result.overview,
      listCourse: Response.data.result.dataCourse,
    });
  } catch (error) {
    console.log(error);
  }
}

function* SagaGetCourseById(payload) {
  try {
    const Response = yield axios.get(
      `https://lekturapp.herokuapp.com/api/courses/detail?courseId=${payload.id}`,
      {validateStatus: (status) => status < 500},
    );
    console.log(Response, 'ini get by ID');
    yield put(SetCoursesById(Response.data.result.course));
    yield put(navigate('AddCourseEdit', {}));
  } catch (error) {
    console.log(error);
  }
}

// CHAEDIR WORK AREA
// this saga to invite student so response will be put on InviteStudents folder reducer
function* SagaGetStudentByCourseId(payload) {
  try {
    const Token = Store.getState().LoginReducer.token;
    console.log(payload);
    console.log(Token, 'ini token');
    const Response = yield axios.get(
      `https://lekturapp.herokuapp.com/api/teacher/courses/student?courseId=${payload.idGetStudent}`,
      {
        headers: {
          Authorization: `Bearer ${Token}`,
        },
      },
      {validateStatus: (status) => status < 500},
    );
    console.log(Response, 'ini get by ID FOR INVT STUDENT');
    //yield put(SetStudentDataById(Response.data.result));
    yield put({
      type: 'SET_STUDENT_ID',
      dataEnrolledStudent: Response.data.result,
    });

    //ini akan tersimpan ke action InviteStudents folder
    yield put({
      type: 'SET_COURSE_ID',
      CourseID: payload.idGetStudent,
    });

    yield put(navigate('InviteStudents', {}));
  } catch (error) {
    console.log(error);
  }
}

export function* SagasTeacherCourse() {
  // yield takeLatest('GET_COURSE', SagaTeacherCourse);
  yield takeLatest('GET_TEACHER_COURSE', SagaTeacherCourse);
  yield takeLatest('GET_COURSE', SagaGetCourseById);
  yield takeLatest('GET_STUDENT', SagaGetStudentByCourseId);
}
