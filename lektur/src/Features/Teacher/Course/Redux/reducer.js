const InitialState = {
  listCourse: [],
  courseDetail: {},
};

export const TeacherCourseReducer = (state = InitialState, action) => {
  switch (action.type) {
    case 'SET_TEACHER_COURSE':
      return {
        ...state,
        listCourse: action.listCourse,
      };
    case 'SET_COURSE_BY_ID':
      return {
        ...state,
        courseDetail: action.data,
      };
    default:
      return state;
  }
};
