import React, {useEffect} from 'react';
import {
  StyleSheet,
  View,
  Text,
  ScrollView,
  TouchableOpacity,
  TouchableHighlight,
  Image,
  FlatList,
} from 'react-native';
import {SearchBar} from 'react-native-elements';
import FastImage from 'react-native-fast-image';
import {Size} from '../../../Shared/Global/Config/Size';
import {Color} from '../../../Shared/Global/Config/Color';
import {
  GetCourseById,
  ActionGetCourse,
  GetIdForInvtStudent,
} from './Redux/action';
import {connect} from 'react-redux';
import {Course} from '../../index';
import {Gap} from '../../../Components';
import {ActionGetContentbyCourseId} from '../AddCourseEdit/Redux/action';

const TeacherCourse = (props) => {
  useEffect(() => {
    props.ActionGetCourse();
    props.GetIdForInvtStudent();
    props.ActionGetContentbyCourseId();
    console.log(props.ActionTeacherCourse, 'ini teacher course');
    // console.log(props.ActionGetCourse, 'ini get course');
  }, []);
  console.log(props.listCourse);
  if (props.teacher) {
    return (
      <>
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={styles.container}>
            <TouchableOpacity
              style={styles.newCourseButton}
              onPress={() => props.navigation.navigate('AddCourse')}>
              <Text style={styles.textStyle}>New Course</Text>
            </TouchableOpacity>

            <FlatList
              data={props.listCourse}
              renderItem={({item}) => {
                const idGetStudent = item._id;
                return (
                  <View style={styles.topContainerCover}>
                    <View style={styles.topContainerBottom}>
                      <View style={styles.courseImageContainer}>
                        <FastImage
                          style={styles.coursePic}
                          source={{uri: item.image}}
                        />
                      </View>
                      <View style={styles.courseDesc}>
                        <View style={styles.courseDescTop}>
                          <Text style={styles.courseDescTop1}>
                            {item.title}
                          </Text>
                          {/* <Gap height={10} /> */}
                          {/* <Text style={styles.courseDescTop2}>
                          {item.overview}
                        </Text> */}
                          <Text style={styles.courseDescTop3}>
                            {item.totalVideo} Videos | {item.totalMaterial}{' '}
                            Lesson Materials
                          </Text>
                          <Text style={styles.courseDescTop3}>
                            {item.totalEnrolled} Students enrolled
                          </Text>

                          <Gap height={10} />
                        </View>
                        <View style={styles.coursebuttonContainer}>
                          <TouchableHighlight
                            style={styles.editButton}
                            onPress={() => {
                              console.log(item._id, 'Hanna');
                              props.GetCourseById(item._id);
                            }}>
                            <Text style={styles.textStyle}>EDIT</Text>
                          </TouchableHighlight>

                          <TouchableHighlight
                            style={styles.inviteButton}
                            onPress={() =>
                              // props.navigation.navigate('InviteStudents')
                              {
                                console.log(idGetStudent, 'YO');
                                props.GetIdForInvtStudent(idGetStudent);
                              }
                            }>
                            <Text style={styles.textStyle}>INVITE</Text>
                          </TouchableHighlight>
                        </View>
                      </View>
                    </View>
                  </View>
                );
              }}
            />
          </View>
        </ScrollView>
      </>
    );
  } else {
    return <Course {...props} />;
  }
};
const mapStateToProps = (state) => ({
  teacher: state.LoginReducer.role,
  listCourse: state.TeacherCourseReducer.listCourse,
});

const mapDispatchToProps = {
  GetCourseById,
  ActionGetCourse,
  GetIdForInvtStudent,
  ActionGetContentbyCourseId,
};

export default connect(mapStateToProps, mapDispatchToProps)(TeacherCourse);

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-evenly',
    flexDirection: 'column',
    padding: 20,
  },

  newCourseButton: {
    backgroundColor: Color.oxfordBlue,
    padding: Size.h1,
    width: Size.wp92,
    borderRadius: Size.ms3,
    elevation: 2,
    marginBottom: 20,
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },

  topContainerCover: {
    flex: 2,
    height: Size.h50,
    width: Size.wp92,
    marginBottom: 20,
  },

  topContainerBottom: {
    flex: 3,
  },

  courseImageContainer: {
    backgroundColor: 'black',
    flex: 4,
    elevation: 5,
  },
  courseDesc: {
    backgroundColor: 'white',
    flex: 3,
    elevation: 5,
  },
  courseDescTop: {
    flex: 1,
    justifyContent: 'space-between',
    padding: Size.wp3,
    // backgroundColor: 'blue',
  },

  courseDescTop1: {
    fontWeight: 'bold',
    fontSize: Size.ms20,
    color: Color.black,
  },
  courseDescTop2: {
    fontWeight: 'normal',
    fontSize: Size.ms12,
    color: Color.black,
  },
  courseDescTop3: {
    fontSize: Size.ms14,
    color: Color.black,
  },

  coursebuttonContainer: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-evenly',
  },

  editButton: {
    backgroundColor: Color.orange,
    padding: Size.h1,
    width: Size.wp30,
    borderRadius: Size.ms3,
    elevation: 2,
  },

  inviteButton: {
    backgroundColor: Color.orange,
    padding: Size.h1,
    width: Size.wp30,
    borderRadius: Size.ms3,
    elevation: 2,
  },

  coursePic: {
    width: Size.wp92,
    height: Size.h32,
  },
});
