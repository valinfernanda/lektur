import React from 'react';
import {
  ImageBackground,
  View,
  Text,
  ScrollView,
  TouchableHighlight,
} from 'react-native';

//STYLE
import {styles} from './style';
import {Rectangle4} from '../../../Assets';

//COMPONENT
import Read from './Read';
import Container from './Container';
import {Button, Gap, Header, Link} from '../../../Components';
import TeacherHeader from '../teacherHeader';
import {connect} from 'react-redux';
import {ActionGetContentbyCourseId} from '../../Teacher/AddCourseEdit/Redux/action';
// import CourseDetail from '../../Home';

const CourseFour = (props) => {
  return (
    <>
      <Header title="New Course" onPress={() => props.navigation.goBack()} />
      <View style={{flex: 1, justifyContent: 'space-between'}}>
        <TeacherHeader navigate={props.navigation.navigate} />
        <ScrollView
          showsVerticalScrollIndicator={false}
          style={styles.scrollView}>
          <View style={styles.container}>
            {/* TOP CONTAINER AREA */}
            <ImageBackground source={Rectangle4} style={styles.topContainer}>
              <View style={styles.topContainerTop}>
                <View style={styles.videoDesc}>
                  <View style={styles.videoDescTop}>
                    <Text style={styles.videoDescTop2}>
                      {props.course.title}
                    </Text>
                  </View>
                </View>
              </View>
            </ImageBackground>
            {/* MIDDLE AREA */}
            <View style={styles.topContainerCover}>
              <View style={styles.topContainerBottom}>
                <View style={styles.courseDetil}>
                  <View style={styles.courseDetilDown}>
                    {/* <Text style={styles.courseDetilDownTxt}>
                </Text> */}
                    <Read text={props.course.overview} />
                  </View>
                </View>
              </View>
            </View>
            <View>
              <Text style={styles.judul}>Content*</Text>
            </View>
            <View style={styles.contain}>
              <Container />
              <Gap height={30} />
            </View>
            <View style={styles.contain}>
              <Container />
              <Gap height={30} />
            </View>

            <View style={styles.bawah}>
              <Button title="Publish" />
              <Gap height={10} />
              <Link title="Delete Course" />
              <Gap height={50} />
            </View>
          </View>
        </ScrollView>
      </View>
    </>
  );
};

const mapStateToProps = (state) => ({
  // getContentbyId : state.TeacherCourseReducer.
  course: state.TeacherCourseReducer.courseDetail,
  //   content: state.TeacherEdit.
  // desccontent: state.TeacherEdit.editContent[0].description,
  titlecontent: state.TeacherEdit.editContent[0].title,
  image: state.TeacherEdit.editContent[0].thumbnail,
});

const mapDispatchToProps = {
  ActionGetContentbyCourseId,
};

export default connect(mapStateToProps, mapDispatchToProps)(CourseFour);
