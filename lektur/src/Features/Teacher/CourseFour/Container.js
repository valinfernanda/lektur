import React from 'react';
import {View, Text, StyleSheet, Image} from 'react-native';
import {Dokumen, DummyLaptop} from '../../../Assets';
import {Gap, Link} from '../../../Components';

const Container = () => {
  return (
    <View style={styles.bungkus}>
      <Text style={styles.judul}>Lesson #1:What is React</Text>
      <Gap height={20} />
      <View style={styles.letakGambar}>
        <Image source={DummyLaptop} style={styles.gambar} />
      </View>
      <Gap height={15} />
      <View style={styles.link}>
        <Dokumen />
        <Gap width={14} />
        <Link title="jhdajabdjhbas" />
      </View>
      <Gap height={15} />
      <View style={styles.link}>
        <Gap height={15} />
        <Dokumen />
        <Gap width={14} />
        <Link title="jhdajabdjhbas" />
        <Gap height={60} />
      </View>
    </View>
  );
};

export default Container;

const styles = StyleSheet.create({
  bungkus: {
    width: 324,
    backgroundColor: 'white',
    elevation: 5,
  },
  link: {
    paddingLeft: 17,
    flexDirection: 'row',
  },
  judul: {
    fontWeight: 'bold',
    paddingTop: 24,
    paddingLeft: 17,
    fontSize: 18,
  },
  gambar: {
    borderRadius: 2,
    width: 296,
    height: 202,
  },
  letakGambar: {
    alignSelf: 'center',
  },
});
