import React, {useState, useEffect} from 'react';
import {View, Text, StyleSheet, Image, TouchableOpacity} from 'react-native';
import {Size} from '../../Shared/Global/Config/Size';
import {Color} from '../../Shared/Global/Config/Color';
import {moderateScale} from 'react-native-size-matters';
import {PencilEdit} from '../../Assets';
import {GetProfile} from './EditProfile/Redux/action';
import {connect} from 'react-redux';

const Profile = (props) => {
  const {fullname} = props;
  const {email} = props;
  const {photo} = props;
  useEffect(() => {
    props.GetProfile(props.token);
  });

  return (
    <View style={styles.container}>
      <View style={styles.detailPagecontainer}>
        {/* <View> */}
        <Image style={styles.profilePic} source={{uri: props.photo}} />
        {/* <PencilEdit style={styles.pensil} /> */}

        {/* </View> */}

        <Text style={styles.userName}>{fullname}</Text>

        <Text style={styles.userEmail}>{email}</Text>

        <TouchableOpacity
          style={styles.editProfileButton}
          onPress={() => props.navigation.navigate('EditProfile')}>
          <Text style={styles.editProfileText}>Edit Profile</Text>
        </TouchableOpacity>
      </View>

      <TouchableOpacity style={styles.logoutButton} onPress={() => {}}>
        <Text style={styles.logoutText}>LogOut</Text>
      </TouchableOpacity>
    </View>
  );
};
const mapStateToProps = (state) => ({
  fullname: state.EditProfileReducer.fullname,
  email: state.EditProfileReducer.email,
  token: state.LoginReducer.token,
  photo: state.EditProfileReducer.photo,
});

const mapDispatchToProps = {
  GetProfile,
};

export default connect(mapStateToProps, mapDispatchToProps)(Profile);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  pensil: {
    position: 'absolute',
    bottom: 160,
    right: 90,
  },

  detailPagecontainer: {
    position: 'absolute',
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    width: 279,
    height: 322,
    // borderWidth: 2,
    borderRadius: 3,
    // borderColor: '#ddd',
    // borderBottomWidth: 0,
    // shadowColor: '#000',
    // shadowOffset: {width: 0, height: 1},
    // shadowOpacity: 1,
    // shadowRadius: 2,
    elevation: 2,
    top: 120,
  },

  profilePic: {
    position: 'absolute',
    width: 140,
    height: 140,
    flex: 1,
    top: 20,
    borderRadius: 140 / 2,
  },

  userName: {
    textAlign: 'center',
    top: 40,
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: 14,
    lineHeight: 24,
    /* identical to box height, or 171% */

    color: '#000000',
  },

  userEmail: {
    textAlign: 'center',
    top: 45,
    fontStyle: 'normal',
  },

  editProfileButton: {
    textAlign: 'center',
    top: 80,
  },

  editProfileText: {
    fontStyle: 'normal',
    textDecorationLine: 'underline',
    color: Color.bluelight,
  },

  logoutButton: {
    top: 250,
  },

  logoutText: {
    fontSize: 20,
    fontStyle: 'normal',
    textDecorationLine: 'underline',
    color: Color.red,
  },
});
