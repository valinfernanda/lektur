import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  ImageBackground,
  StyleSheet,
  TouchableOpacity,
  Image,
} from 'react-native';
import jwt from 'jwt-decode';
import {BGClassroom, DummyLaptop, ILNullPhoto} from '../../../Assets';
import Link from '../../../Components/atoms/Link';
import Gap from '../../../Components/atoms/Gap';
import Input from '../../../Components/atoms/Input';
import Button from '../../../Components/atoms/Button';
import {Header} from '../../../Components/molecules';
import {GetProfile, updateProfile} from './Redux/action';
import {connect} from 'react-redux';
import {Size} from '../../../Shared/Global/Config/Size';
import {PencilEdit} from '../../../Assets';
// import {TouchableOpacity} from 'react-native-gesture-handler';
// import ImagePicker from 'react-native-image-picker';
// import {launchImageLibrary} from 'react-native-image-picker';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import {gantiPhoto} from './Redux/action';
import FastImage from 'react-native-fast-image';

const EditProfile = (props) => {
  const [fullname, setFullname] = useState(props.fullname);
  const [email, setEmail] = useState(props.email);
  const [imagePicker, setImagePicker] = useState({});
  const [photo, setPhoto] = useState(props.photo);

  const getImage = () => {
    launchImageLibrary(
      {
        mediaType: 'photo',
        title: 'Select Image',
        // customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
        storageOptions: {
          skipBackup: true,
          path: 'images',
        },
        allowsEditing: true,
      },
      async (response) => {
        setImagePicker(response);
        console.log(response, 'response: ');
        const source = response.uri;
        // const source = {uri: props.photo};
        setPhoto(source);
      },
    );
  };
  console.log(photo, 'ini photo wey');

  console.log(imagePicker, 'set photo wey');

  return (
    <ImageBackground source={BGClassroom} style={styles.page}>
      <Header title="Edit Profile" />

      <View style={styles.kotak}>
        <TouchableOpacity onPress={getImage}>
          <View style={styles.gpicts}>
            <Image
              style={styles.profilePic}
              source={{
                uri: `photo.length`
                  ? photo
                  : 'http://assets0.prcdn.com/uk/people/default-profile.png?1406639312',
              }}
            />
            <Gap width={1} />
            <PencilEdit style={styles.pensil} />
          </View>
        </TouchableOpacity>

        <Gap height={20} />
        <Input
          label="Username*"
          onChangeText={(text) => setFullname(text)}
          value={fullname}
        />
        <Gap height={20} />
        <Input
          label="Email*"
          onChangeText={(text) => setEmail(text)}
          value={email}
        />
        <Gap height={20} />

        {/* <Input
          label="Password*"
          onChangeText={(text) => setPassword(text)}
          value={password}
        /> */}
        <Gap height={5} />
        <View style={styles.jarak}>
          <View style={styles.press}>
            <Button
              title="SAVE"
              onPress={() => {
                props.updateProfile(fullname, email);
                // props.GetProfile(props.token);
                props.gantiPhoto(imagePicker);
              }}
            />
          </View>
        </View>
      </View>
    </ImageBackground>
  );
};
//get
const mapStateToProps = (state) => ({
  fullname: state.EditProfileReducer.fullname,
  email: state.EditProfileReducer.email,
  token: state.LoginReducer.token,
  photo: state.EditProfileReducer.photo,
});

const mapDispatchToProps = {
  // GetProfile,
  updateProfile,
  gantiPhoto,
};

export default connect(mapStateToProps, mapDispatchToProps)(EditProfile);

const styles = StyleSheet.create({
  page: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'blue',
    alignItems: 'center',
  },
  press: {
    flexDirection: 'row',
    alignSelf: 'flex-end',
    paddingTop: Size.wp3,
  },
  kotak: {
    backgroundColor: 'white',
    width: Size.wp82,
    height: Size.h80,
    paddingVertical: Size.wp3,
    alignSelf: 'center',
    paddingLeft: Size.wp1,
    marginTop: Size.wp10,
    alignItems: 'center',
  },
  welcome: {
    fontSize: Size.ms24,
    fontWeight: 'bold',
  },
  gpicts: {
    flexDirection: 'row',
    paddingTop: Size.h10,
  },
  underLogin: {
    fontSize: Size.ms16,
  },
  jarak: {
    paddingRight: Size.wp7,
  },
  choose: {
    flexDirection: 'row',
    justifyContent: 'center',
    paddingTop: Size.wp9,
    paddingBottom: Size.wp5,
  },

  profilePic: {
    width: 140,
    height: 140,
    borderRadius: 140 / 2,
  },
});
