import {ILNullPhoto} from '../../../../Assets';

const InitialState = {
  fullname: '',
  email: '',
  photo: null,
};

export const EditProfileReducer = (state = InitialState, action) => {
  switch (action.type) {
    case 'SET_EDIT_PROFILE':
      return {
        ...state,
        fullname: action.fullname,
        email: action.email,
      };
    case 'SET_EDIT_PHOTO_PROFILE':
      return {
        ...state,
        photo: action.photo,
      };
    // case 'LOGOUT':
    //   return {
    //     ...state,
    //     fullname: '',
    //   };
    default:
      return state;
  }
};
