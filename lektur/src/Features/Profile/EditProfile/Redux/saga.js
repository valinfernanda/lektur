import {all, takeLatest, put, take, takeEvery} from 'redux-saga/effects';
import axios from 'axios';

import {Store} from '../../../../Store/Store';
import {Alert} from 'react-native';

import {navigate} from '../../../Utils/Nav';
import {setPhoto, updateProfilebyrole} from './action';

// import qs from 'query-string';

function* SagaEditProfile(payload) {
  try {
    console.log(payload);

    const Response = yield axios.get(
      `https://lekturapp.herokuapp.com/api/users/profile`,
      {
        //di bawah ini buat get data header di token
        headers: {
          Authorization: `Bearer ${payload.token}`,
        },
      },
      // {validateStatus: (status) => status < 500},
    );
    console.log('ini response', Response);

    yield put({
      type: 'SET_EDIT_PROFILE',
      fullname: Response.data.result.fullname, //pastikan di lognya bntuknya gmn
      email: Response.data.result.email,
    });
    // yield put(navigate('Login', {}));
    console.log(Response, 'hasil data get profile');
  } catch (error) {
    console.log(error);
    // console.log(JSON.stringify(error));
  }
}

function* SagaUploadPhoto(payload) {
  try {
    const token = Store.getState().LoginReducer.token;
    console.log(payload);
    //formData liat dari struktur APInya (form-data)
    //body di append, dan dia terima 2 params.

    const formData = new FormData();
    formData.append('file', {
      uri: payload.imagePicker.uri,
      name: payload.imagePicker.fileName,
      type: payload.imagePicker.type,
    });

    console.log(formData, 'formdata update profile');

    const Response = yield axios({
      method: 'put',
      url: 'https://lekturapp.herokuapp.com/api/users/update/image',
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'multipart/form-data',
        Accept: 'application/json',
      },
      data: formData,
      validateStatus: (status) => status < 500,
    });

    console.log('INI RESPONSE UPDATE PROFILE', Response);
    if (Response.data.success) {
      yield put({
        type: 'SET_EDIT_PHOTO_PROFILE',
        photo: Response.data.result.Location,
      });
      yield put(updateProfilebyrole());
      yield put(navigate('Profile', {}));
    } else {
      Alert.alert(Response.data.message);
    }
    //dibawah buat si action
  } catch (error) {
    console.log(error, 'ini error dari upload photo');
  }
}

function* SagaGetPhotoProfile(payload) {
  try {
    const token = Store.getState().LoginReducer.token;
    const role = Store.getState().RegisterReducer.role;
    console.log(payload);
    //formData liat dari struktur APInya (form-data)
    //body di append, dan dia terima 2 params.

    const endpoint = role === 1 ? 'api/teacher/profile' : 'api/student/profile';

    const Response = yield axios({
      method: 'get',
      url: `https://lekturapp.herokuapp.com/${endpoint}`,
      headers: {
        Authorization: `Bearer ${token}`,
      },
      validateStatus: (status) => status < 500,
    });

    console.log('INI RESPONSE UPDATE PROFILE', Response);
    //dibawah buat si action
    if (Response.data.success) {
      yield put({
        type: 'SET_EDIT_PHOTO_PROFILE',
        photo: Response.data.result.dataUser.image,
      });
    } else {
      Alert.alert(Response.data.message);
    }
  } catch (error) {
    console.log(error, 'ini error dari upload photo');
  }
}
export function* SagasEditProfile() {
  //GET_PROFILE INI SESUAIIN SAMA ACTION WEY
  yield takeLatest('GET_PROFILE', SagaEditProfile);
  yield takeEvery('UPDATE_PHOTO_PROFILE', SagaUploadPhoto);
  yield takeEvery('GET_PHOTO_PROFILE', SagaGetPhotoProfile);
}
