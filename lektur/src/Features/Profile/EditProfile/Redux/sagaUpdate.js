import {all, takeLatest, put, takeEvery} from 'redux-saga/effects';
import axios from 'axios';
import {ActionLogin} from '../../../Auth/Login/Redux/action';
import {Store} from '../../../../Store/Store';
import {navigate} from '../../../Utils/Nav';
import {gantiPhoto} from './action';
// import qs from 'query-string';

function* SagaUpdateProfile(payload) {
  try {
    const token = Store.getState().LoginReducer.token;
    const password = Store.getState().LoginReducer.password;
    //parameter post : url dan body. Bodynya bentuk object
    console.log(payload);
    const body = {
      fullname: payload.fullname,
      email: payload.email,
    };
    console.log(token, 'ini token'), console.log(body, 'ini body');
    const Response = yield axios({
      method: 'put',
      url: 'https://lekturapp.herokuapp.com/api/users/update',
      data: body,
      headers: {
        Authorization: `Bearer ${token}`,
      },
      validateStatus: (status) => status < 500,
    });
    console.log(Response);
    console.log(payload, 'INI IMAGE PICKER');
    // yield put(gantiPhoto(payload.imagePicker));
    yield put(ActionLogin(Response.data.result.email, password));

    yield put(navigate('Profile', {}));
    // const Response = yield axios.put(
    //   `https://lekturapp.herokuapp.com/api/users/update`,
    //   {
    //     headers: {
    //       Authorization: `Bearer ${token}`,
    //       'Content-Type': 'application/x-www-form-urlencoded',
    //     },
    //   },
    //   body,
    //   {validateStatus: (status) => status < 500},
    // );
    console.log('ini response', Response);

    // yield put({
    //   type: 'SET_EDIT_PROFILE',
    //   fullname: Response.data.fullname, //pastikan di lognya bntuknya gmn
    //   email: Response.data.email,
    // });
    // yield put(navigate('Profile', {}));
  } catch (error) {
    console.log(error);
  }
}

export function* SagasUpdateProfile() {
  //GET_PROFILE INI SESUAIIN SAMA ACTION WEY
  yield takeEvery('UPDATE_PROFILE', SagaUpdateProfile);
}
//panggil saga updateprofile, daftarin ke wathcer, panggil action di indexnya
