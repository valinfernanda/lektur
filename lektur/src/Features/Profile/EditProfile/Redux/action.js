export const GetProfile = (token) => {
  return {
    type: 'GET_PROFILE',
    token,
  };
};

export const updateProfile = (fullname, email) => {
  return {
    type: 'UPDATE_PROFILE',
    fullname,
    email,
  };
};

//untuk trigger saga
export const gantiPhoto = (imagePicker) => {
  return {
    type: 'UPDATE_PHOTO_PROFILE',
    imagePicker,
  };
};

export const updateProfilebyrole = () => {
  return {
    type: 'GET_PHOTO_PROFILE',
  };
};

//untuk trigger reducer
export const setPhoto = (data) => {
  return {
    type: 'SET_EDIT_PHOTO_PROFILE',
    data,
  };
};
