import Login from './Auth/Login';
import Register from './Auth/Register';
import Home from './Home';
import Assignment from './Student/Assignment';
import Profile from './Profile';
import CourseContent from './Student/CourseContent';
import CourseDetail from './CourseDetail';
import CoursePlay from './Student/CoursePlay';
import GetStarted from './GetStarted';
import LoginTeacher from './Auth/LoginTeacher';
import RegisterTeacher from './Auth/RegisterTeacher';
import InputAssesment from './Teacher/InputAssesment';
import CourseThree from './Teacher/CourseThree';
import CourseFour from './Teacher/CourseFour';
import Course from './Student/StudentCourses';
import CourseMaterial from './Student/CourseMaterial';
import Assessment from './Teacher/Assessment';
import InputEmailStudent from './Teacher/InviteEmailStudents';
import TeacherCourse from '../Features/Teacher/Course';
import AssignmentStudent from './Student/Assignment/AssignmentStudent';
import AssignmentStudentScore from './Student/Assignment/AssignmentStudentScore';
import SearchScreen from './SearchScreen/index';

//testing area
import Testingcuy from './Testing/Testing';

export {
  Login,
  Register,
  TeacherCourse,
  Home,
  CourseDetail,
  CoursePlay,
  CourseMaterial,
  Assignment,
  Profile,
  GetStarted,
  LoginTeacher,
  RegisterTeacher,
  InputAssesment,
  CourseThree,
  CourseFour,
  Course,
  Assessment,
  InputEmailStudent,
  CourseContent,
  Testingcuy,
  AssignmentStudent,
  AssignmentStudentScore,
  SearchScreen,
};
