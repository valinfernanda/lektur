import {combineReducers} from 'redux';
import {GlobalReducer} from './GlobalReducer';
import {RegisterReducer} from '../Features/Auth/Register/Redux/reducer';
import {LoginReducer} from '../Features/Auth/Login/Redux/reducer';
import {EditProfileReducer} from '../Features/Profile/EditProfile/Redux/reducer';
import {TeacherCourseReducer} from '../Features/Teacher/Course/Redux/reducer';
import {TeacherAssessmentReducer} from '../Features/Teacher/Assessment/Redux/reducer';
import {TeacherAddCourseReducer} from '../Features/Teacher/AddCourse/Redux/reducer';
import {InputAssesmentReducer} from '../Features/Teacher/InputAssesment/Redux/reducer';
import {HomeReducer} from '../Features/Home/Redux/reducer';
import {CourseDetailReducer} from '../Features/CourseDetail/Redux/reducer';
import {SetStudentDataById} from '../Features/Teacher/InviteStudents/redux/reducer';
import {StudentCoursesReducer} from '../Features/Student/StudentCourses/redux/reducer';
import {StudentAssessmentReducer} from '../Features/Student/Assignment/AssignmentStudent/Redux/reducer';
import {StudentContentReducer} from '../Features/Student/CourseContent/redux/reducer';
import {CoursePlayReducer} from '../Features/Student/CoursePlay/redux/reducer';
import {StudentAssignmentReducer} from '../Features/Student/Assignment/AssignmentStudentScore/redux/reducer';
import {TeacherEdit} from '../Features/Teacher/AddCourseEdit/Redux/reducer';
import {InviteEmailReducer} from '../Features/Teacher/InviteEmailStudents/Redux/reducer';
import {StudentAssesmentReducer} from '../Features/Student/Assignment/redux/reducer';
import {SearchResultReducer} from '../Features/SearchScreen/redux/reducer';
import {CourseMaterialReducer} from '../Features/Student/CourseMaterial/redux/reducer';
// import {StudentPdfReducer} from '../Features/Student/CourseMaterial/Redux/reducer';

export const AllReducer = combineReducers({
  GlobalReducer,
  RegisterReducer,
  LoginReducer,
  EditProfileReducer,
  TeacherCourseReducer,
  TeacherAssessmentReducer,
  TeacherAddCourseReducer,
  InputAssesmentReducer,
  HomeReducer,
  CourseDetailReducer,
  SetStudentDataById,
  StudentCoursesReducer,
  StudentAssessmentReducer,
  StudentContentReducer,
  CoursePlayReducer,
  StudentAssignmentReducer,
  TeacherEdit,
  InviteEmailReducer,
  StudentAssesmentReducer,
  SearchResultReducer,
  CourseMaterialReducer,
  // StudentPdfReducer,
});
