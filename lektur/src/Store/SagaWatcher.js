import {all} from 'redux-saga/effects';
import {SagasRegister} from '../Features/Auth/Register/Redux/saga';
import {SagasLogin} from '../Features/Auth/Login/Redux/saga';
import {SagasEditProfile} from '../Features/Profile/EditProfile/Redux/saga';
import {SagasUpdateProfile} from '../Features/Profile/EditProfile/Redux/sagaUpdate';
import {SagasTeacherCourse} from '../Features/Teacher/Course/Redux/saga';
import {SagasTeacherAssesment} from '../Features/Teacher/Assessment/Redux/saga';
import {SagasAddCourse} from '../Features/Teacher/AddCourse/Redux/saga';
import {SagasGetCourse} from '../Features/Teacher/CourseThree/Redux/saga';
import {SagasInputAssesment} from '../Features/Teacher/InputAssesment/Redux/saga';
import {SagaHome} from '../Features/Home/Redux/saga';
import {ToCourseDetail} from '../Features/Home/Redux/sagaToCourseDetail';
// import {SagaCourseDetail} from '../Features/Course/CourseDetail/Redux/saga';

import {SagaEnroll} from '../Features/CourseDetail/Redux/sagaForEnroll';
import {SagasApproveStudent} from '../Features/Teacher/InviteStudents/redux/saga';
import {SagasGetStudentCourses} from '../Features/Student/StudentCourses/redux/saga';
import {SagasStudentAssesment} from '../Features/Student/Assignment/AssignmentStudent/Redux/saga';
import {SagasToCourseContent} from '../Features/Student/StudentCourses/redux/sagaToContents';
import {SagasToCoursePlay} from '../Features/Student/CourseContent/redux/sagaToCoursePlay';
// import {SagasToCourseAssesment} from '../Features/Student/Assignment/redux/saga';
import {SagasToCourseAssesment} from '../Features/Student/Assignment/redux/saga';
import {SagasEditTeacher} from '../Features/Teacher/AddCourseEdit/Redux/saga';
import {SagasInviteEmail} from '../Features/Teacher/InviteEmailStudents/Redux/saga';
import {SagasInputScore} from '../Features/Student/Assignment/AssignmentStudentScore/redux/saga';
import {SagasGetStudentAssesment} from '../Features/Student/Assignment/redux/sagaGetData';
// import {SagasSearchCourse} from '../Features/Home/Redux/sagaForSearch';
import {SagasSearchResult} from '../Features/SearchScreen/redux/saga';
import {SagasCourseMaterial} from '../Features/Student/CourseMaterial/redux/saga';
// import {SagasStudentPdf} from '../Features/Student/CourseMaterial/Redux/saga';

export function* SagaWatcher() {
  yield all([
    SagasRegister(),
    SagasLogin(),
    SagasEditProfile(),
    SagasUpdateProfile(),
    SagasTeacherCourse(),
    SagasTeacherAssesment(),
    SagasAddCourse(),
    SagasGetCourse(),
    SagasInputAssesment(),
    SagaHome(),
    ToCourseDetail(),
    // SagaCourseDetail(),
    SagaEnroll(),
    SagasApproveStudent(),
    SagasGetStudentCourses(),
    SagasStudentAssesment(),
    SagasToCourseContent(),
    SagasToCoursePlay(),
    SagasToCourseAssesment(),
    SagasEditTeacher(),
    SagasInviteEmail(),
    SagasInputScore(),
    SagasGetStudentAssesment(),
    // SagasSearchCourse(),
    SagasSearchResult(),
    SagasCourseMaterial(),
    // SagasStudentPdf(),
  ]);
}
