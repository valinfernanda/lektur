import {createStore, applyMiddleware} from 'redux';
import {AllReducer} from './CombineReducer';
import {persistStore, persistReducer} from 'redux-persist';
import logger from 'redux-logger';
import CreateSagaMiddleware from 'redux-saga';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {SagaWatcher} from './SagaWatcher';

const config = {
  key: 'lektur',
  storage: AsyncStorage,
};

const SagaMiddleware = CreateSagaMiddleware();

const persistedReducer = persistReducer(config, AllReducer);

const allMiddleware = applyMiddleware(logger, SagaMiddleware);

export const Store = createStore(persistedReducer, allMiddleware);

export const Persistor = persistStore(Store);

SagaMiddleware.run(SagaWatcher);
