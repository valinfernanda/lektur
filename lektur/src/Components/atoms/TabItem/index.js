import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {
  Course,
  Profile,
  Home,
  Assignment,
  HomeActive,
  CourseActive,
  AssignmentActive,
  ProfileActive,
} from '../../../Assets/icon';

const TabItem = ({title, active, onPress, onLongPress}) => {
  const Icon = () => {
    if (title === 'Home') {
      return active ? <HomeActive /> : <Home />;
    }
    if (title === 'Course') {
      return active ? <CourseActive /> : <Course />;
    }
    if (title === 'Assignment') {
      return active ? <AssignmentActive /> : <Assignment />;
    }
    if (title === 'Profile') {
      return active ? <ProfileActive /> : <Profile />;
    }
    return <Home />;
  };
  return (
    <TouchableOpacity
      style={styles.container}
      onPress={onPress}
      onLongPress={onLongPress}>
      <Icon />
      <Text style={styles.text(active)}>{title}</Text>
    </TouchableOpacity>
  );
};

export default TabItem;

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
  },
  text: (active) => ({
    fontSize: 10,
    marginTop: 4,
    fontWeight: active ? 'bold' : 'normal',
  }),
});
