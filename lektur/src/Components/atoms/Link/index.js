import React from 'react';
import {StyleSheet, Text, TouchableOpacity} from 'react-native';
import {color} from 'react-native-reanimated';
// import {colors} from '../../../utils';

const Link = ({title, size, align, onPress, color}) => {
  return (
    <TouchableOpacity onPress={onPress}>
      <Text style={styles.text(size, align, color)}>{title}</Text>
    </TouchableOpacity>
  );
};

export default Link;

const styles = StyleSheet.create({
  text: (size, align, underline, color) => ({
    fontSize: size,
    // color: colors.text.secondary,
    fontFamily: 'Nunito-Regular',
    textDecorationLine: 'underline',
    textAlign: align,
    color: color ? 'blue' : 'black',
  }),
});
