import React from 'react';
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
// import { TouchableOpacity } from 'react-native-gesture-handler';
// import { colors, fonts } from '../../../utils';
// import IconOnly from './IconOnly';

// import BtnIconSend from './BtnIconSend';

const CourseContent = ({type, title, onPress, icon, disable}) => {
  return (
    <TouchableOpacity style={styles.container(type)} onPress={onPress}>
      <Text style={styles.text(type)}>{title}</Text>
    </TouchableOpacity>
  );
};

export default CourseContent;

const styles = StyleSheet.create({
  container: (type) => ({
    backgroundColor: 'white',
    paddingVertical: 10,
    borderRadius: 10,
    backgroundColor: '#16213B',
    borderRadius: 3,
    width: 90,
    height: 45,
  }),
  text: (type) => ({
    fontSize: 18,
    fontWeight: '600',
    fontFamily: 'Nunito-SemiBold',
    textAlign: 'center',
    color: 'white',
  }),
  disableBg: {
    paddingVertical: 10,
    borderRadius: 10,
  },
  disableText: {
    fontSize: 18,

    textAlign: 'center',
  },
});
