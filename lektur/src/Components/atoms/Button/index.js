import React from 'react';
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import {Size} from '../../../Shared/Global/Config/Size';

const Button = ({type, title, onPress}) => {
  return (
    <TouchableOpacity style={styles.container(type)} onPress={onPress}>
      <Text style={styles.text(type)}>{title}</Text>
    </TouchableOpacity>
  );
};

export default Button;

const styles = StyleSheet.create({
  container: (type) => ({
    backgroundColor: 'white',
    paddingVertical: 10,
    borderRadius: 10,
    backgroundColor: '#16213B',
    borderRadius: 3,
    width: Size.wp25,
    height: Size.h6,
  }),
  text: (type) => ({
    fontSize: Size.ms16,
    fontWeight: '600',
    fontFamily: 'Nunito-SemiBold',
    textAlign: 'center',
    color: 'white',
  }),
});
