import React, {useState} from 'react';
import {StyleSheet, Text, View, TextInput} from 'react-native';
import {Size} from '../../../Shared/Global/Config/Size';
// import { colors, fonts } from '../../../utils'

const Input = ({label, value, onChangeText, secureTextEntry, disable}) => {
  return (
    <View>
      <Text style={styles.label}>{label}</Text>
      <TextInput
        value={value}
        onChangeText={onChangeText}
        secureTextEntry={secureTextEntry}
        editable={!disable}
        selectTextOnFocus={!disable}
        style={styles.kotak}
      />
    </View>
  );
};

export default Input;

const styles = StyleSheet.create({
  // input: {
  //   borderWidth: 1,
  //   // borderColor: border,
  //   borderRadius: 10,
  //   padding: 12,
  // },
  label: {
    fontSize: Size.ms16,
    fontFamily: 'Nunito-Regular',
  },
  kotak: {
    borderBottomWidth: 1,
    paddingRight: Size.wp13,
    width: Size.wp70,
  },
});
