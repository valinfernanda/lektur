import React from 'react';
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import {ClickWhite} from '../../../Assets';
import {Size} from '../../../Shared/Global/Config/Size';
import Gap from '../Gap';

const ButtonLesson = ({type, title, onPress, icon, disable}) => {
  return (
    <TouchableOpacity style={styles.container(type)} onPress={onPress}>
      <View style={styles.kotak}>
        <ClickWhite />
        <Gap width={10} />
        <Text style={styles.text(type)}>{title}</Text>
      </View>
    </TouchableOpacity>
  );
};

export default ButtonLesson;

const styles = StyleSheet.create({
  container: (type) => ({
    backgroundColor: 'white',
    paddingVertical: Size.h1,
    backgroundColor: '#3E89AE',
    borderRadius: 3,
    width: Size.wp70,
    height: Size.h5,
  }),
  text: (type) => ({
    fontSize: Size.ms14,
    fontFamily: 'Nunito-SemiBold',
    textAlign: 'center',
    color: 'white',
  }),
  kotak: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
});
