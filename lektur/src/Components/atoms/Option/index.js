import React, {useState} from 'react';
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import {CheckBox} from 'react-native-elements';

const Option = ({type, title, onPress, icon, disable}) => {
  const [check, setcheck] = useState(false);
  return (
    <CheckBox
      title={title}
      checked={check}
      checkedIcon="dot-circle-o"
      uncheckedIcon="circle-o"
      containerStyle={styles.checkboxStyle}
      onPress={() => {
        setcheck(!check);
      }}
    />
  );
};

export default Option;

const styles = StyleSheet.create({});
