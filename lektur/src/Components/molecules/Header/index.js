import React from 'react';
// import { TouchableOpacity } from 'react-native';
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import {ArrowBack} from '../../../Assets';
import {Size} from '../../../Shared/Global/Config/Size';
import {Gap} from '../../atoms';

const Header = ({onPress, title, type, navigation}) => {
  // if(type==='dark-profile'){
  //     return <DarkProfile onPress={onPress} />
  // }
  return (
    <View style={styles.container(type)}>
      {/* <Button
        type="icon-only"
        icon={type === 'dark' ? 'back-light' : 'back-dark'}
        onPress={onPress}
      /> */}
      <TouchableOpacity onPress={onPress}>
        <ArrowBack />
      </TouchableOpacity>

      <Text style={styles.text(type)}>{title}</Text>
      <Gap width={24} />
    </View>
  );
};

export default Header;

const styles = StyleSheet.create({
  container: (type) => ({
    paddingHorizontal: 16,
    paddingVertical: 30,
    backgroundColor: type === 'dark' ? 'blue' : 'white',
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomLeftRadius: type === 'dark' ? 20 : 0,
    borderBottomRightRadius: type === 'dark' ? 20 : 0,
  }),
  text: (type) => ({
    flex: 1,
    textAlign: 'center',
    fontSize: Size.ms18,
    fontFamily: 'Nunito-SemiBold',
    fontWeight: 'bold',
    color: 'black',
  }),
});
